/**
 * 底部flatlistview
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet,SafeAreaView } from 'react-native';
import GlobalStyles from '../common/GlobalStyles';


export default class BottomListModal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
        }
    }

    show() {
        this.setState({
            modalVisible: true,
        })
    }

    static propTypes = {
        data: PropTypes.array,   //flatlist数据
        callBack: PropTypes.func //cell点击的回调
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //cell
    renderItem(data) {
        return <TouchableOpacity style={[styles.FlatListText, { backgroundColor: 'white' }]} onPress={() => {
            this.setState({ modalVisible: false })
            //使用定时器,相当于新开线程,防止界面卡死
            setTimeout(() => {
                this.props.callBack(data.item)
            }, 200);
        }}>
            <Text style={styles.FlatListText}>{data.item}</Text>
        </TouchableOpacity>
    }

    //flatlist
    renderFlatList() {

        // console.log('传过来的数据:', this.props.data)
        return (
            <View style={styles.backgroundView}>
                <View style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => this.separatorView()}
                        data={this.props.data}
                        renderItem={data => this.renderItem(data)}
                        bounces={false}
                    />
                    <TouchableOpacity style={styles.cancelText} onPress={() => {
                        this.setState({
                            modalVisible: false,
                        })
                    }}>
                        <Text style={styles.FlatListText}>取消</Text>
                    </TouchableOpacity>
                </View>
            </View >
        )
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderFlatList()}
                </Modal>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    backgroundView: {
        flex:1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    FlatListText: {
        height: 50,
        lineHeight: 50,
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
    },
    cancelText: {
        marginTop: 10,
        backgroundColor: 'white',
        height: 50,
    }
});
