/**
 * 从相册选择照片或者拍照
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Platform } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Marker, { Position, ImageFormat } from 'react-native-image-marker'
import BottomListModal from '../common/BottomListModal';

const ImgSelectArr = ['拍摄', '从手机相册选择'];

export default class CustomImagePicker extends Component {

  constructor(props) {
    super(props)
  }
  static propTypes = {
    onPickImages: PropTypes.func,
    options: PropTypes.object,
    title: PropTypes.string,
  };

  static defaultProps = {
    onPickImages: undefined,
    options: {},
    title: '选择图片',
  };
  //打开相机
  onPressCamera() {
    ImagePicker.openCamera({
      compressImageQuality: 0.3,
      compressImageMaxWidth: 720,
      compressImageMaxHeight: 720,
      mediaType: 'photo'
    }).then(image => {
      console.log('image', image);
      this._mark(image, 'take')
    });
  }

  //! 图片添加水印
  _mark(image, type) {
    if (type === 'take') {
      //! 使用拍照
      Marker.markText({
        src: image.path,
        text: this._getTime(),
        X: 30,
        Y: 30,
        color: '#FFF',
        fontName: 'Arial-BoldItalicMT',
        fontSize: 27,
        scale: 1,
        quality: 70,
        shadowStyle: {
          dx: 2,
          dy: 2,
          radius: 2,
          color: '#000'
        },
        position: Position.bottomRight,
      })
        .then((path) => {
          console.log('path',path);
          
          let tempArr = []
          image.path = Platform.OS === 'android' ? 'file://' + path : path
          tempArr.push(image)
          this.props.onPickImages && this.props.onPickImages(tempArr);
        })
        .catch((err) => {
          console.log(err)
        })
    } else {
      //! 使用多选
      for (var i = 0; i < image.length; i++) {
        var item = image[i]
        Marker.markText({
          src: item.path,
          text: this._getTime(),
          X: 30,
          Y: 30,
          color: '#FFF',
          fontName: 'Arial-BoldItalicMT',
          fontSize: 27,
          scale: 1,
          quality: 70,
          shadowStyle: {
            dx: 2,
            dy: 2,
            radius: 2,
            color: '#000'
          },
          position: Position.bottomRight,
        }).then((path) => {
          let tempArr = []
          item.path = Platform.OS === 'android' ? 'file://' + path : path
          tempArr.push(item)
          this.props.onPickImages && this.props.onPickImages(tempArr);
        }).catch((err) => {
          console.log(err)
        })
      }
    }

  }

  //! 获取拍照时间
  _getTime() {
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    m = m < 10 ? ('0' + m) : m;
    var d = date.getDate();
    d = d < 10 ? ('0' + d) : d;
    var h = date.getHours();
    h = h < 10 ? ('0' + h) : h;
    var minute = date.getMinutes();
    var second = date.getSeconds();
    minute = minute < 10 ? ('0' + minute) : minute;
    second = second < 10 ? ('0' + second) : second;
    return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;

  }

  //打开相册
  onPressPhoto() {
    ImagePicker.openPicker({
      multiple: true,
      compressImageQuality: 0.3,
      compressImageMaxWidth: 720,
      compressImageMaxHeight: 720,
      mediaType: 'photo'
    }).then(images => {
      console.log('image', images);
      this._mark(images, 'pick')
    });
  }

  show() {
    this.refs.selectImgModal.show();
  }

  render() {
    return (
      <BottomListModal
        ref="selectImgModal"
        data={ImgSelectArr}
        callBack={(data) => {
          console.log('点击了啥:', data)
          if (data === ImgSelectArr[0]) {
            this.onPressCamera()
          } else if (data === ImgSelectArr[1]) {
            this.onPressPhoto()
          }
        }}
      />

    );
  }
}