/**
 * 点击cell添加照片的view
 */
import React,{ Component }  from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, TouchableOpacity, Modal, ActivityIndicator, View, ViewPropTypes, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import ImageViewer from 'react-native-image-zoom-viewer';   //类似微信朋友圈浏览图片的效果
import FastImage from 'react-native-fast-image';    //加载图片,添加缓存,占位图
import CustomImagePicker from '../common/CustomImagePicker';



const SelectImageSourcePropType = PropTypes.shape({
    uri: PropTypes.string,
});

export default class SelectImageView extends Component {
    static propTypes = {
        // title: propTypes.string,
        onPickImages: PropTypes.func,
        onPress: PropTypes.func,
        onDelete: PropTypes.func,
        options: PropTypes.object,
        source: PropTypes.oneOfType([SelectImageSourcePropType, PropTypes.string]), //source或者是uri字段,或者直接就是字符串
        readonly: PropTypes.bool,
        style: ViewPropTypes.style,
    };
    static defaultProps = {
        options: {},
        // title: '选择图片',
        onPickImages: undefined,
        onPress: undefined,
        onDelete: undefined,
        source: undefined,
        style: undefined,
        readonly: false,
    };

    constructor(props) {
        super(props);

        this.state = {
            visible: false, //点击图片放大图片的modal是否可见
            loaded: false,
            loading: false,
        };
    }


    onDelete = () => {
        if (this.props.onDelete) this.props.onDelete();
    };

    picker: CustomImagePicker;

    //显示图片的view
    renderImage() {
        if (this.props.source) {        //如果有图片
            
            const source = { uri: 'empty' };
            if (_.isString(this.props.source) && this.props.source !== '') {
                source.uri = this.props.source;
            } else if (_.isString(this.props.source.uri) && this.props.source.uri !== '') {
                source.uri = this.props.source.uri;
            }
            return (
                <View>
                    <FastImage
                        source={source}
                        style={styles.imageStyle}
                        resizeMode={FastImage.resizeMode.cover}
                        onLoadStart={() => {
                            if (!this.state.loaded) {
                                this.setState({ loading: true });
                            }
                        }}
                        onLoadEnd={() => {
                            this.setState({ loading: false, loaded: true });
                        }}
                        onError={() => {
                            this.setState({ loading: false });
                        }}
                    />
                    {this.state.loading && (
                        <View style={{
                            position: 'absolute',
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                        >
                            <ActivityIndicator />
                        </View>
                    )}
                </View>
            );
        }
        
        return (            //没有图片就显示 add-img.png
            <Image
                source={require('../../resource/add-img.png')}
                style={styles.imageStyle}
                resizeMode="cover"
            />
        );
    }

    //删除的图标 
    renderDelete() {
        if (this.props.source && this.props.onDelete) {
            return (
                <TouchableOpacity style={styles.deleteIcon} onPress={this.onDelete}>
                    <Icon active style={{ color: '#ff171a', fontSize: 18 }} name="trash" /> 
                </TouchableOpacity>
            );
        }
        return null;
    }

    //CustomImagePicker
    renderPicker() {
        return (
            <CustomImagePicker
                ref={(ref) => {
                    this.picker = ref;
                }}
                options={this.props.options}
                onPickImages={this.props.onPickImages}
            />
        );
    }

    //菊花
    renderImageLoading = () => (
        <View style={{
            justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%',
        }}
        >
            <ActivityIndicator />
        </View>
    );

    //大图展示
    renderPreView() {
        if (this.props.source && !this.props.onPress) {
            return (
                <Modal visible={this.state.visible} transparent>
                    <ImageViewer
                        imageUrls={[{
                            url: this.props.source.uri ?
                                this.props.source.uri :
                                this.props.source,
                        }]}
                        onClick={() => {
                            this.setState({ visible: false });
                        }}
                        onSwipeDown={() => {
                            this.setState({ visible: false });
                        }}
                        loadingRender={this.renderImageLoading}
                    />
                </Modal>
            );
        }
        return null;
    }

    render() {
        return (
            <TouchableOpacity
                style={[styles.imageTouch, this.props.style]}
                onPress={() => {
                    if (this.props.source) {                              //有图片
                        if (this.props.onPress) {
                            this.props.onPress();
                        } else {
                            this.setState({ visible: true });
                        }
                    } else if (this.props.onPickImages && this.picker) {  //添加图片
                        this.picker.show();
                        console.log(this.props);
                    }
                }}
            >
                {this.renderImage()}
                {!this.props.readonly && this.renderDelete()}
                {!this.props.readonly && this.renderPicker()}
                {this.renderPreView()}
            </TouchableOpacity>
        );
    }
}


const styles = StyleSheet.create({
    imageStyle: {
        width: '100%',
        height: '100%',
        maxWidth: 100,
        maxHeight: 75,
    },
    imageTouch: {
        width: '100%',
        height: '100%',
        maxWidth: 100,
        maxHeight: 75,
        backgroundColor: '#E0E0E0',
        marginLeft: 5,
    },
    deleteIcon: {
        position: 'absolute',
        top: 0,
        right: 0,
        height: 30,
        width: 30,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
