/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button, Image, TextInput, TouchableOpacity, Modal, FlatList, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../Navigator/NavigationUtils'
import { Dimensions } from 'react-native'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DataStore from '../dao/DataStore'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_LOGOUT_TYPE } from '../common/EmitterTypes'
import DeviceInfo from 'react-native-device-info'
// import JSEncrypt from 'jsencrypt';
import secret from '../data/secret.json'
import RSAKey from 'react-native-rsa'
import { Buffer } from 'buffer'
import NetworkDao from '../dao/NetworkDao'
import publicDao from '../dao/publicDao'


// var screenHeight = Dimensions.get('window').height;
const screenHeight = Platform.OS === "ios" ? Dimensions.get("window").height : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT");
var screenWidth = Dimensions.get('window').width;
var IS_IOS = Platform.OS === 'ios';
let dataStore = new DataStore();
let networkDao = new NetworkDao();

type Props = {};
export default class LoginViewController extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            animationType: 'none',//modal动画效果
            modalVisible: false,//modal模态场景是否可见
            transparent: true,//modal是否透明显示
            duiName: '',     //名称
            duiCode: '',
            userName: '',    //用户名
            passWord: '',    //密码
            usersArray: [],     //本地所有用户信息
            indicatorAnimating: false, //菊花
            duiChangArr: [],
        };
    }

    //用户名下拉箭头点击事件
    loadMoreUserName = () => {
        this._setModalVisible(true)
    }

    //登陆按钮点击事件
    loginBtnClick = () => {
        NavigationUtil.navigation = this.props.navigation;

        if ((this.state.duiName && this.state.duiName.length > 0) && (this.state.duiCode && this.state.duiCode.length > 0)){

            if ((this.state.userName && this.state.userName.length > 1) && (this.state.passWord && this.state.passWord.length > 1)) {
                this.setState({ indicatorAnimating: true })

                let modulusKey1 = secret.modulus;                       //获取modulus 
                let modulusKey2 = new Buffer(modulusKey1, 'base64');    //创建buffer
                let modulusKey3 = modulusKey2.toString('hex');          //转16进制
                let modulusKey4 = secret.exponent                       //获取AQAB,转16进制就是10001
                let ConsultPublicKey = { n: modulusKey3, e: '10001' };  //生成公钥
                var rsa = new RSAKey();
                rsa.setPublicString(JSON.stringify(ConsultPublicKey));
                var sec_pwd = rsa.encrypt(this.state.passWord);
                let pwdBuffer = new Buffer(sec_pwd, 'hex');
                let pwdBuffer3 = pwdBuffer.toString('base64');
                const udid = DeviceInfo.getUniqueID();
                var plat = IS_IOS ? 'ios' : 'android'
                let replacePwd = pwdBuffer3.split('+').join('%2B')
                var params = {}
                params.UserId = this.state.userName
                params.Password = replacePwd
                params.UDID = udid
                params.AppKey = plat
                params.YardCode = this.state.duiCode
                var paramsStr = JSON.stringify(params)
                networkDao.fetchLoginNet(paramsStr)
                    .then(data => {
                        this.setState({ indicatorAnimating: false })
                        if (data._backcode == '202') {
                            console.log('登陆成功',data.LoginUser.Privileges)
                            publicDao.CRUUENT_DUINAME = this.state.duiName
                            publicDao.CURRENT_DUICODE = this.state.duiCode
                            publicDao.CURRENT_USER = this.state.userName
                            publicDao.CURRENT_TOKEN = data.Token
                            publicDao.CURRENT_PRIVILEGES = data.LoginUser.Privileges
                            publicDao.CURRENT_ROLEID = data.LoginUser.RoleID
                            publicDao.IS_LOGOUT = "0"
                            dataStore.saveDuiNameArr(this.state.duiChangArr, this.state.duiName)
                            dataStore.saveUser(this.state.duiName, this.state.userName, this.state.passWord, data.Token)
                            let currentInfo = {}
                            currentInfo.userName = this.state.userName
                            currentInfo.duiName = this.state.duiName
                            currentInfo.duiCode = this.state.duiCode
                            currentInfo.token = data.Token
                            currentInfo.RoleID = data.LoginUser.RoleID
                            currentInfo.Privileges = data.LoginUser.Privileges
                            dataStore.saveCurrentUserInfo(currentInfo)
                            NavigationUtil.goPage(currentInfo, 'Main')  //currentInfo传不过去
                        } else if (data._backcode == '300') {
                            this.refs.toast.show('用户不存在', 1200)
                        } else if (data._backcode == '301') {
                            this.refs.toast.show('密码错误', 1200)
                        } else if (data._backcode == '302') {
                            this.refs.toast.show('用户被锁定', 1200)
                        } else if (data._backcode == '500') {
                            console.log('token失效')
                        }
                    })
                    .catch(error => {
                        this.setState({ indicatorAnimating: false })
                    })
    
            } else {
                this.refs.toast.show('用户名或密码不能为空', 1200)
            }
    
        }else{
            this.refs.toast.show('请选择堆场', 1200)
        }
    }

    //清除记录按钮点击事件
    clearUserBtnClick = () => {
        this.setState({
            duiName: '',
            userName: '',
            passWord: '',
            duiCode:''
        })
        dataStore.clearUser(this.state.duiName, (data) => {
            if (data == '1') {
                console.log('删除用户成功')
            }
        })
    }

    //modal的显示和隐藏
    _setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    //绘制下拉更多用户名cell
    _createDuiNameItem(item) {
        // console.log('>>>', item.item)
        return (
            <TouchableOpacity onPress={() => {
                //点了了cell
                this._setModalVisible(false);
                // this.setState({ userName: item.userName, passWord: item.passWord })
                dataStore.getUsers(item.item.YardName, (data) => {
                    // var user = JSON.parse(data)
                    this.setState({
                        duiName: item.item.YardName,
                        duiCode: item.item.YardCode,
                        // userName: user.userName,
                        // passWord: user.passWord,
                    })
                })

            }}>
                <View >
                    <Text style={styles.userNameCell}>{item.item.YardName}</Text>
                    <View style={styles.cellSepLine}></View>
                </View>
            </TouchableOpacity>
        )
    }

    componentDidMount() {
        dataStore.getDuiNameArr((data) => {
            var duiNameInfo = JSON.parse(data)
            if (duiNameInfo.currentDuiName == null) {
                return
            }
            dataStore.getUsers(duiNameInfo.currentDuiName, (data) => {
                console.log('当前用户:', data)
                // var user = JSON.parse(data)
                this.setState({
                    duiChangArr: duiNameInfo.duiArr,
                })
                if (publicDao.IS_LOGOUT == '1') {   //退出登录
                    console.log('点了了退出登录,可以在这删除本地数据')
                    this.setState({
                        userName: '',
                        passWord: '',
                    })

                    dataStore.clearUser(this.state.duiName, (data) => {
                        if (data == '1') {
                            console.log('删除用户成功')
                        }
                    })

                    dataStore.getCurrentUserInfo(data => {
                        var jsonData = JSON.parse(data)
                        this.setState({ duiCode: jsonData.duiCode })
                        dataStore.clearCurrentUserInfo((data) => {
                            if (data == '1') {
                                console.log('删除当前用户信息成功')
                            }
                        })
                    })

                }

            })

        })
        networkDao.fetDuiName()
            .then(data => {
                // console.log('堆场信息:', data)
                if (data._backcode == '200') {
                    this.setState({ duiChangArr: data.YardList })
                }
            })
            .catch(error => {
                console.log('获取堆场数据失败')
            })


    }

    componentWillUnmount() {
        console.log('销毁了')
        if (this.logOutListener) {
            this.logOutListener.remove();   //移除事件监听
        }
    }

    render() {
        let modalBackgroundStyle = {
            backgroundColor: this.state.transparent ? 'rgba(0, 0, 0, 0.1)' : 'red',
            width: screenWidth,
            height: screenHeight,
            justifyContent: 'center',
        };
        return (
            <View style={styles.container}>
                <Modal
                    animationType={this.state.animationType}
                    transparent={this.state.transparent}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { this._setModalVisible(false) }}
                >
                    <TouchableOpacity onPress={() => { this._setModalVisible(false) }}>
                        <View style={modalBackgroundStyle}>
                            <FlatList
                                data={this.state.duiChangArr}
                                renderItem={item => this._createDuiNameItem(item)}
                                keyExtractor={(item, index) => index.toString()}
                                style={[styles.flatListStyle, , { marginTop: IS_IOS ? 285 : 315 }]}
                                bounces='false'
                            />
                        </View>
                    </TouchableOpacity>
                </Modal>
                <KeyboardAwareScrollView keyboardShouldPersistTaps={'handled'}>
                    {this.state.indicatorAnimating && (
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: screenHeight / 2 - 10, left: screenWidth / 2 - 10 }]}
                            size={IS_IOS ? 'large' : 40}
                            color='gray'
                        />)}
                    <View style={styles.container}>
                        <Image
                            style={{ width: screenWidth, height: screenWidth * 41 / 63 }}
                            source={require('../../resource/banner.png')}
                        />
                        <View style={[styles.middleImageView, { zIndex: 100, marginLeft: screenWidth / 2 - 40 }]}>
                            <Image
                                style={{ backgroundColor: 'white', width: 73, height: 73 }}
                                source={require('../../resource/middle.png')}
                            />
                        </View>
                        <View style={styles.backgroundView}>
                            <Text style={styles.titleStyle}>天津堆场系统</Text>
                            <View style={styles.userNameView}>
                                <Image
                                    style={{ width: 20, height: 25, marginLeft: 10, }}
                                    source={require('../../resource/zhuangxiang.png')}
                                />
                                <View style={styles.cutStyle}></View>
                                <TouchableOpacity
                                    style={{ marginLeft: 10, height: 30, minWidth: 200, marginTop: 11 }}
                                    onPress={this.loadMoreUserName}
                                >
                                    <Text style={{ fontSize: 16 }}>{this.state.duiName ? this.state.duiName : '请选择堆场'}</Text>
                                </TouchableOpacity>
                                {/* <Text style={{ marginLeft: 10, height: 30, minWidth: 200, marginTop: 11, fontSize: 16 }}>{this.state.duiName}</Text> */}
                                <View style={styles.caretdown}>
                                    <AntDesign
                                        name={'caretdown'}
                                        size={15}
                                        style={{ color: 'black' }}
                                        onPress={this.loadMoreUserName}
                                    />
                                </View>
                            </View>
                            <View style={styles.pwdView}>
                                <Image
                                    style={{ width: 20, height: 25, marginLeft: 10 }}
                                    source={require('../../resource/username.png')}
                                />
                                <View style={styles.cutStyle}></View>
                                <TextInput
                                    style={{ minWidth: 200, marginLeft: 10, height: 30, marginRight: 30, paddingTop: 0, paddingBottom: 0 }}
                                    placeholder='请输入用户名'
                                    defaultValue={this.state.userName}
                                    autoCapitalize='characters'
                                    onChangeText={(text) => {
                                        text = text.replace(/\s/gi, '').toUpperCase()
                                        this.setState({ userName: text })
                                    }}
                                />
                            </View>
                            <View style={styles.pwdView}>
                                <Image
                                    style={{ width: 20, height: 25, marginLeft: 10 }}
                                    source={require('../../resource/pwd.png')}
                                />
                                <View style={styles.cutStyle}></View>
                                <TextInput
                                    style={{ minWidth: 200, marginLeft: 10, height: 30, marginRight: 30, paddingTop: 0, paddingBottom: 0 }}
                                    placeholder='请输入密码'
                                    secureTextEntry={true}
                                    defaultValue={this.state.passWord}
                                    onChangeText={(text) => {
                                        text = text.replace(/\s/gi, '')
                                        this.setState({ passWord: text })
                                    }}
                                />
                            </View>
                            <TouchableOpacity
                                style={styles.loginBtn}
                                onPress={this.loginBtnClick}>
                                <View >
                                    <Text style={styles.loginText}>登 陆</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.clearBtn} onPress={this.clearUserBtnClick}>
                                <Text style={styles.clearText}>清除记录</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
                <Toast ref="toast"
                    position='center'
                />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(255,249,249,1)',
    },
    middleImageView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        width: 80,
        height: 80,
        marginTop: -130,


    },
    backgroundView: {
        // alignItems: 'flex-end',
        backgroundColor: 'white',
        marginLeft: 20,
        marginRight: 20,
        marginTop: -40,
        borderRadius: 5,
        padding: 20,
    },
    titleStyle: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'black',
        marginTop: 30,
        fontWeight: 'bold'
    },
    userNameView: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#DEDEDE',
        borderWidth: 1,
        marginTop: 10,
        width: '100%',
        height: 40,
        paddingRight: 40,

    },
    cutStyle: {
        marginLeft: 10,
        backgroundColor: '#DEDEDE',
        width: 1,
        height: 30,
    },
    caretdown: {                //加载更多用户名下三角
        position: 'absolute',
        top: 10,
        right: 10,
        width: 20,
        height: 20,
    },
    pwdView: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: '#DEDEDE',
        borderWidth: 1,
        marginTop: 15,
        width: '100%',
        height: 40,
        paddingRight: 20,
    },
    loginBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        width: '100%',
        borderRadius: 5,
        backgroundColor: 'rgba(186,28,38,1)',
        height: 40,
        shadowColor: 'gray',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.4,
        shadowRadius: 1,
        elevation: 1,       //这是Android阴影,上面是iOS
    },
    loginText: {
        fontSize: 18,
        color: 'white',
    },
    clearBtn: {
        width: 80,
        alignSelf: 'flex-end',
    },
    clearText: {
        fontSize: 15,
        color: 'red',
        height: 20,
        marginTop: 15,
        textDecorationLine: 'underline',
        textAlign: 'right'
    },
    flatListStyle: {
        marginLeft: 80,
        marginRight: 40,
    },
    userNameCell: {
        backgroundColor: 'white',
        height: 40,
        fontSize: 15,
        paddingLeft: 10,
        paddingTop: 10,
    },
    indicatorStyle: {
        zIndex: 119,
    }

});
