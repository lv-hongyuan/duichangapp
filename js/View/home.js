//首页
import React, { Component } from 'react';
import { ScrollView, Linking, Platform, StyleSheet, Text, View, TouchableOpacity, Image, Modal, StatusBar, PermissionsAndroid } from 'react-native';
import { Dimensions } from 'react-native'
import NavigationUtil from '../Navigator/NavigationUtils'
import GlobalStyles from '../common/GlobalStyles';
import publicDao from '../dao/publicDao';
import SafeAreaViewPlus from '../tools/SafeAreaViewPlus';
import DeviceInfo from 'react-native-device-info'
import NetworkDao from '../dao/NetworkDao';
import Toast from 'react-native-easy-toast';
import BackPressComponent from "../common/BackPressComponent";
let networkDao = new NetworkDao()
const USERINFO_KEY = 'userInfoKey'
import AsyncStorage from '@react-native-community/async-storage';
const screenHeight = Platform.OS === "ios" ? Dimensions.get("window").height : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT");
var screenWidth = Dimensions.get('window').width;


export default class HomeView extends Component {

    constructor(props) {
        super(props);
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        const version = DeviceInfo.getVersion()
        this.state = {
            data: [ '修箱业务','拆装箱业务'],     //要显示的功能
            Appversion: version,
            newestVersion: '',               //最新版本
            newestVersionURL: '',            //最新版本下载地址
            newestVersionMessage: '',           //最新版本更新信息
            modalShow: false
        };
    }
    onBackPress() {
        // NavigationUtil.goBack(this.props.navigation);
        return true;
    }
    componentDidMount() {
        //获取多媒体权限
        const permissions = [
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.CAMERA
        ]
        PermissionsAndroid.requestMultiple(permissions)

        //获取版本信息
        this.getVersionMessage()
        this.backPress.componentDidMount();
        NavigationUtil.navigation = this.props.navigation;
        let privileges = [1, 2]
        let newArr = []
        for (let i = 0; i < privileges.length; i++) {
            let index = parseInt(privileges[i]) - 1
            newArr.push(this.state.data[index])
        }
        this.setState({ data: newArr })
    }
    componentWillUnmount() {

        this.backPress.componentWillUnmount();
        this.timer && clearTimeout(this.timer);
    }

    //自动下载
    _version() {
        AsyncStorage.removeItem(USERINFO_KEY, (error) => {
            if (error) {
                console.log('删除失败:', error)
            } else {
                console.log('删除成功')
            }
        })
        let url = this.state.newestVersionURL
        this.refs.toasts.show('即将前往浏览器下载最新版本', 800)
        //this.setState({modalShow:false})
        this.timer = setTimeout(() => {
            Linking.canOpenURL(url).then(supported => {
                if (!supported) {
                    this.refs.toast.show('请先下载浏览器', 800)
                } else {
                    return Linking.openURL(url);
                }
            }).catch(err => console.error('An error occurred', err));
        }, 2000);
    }



    //获取最新版本
    getVersionMessage() {
        let param = {}
        param.AppKey = Platform.OS
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetAPPNewestVersion', paramStr)
            .then(data => {
                let num = data.VersionCode.replace(/[^\d^\.]+/g, '')
                if (data._backcode == '200') {
                    console.log('当前版本:', this.state.Appversion, '      最新版本:', num, publicDao.CURRENT_DUICODE);
                    if (this.state.Appversion >= num) {
                        console.log('已是最新版本');
                    } else {
                        this.setState({
                            modalShow: true,
                            newestVersion: data.VersionCode,
                            newestVersionURL: data.URL,
                            newestVersionMessage: data.UpgradeContent
                        })
                    }
                } else if (data._backcode == '400') {
                    this.refs.toast.show('接口调用异常-获取失败', 800)
                }
            })
            .catch(error => {
                console.log('errer:', error)
            })
    }

    //要展示的功能
    renderShowView() {

        return this.state.data.map((item, index) => {
            switch (item) {
                case '拆装箱业务':
                    return <TouchableOpacity
                        key={`${index}`}
                        style={styles.contentItem}
                        onPress={() => {
                            this.refs.toast.show('敬请期待', 300)
                        }}
                        activeOpacity={0.8}
                    >
                        <View style={{ flex:1  }} >
                            <Image style={{ width: '100%', height: '100%' }} source={require('../../resource/news-banner11.jpg')} />
                        </View>
                    </TouchableOpacity >

                    break;
                case '修箱业务':
                    return <TouchableOpacity
                        key={`${index}`}
                        style={styles.contentItem}
                        onPress={() => {
                            NavigationUtil.goPage({}, 'MainView')
                        }}
                        activeOpacity={0.8}
                    >
                        <View style={{ flex:1 }} >
                            <Image style={{ width: '100%', height: '100%' }} source={require('../../resource/news-banner.jpg')} />
                        </View>
                    </TouchableOpacity>
                    break;
            }
        })
    }

    render() {

        let modalBackgroundStyle = {
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            width: screenWidth,
            height: screenHeight,
            justifyContent: 'center',
            alignItems: 'center',
        };

        return (
            <SafeAreaViewPlus bottomInset={false} topColor={GlobalStyles.nav_bar_color} style={{
                backgroundColor: GlobalStyles.nav_bar_color,
                paddingTop: Platform.OS == 'android' ? 30 : 0
            }}>
                <View style={styles.container}>
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={this.state.modalShow}
                    >
                        <View style={modalBackgroundStyle}>
                            <Toast ref="toasts"
                                position='center'
                            />
                            <View style={styles.alertView}>
                                <View style={styles.alertNameView}>
                                    <Text style={styles.duiChangName}>修箱APP</Text>
                                    <Text style={styles.duiChangName}>{this.state.newestVersion}</Text>
                                </View>
                                <ScrollView style={styles.newVersionMessage}>
                                    <Text style={{ fontSize: 17, marginBottom: 10, }}>升级说明:</Text>
                                    <View style={styles.newVersionMessage2}>
                                        <Text style={{ lineHeight: 25 }}>{this.state.newestVersionMessage}</Text>
                                    </View>
                                </ScrollView>
                                <TouchableOpacity
                                    style={styles.upDataBotton}
                                    onPress={() => {
                                        this._version();
                                    }}
                                >
                                    <Text style={{ color: '#fff', fontSize: 23, fontWeight: 'bold' }}>升级</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Modal>
                    <Toast ref="toast"
                        position='center'
                    />
                    <View style={styles.navigationBar}>
                        <Text style={[styles.navTitle, { marginLeft: screenWidth / 2 - 40 }]}>业务选择</Text>
                        <TouchableOpacity
                            style={[styles.logoutBtn, { marginLeft: screenWidth / 2 - 85 }]}
                            onPress={() => {
                                NavigationUtil.goPage({}, 'UserInfo')
                            }}
                        >
                            <Image style={{ width: 24, height: 24 }} source={require('../../resource/icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.showItemBackView}>
                        {this.renderShowView()}
                    </View>
                </View>
            </SafeAreaViewPlus>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e3e3e3',
    },
    navigationBar: {
        height: GlobalStyles.is_iphoneX ? 44 : 55,
        backgroundColor: 'rgba(186,28,38,1)',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 20,
        paddingBottom: 7,
    },
    navTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
        width: 80,
        textAlign: 'center',
        marginTop: GlobalStyles.is_iphoneX ? 0 : 15,
    },
    logoutBtn: {
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: GlobalStyles.is_iphoneX ? 0 : 15,
    },
    contentItem: {
        marginTop: 15,
        width: '100%',
        height: screenWidth / 2.14 -15,
        flexDirection: 'column',
        borderRadius: 10,
        overflow: 'hidden'
    },
    alertView: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: '44%',
        width: '80%',
        overflow: 'hidden'
    },
    alertNameView: {
        flexDirection: 'row',
        height: 50,
        borderBottomColor: '#888',
        borderBottomWidth: 0.5,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    duiChangName: {
        color: GlobalStyles.nav_bar_color,
        fontSize: 18,
        textAlign: 'center',
        marginRight: 10,
    },
    newVersionMessage: {
        flex: 1,
        paddingHorizontal: 10,
        flexDirection: 'column',
        marginTop: 10
    },
    newVersionMessage2: {
        flex: 1,
        marginLeft: 30
    },
    showItemBackView: {
        flexDirection: 'column',
        paddingHorizontal: 15
    },
    upDataBotton: {
        backgroundColor: GlobalStyles.nav_bar_color,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
