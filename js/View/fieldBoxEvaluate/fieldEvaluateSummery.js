/**
 * 估价汇总
 */
import React, { Component } from 'react';
import { FlatList, Platform, StyleSheet, Text, View, ScrollView, TouchableOpacity, Image, Modal, Keyboard, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import Ionicons from 'react-native-vector-icons/Ionicons'
import BottomListModal from '../../common/BottomListModal'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_EVALUATERSUMMER_BACK_TYPE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView'
import { EMITTER_BOX_UPDATE_OK_TYPE, EMITTER_EVALUATERSUMMER_DEELETE_TYPE } from '../../common/EmitterTypes'
import FormData from '../../../node_modules/react-native/Libraries/Network/FormData'
import RNFS from 'react-native-fs';

const blame_selected_color = '#FFF4F4'  //船公司,货主或者自然锈蚀老化选择时的颜色
var IS_ADNROID = Platform.OS === 'android';
var cell_text_width = (GlobalStyles.screenWidth - 30) / 4
let networkDao = new NetworkDao()


export default class FieldEvaluateSummery extends Component {

    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params;
        console.log('====================================');
        console.log('我是进场数据：', this.params);
        console.log('====================================');
        let cntroNo = this.params.params.CntrNo
        let upLoadArr = this.params.upLoardArr
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        this.state = {
            data: new Array().concat(upLoadArr),   //flatlist数据
            blame1Color: 'white',    //船公司
            blame2Color: 'white',  //货主
            blame3Color: 'white',    //自然锈蚀老化
            depostArr: ['是', '否'],   //是否交押金的数组
            depostValue: '否',       //是否交押金
            isShowDoneView: true,    //是否显示下面的保存和提交的view
            indicatorAnimating: false, //菊花
            IndicatorAnimating: false,
            CntrNo: cntroNo,            //箱号
            shiperTotalMoney: this.params.ShiperMoney ? this.params.ShiperMoney : '',
            ownerTotalMoney: this.params.OwnerMoney ? this.params.OwnerMoney : ''
        }
    }

    onBackPress(e) {
        //NavigationUtil.goBack(this.props.navigation);
        return true;

    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
            //监听键盘弹出事件
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
                this.keyboardDidShowHandler.bind(this));
            //监听键盘隐藏事件
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
                this.keyboardDidHideHandler.bind(this));
        }

        let om = this.params.OwnerMoney
        let sm = this.params.ShiperMoney
        let ownerMoney = parseFloat(om)
        let shiperMoney = parseFloat(sm)
        if (shiperMoney > 0) {
            this.setState({ blame1Color: blame_selected_color, shiperTotalMoney: shiperMoney })
        }
        if (ownerMoney > 0) {
            this.setState({ blame2Color: blame_selected_color, ownerTotalMoney: ownerMoney })
        }


    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        //卸载键盘弹出事件监听
        if (this.keyboardDidShowListener != null) {
            this.keyboardDidShowListener.remove();
        }
        //卸载键盘隐藏事件监听
        if (this.keyboardDidHideListener != null) {
            this.keyboardDidHideListener.remove();
        }
    }

    //键盘弹出事件响应
    keyboardDidShowHandler(event) {
        this.setState({ isShowDoneView: false });
    }

    //键盘隐藏事件响应
    keyboardDidHideHandler(event) {
        this.setState({ isShowDoneView: true });
    }

    handleNumber(num) {
        return parseFloat(num) ? parseFloat(num) : 0
    }

    alertSureDown = () => {
        var navigation = this.props.navigation
        var tempArr = this.state.data
        let tempDic = tempArr[this.current_delete_index]
        let cellID = tempDic.currentID
        if (cellID.length > 0) {
            this.ClearCellImg(tempDic)
        }
        tempArr.splice(this.current_delete_index, 1)
        if (tempArr.length === 0) {
            this.setState({
                data: [],
                ownerTotalMoney: 0,
                shiperTotalMoney: 0
            })
        } else {
            let ownerM = 0  //货主的总钱数
            let shiperM = 0  //船公司的总钱数
            for (let i = 0; i < tempArr.length; i++) {
                let tempDic1 = tempArr[i]
                if (tempDic1.BlameValue == '货主') {
                    let o1 = this.handleNumber(tempDic1.TotalFee)
                    ownerM += o1
                } else if (tempDic1.BlameValue == '船公司') {
                    let s1 = this.handleNumber(tempDic1.TotalFee)
                    shiperM += s1
                }
            }
            this.setState({
                data: tempArr,
                ownerTotalMoney: ownerM,
                shiperTotalMoney: shiperM
            })
        }
        var dic = {}
        dic.index = this.current_delete_index
        DeviceEventEmitter.emit(EMITTER_EVALUATERSUMMER_DEELETE_TYPE, dic)
    }

    alertCancelDown = () => {
        console.log('点击了alert取消按钮')
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    //点击删除明细时删除服务器照片
    ClearCellImg(tempDic) {
        this.setState({ IndicatorAnimating: true })
        let secVal = this.getToken()
        let formData = new FormData()
        formData.append('CntrNo', this.params.params.CntrNo)
        formData.append('HandlingId', this.params.params.InHandlingId)
        formData.append('ID', tempDic.currentID)
        formData.append('Token', secVal)
        networkDao.uploadImgArr2('UploadDamagePhotos', formData)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    console.log('照片清除成功')
                }
            })
            .catch(error => {
                this.refs.toast.show('清除照片失败', 800)
            })
    }

    //获取本地照片数组
    // deleteFile() {
    //     let data = this.state.data
    //     let urlArr = []
    //     console.log(data);
    //     if (data && data.length > 0) {

    //         for (var i = 0; i < data.length; i++) {
    //             if (data[i].ImagesArr && data[i].ImagesArr.length > 0) {
    //                 for (var j = 0; j < data[i].ImagesArr.length; j++) {
    //                     let url = data[i].ImagesArr[j].path
    //                     urlArr.push(url)
    //                 }
    //             }
    //         }
    //         console.log(urlArr);
    //         if (urlArr && urlArr.length > 0) {
    //             let rnfsPath = Platform.OS === 'ios' ? RNFS.LibraryDirectoryPath : RNFS.ExternalStorageDirectoryPath
    //             let path = rnfsPath + '/Pictures'
    //             RNFS.unlink(path).then(() => {
    //                 console.log('照片删除成功', 800)
    //             })
    //             RNFS.mkdir(path).then((result) => {
    //                 console.log('result', result)
    //             }).catch((err) => {
    //                 console.log('err', err)
    //             })
    //                 .catch((e) => {
    //                     console.log(e)
    //                 })
    //         }
    //     }
    // }

    //去交押金
    depositAlertSureDown = () => {
        let dic = {}
        dic.viewKey = 'FieldEvaluateSummery'
        dic.handlingId = this.params.params.InHandlingId
        dic.cntrNo = this.state.CntrNo
        dic.shiperTotalMoney = this.state.shiperTotalMoney
        dic.ownerTotalMoney = this.state.ownerTotalMoney
        dic.depositType = 'IN'
        NavigationUtil.goPage(dic, 'RegisteFormView')
    }

    depositAlertCancelDown = () => {
        this.props.navigation.goBack(publicDao.FieldBoxStateView_NavigationKey)
        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
    }

    uploadParamsData(isTemporary) {
        
        let upArr = this.params.upLoardArr
        let para = this.params.params
        let isDepost = this.state.depostValue == '是' ? 'Y' : 'N'
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.IsTemporary = isTemporary
        param.IsHandInDeposit = isDepost
        param.HandlingId = para.InHandlingId

        let tempArr = []
        for (let i = 0; i < this.params.upLoardArr.length; i++) {
            let tempObj = this.params.upLoardArr[i]
            let isTemporary2 = tempObj.TempSave == '1' ? 'Y' : 'N'
            let tempDic = {}
            tempDic.ID = tempObj.currentID
            tempDic.Remark = tempObj.Remark
            tempDic.HandlingId = para.InHandlingId
            tempDic.CntrNo = para.CntrNo
            tempDic.CntrType = para.CntrType
            tempDic.CntrSize = para.CntrSize
            tempDic.CntrOperator = para.CntrOperator
            tempDic.RepairCode = tempObj.RepairCode ? tempObj.RepairCode : ''
            tempDic.RepairPart = tempObj.RepairPart ? tempObj.RepairPart : ''
            tempDic.RepairTimes = '1'
            tempDic.RepairMete = tempObj.b_rePairMete ? tempObj.b_rePairMete : ''
            tempDic.RepairName = tempObj.ProjectName ? tempObj.ProjectName : ''
            tempDic.CurrencyCode = tempObj.currencyCode ? tempObj.currencyCode : ''
            tempDic.MaterialFee = tempObj.MaterialFee ? tempObj.MaterialFee : ''
            tempDic.Labour = tempObj.WorkHours ? tempObj.WorkHours : ''
            tempDic.LabourFee = tempObj.WorkHoursFee ? tempObj.WorkHoursFee : ''
            tempDic.Amount = tempObj.TotalFee ? tempObj.TotalFee : ''
            tempDic.Responser = tempObj.BlameValue ? tempObj.BlameValue : ''
            tempDic.QuoteNo = ''
            tempDic.RepairMeans = tempObj.RepairMeans ? tempObj.RepairMeans : ''
            tempDic.DamageLocation = tempObj.BadPosition ? tempObj.BadPosition : ''
            tempDic.DamageType = tempObj.BadType ? tempObj.BadType : ''
            tempDic.RepairType = tempObj.RepairMethodCode ? tempObj.RepairMethodCode : ''
            tempDic.Length = tempObj.Long ? tempObj.Long : ''
            tempDic.Width = tempObj.Wide ? tempObj.Wide : ''
            tempDic.RepairNum = tempObj.Quantity ? tempObj.Quantity : ''
            tempDic.IsTemporary = isTemporary2
            tempDic.CreateTime = ''
            tempDic.CreateName = ''
            tempArr.push(tempDic)
        }

        param.HandlingQuoteList = tempArr
        let paramStr = JSON.stringify(param)
        console.log('paramStr',paramStr)
        networkDao.fetchPostNet('UploadHandlingQuoteByEnterYard', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    // this.deleteFile()
                    if (isTemporary == 'N') {
                        if (isDepost == 'Y') {
                            this.refs.DepostAlertView.showAlert();
                        } else {
                            this.props.navigation.goBack(publicDao.FieldBoxStateView_NavigationKey)
                            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                        }
                    } else {
                        this.props.navigation.goBack(publicDao.FieldBoxStateView_NavigationKey)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                    }
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.refs.toast.show(data._backmes)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })

                console.log('验箱估价上传失败', error)
            })
    }

    //暂存按钮点击事件
    temporaryBtnClick() {
        if (this.state.data && this.state.data.length > 0) {
            this.setState({ IndicatorAnimating: true })
            this.uploadParamsData('Y')
        } else {
            this.refs.toast.show('至少录入一条明细', 800)
        }
    }

    //提交按钮点击事件
    commitBtnClick() {

        for (let i = 0; i < this.params.upLoardArr.length; i++) {
            let tempObj = this.params.upLoardArr[i]
            if (tempObj.TempSave == '1') {
                this.refs.toast.show('暂存数据不可操作提交', 800)
                return
            }

        }
        if (this.state.data && this.state.data.length > 0) {
            this.setState({ IndicatorAnimating: true })
            this.uploadParamsData('N')
        } else {
            this.refs.toast.show('至少录入一条明细', 800)
        }
    }

    //分割线
    renderLine() {
        return <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //箱号,右边点击添加cell
    renderTitleView() {
        return <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 45, paddingLeft: 10 }}>
            <Text style={styles.textStyle}>箱号</Text>
            <Text style={[styles.textStyle, { color: '#0877DE', marginLeft: 25, fontSize: 16 }]}>{this.state.CntrNo}</Text>
            <TouchableOpacity style={{ position: 'absolute', right: 15, top: 12.5, width: 20, height: 20 }}
                onPress={() => {
                    //右上角添加cell按钮点击事件'
                    var dic = {}
                    dic.index = -1
                    dic.new = 'true'
                    dic.jsonD = this.state.data
                    DeviceEventEmitter.emit(EMITTER_EVALUATERSUMMER_BACK_TYPE, dic);
                    NavigationUtil.goBack(this.props.navigation)

                }}>
                <Image
                    style={{ width: 20, height: 20 }}
                    source={require('../../../resource/add.png')}
                />
            </TouchableOpacity>
        </View>

    }
    //flatList的cell
    renderItem(data) {
        // console.log('data:', data)
        var item = data.item
        let cellColor = item.TempSave == '1' ? '#FFF7CF' : 'white'
        return <TouchableOpacity
            activeOpacity={1}
            style={{ height: 75, backgroundColor: cellColor, paddingLeft: 10 }}
            onPress={() => {
                console.log('编辑cell按钮点击事件')
                var dic = {}
                dic.index = data.index
                dic.new = 'false'
                dic.jsonD = this.state.data
                DeviceEventEmitter.emit(EMITTER_EVALUATERSUMMER_BACK_TYPE, dic);
                NavigationUtil.goBack(this.props.navigation)
            }}
        >
            <View style={{ flexDirection: 'row', height: 35, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', height: 35, marginTop: 10 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', height: 15, width: 15, backgroundColor: '#BC1920' }}>
                        <Text style={{ color: 'white', width: 15, height: 15, fontSize: 12, textAlign: 'center' }}>{data.index + 1}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Text style={{ marginLeft: 10, height: 15, fontSize: 13, color: '#666666' }}>长-宽-量 :  </Text>
                        <Text style={{ height: 15, fontSize: 13, color: GlobalStyles.gray_text_color }}>{item.Long + '-' + item.Wide + '-' + item.Quantity}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: 35, width: 60, marginTop: 10 }}>
                    <TouchableOpacity
                        style={{ height: 32, width: 25, marginLeft: 30 }}
                        onPress={() => {
                            console.log('删除cell按钮点击事件')
                            this.current_delete_index = data.index
                            this.refs.AlertView.showAlert();
                        }}>
                        <Image
                            style={{ height: 18, width: 14 }}
                            source={require('../../../resource/dele.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ flexDirection: 'row', height: 39 }}>
                <Text style={styles.cellInput}>{item.RepairPart}</Text>
                <Text style={styles.cellInput}>{item.RepairMeans}</Text>
                <Text style={styles.cellInput}>{item.BadPosition}</Text>
                <Text style={styles.cellInput}>{item.BadType}</Text>
            </View>
            {this.renderLine()}
        </TouchableOpacity >
    }


    //船公司,货主,自然锈蚀老化
    renderBlameView() {

        let om = Number(this.state.ownerTotalMoney).toFixed(2)
        let sm = Number(this.state.shiperTotalMoney).toFixed(2)

        return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, height: 95, backgroundColor: "white" }}>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame1Color }]}>
                <Text style={styles.blameTitleText}>船公司</Text>
                <Text style={styles.blameValueText}>{'￥ ' + sm}</Text>
            </View>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame2Color }]}>
                <Text style={styles.blameTitleText}>货主</Text>
                <Text style={styles.blameValueText}>{'￥ ' + om}</Text>
            </View>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame3Color }]}>
                <Text style={styles.blameTitleText}>自然锈蚀老化</Text>
                <Text style={styles.blameValueText}>￥ 0</Text>
            </View>

        </View>
    }

    //交押金
    renderDepositView() {
        return <View style={{ height: 60, backgroundColor: 'white', marginTop: 10 }}>
            <TouchableOpacity
                onPress={() => {
                    this.refs.selectDeposiModal.show();
                }}
            >
                <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white' }}>
                    <Text style={[styles.depostText, { fontSize: 17, }]}>交押金</Text>
                    <Text style={[styles.depostText, { fontSize: 16, color: '#666666' }]}>{this.state.depostValue}</Text>
                    <View style={{ position: 'absolute', right: 20, top: 15, width: 20, height: 20 }}>
                        <Ionicons
                            name={'ios-arrow-down'}
                            size={20}
                            style={{
                                alignSelf: 'center',
                                color: '#666666',
                            }} />
                    </View>
                </View>
            </TouchableOpacity>
            <View style={{ height: 10, width: GlobalStyles.screenWidth, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }

    //暂存和提交
    renderBottomView() {
        return this.state.isShowDoneView ? <View style={styles.bottomBackView}>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.bottomTouchable, { backgroundColor: 'white' }]}
                onPress={() => {
                    this.temporaryBtnClick()
                }}
            >
                <Text style={[styles.bottomText, { color: GlobalStyles.nav_bar_color }]}>暂  存</Text>
            </TouchableOpacity>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.bottomTouchable, { backgroundColor: GlobalStyles.nav_bar_color }]}
                onPress={() => {
                    //提交按钮点击事件
                    this.commitBtnClick()
                }}
            >
                <Text style={[styles.bottomText, { color: 'white' }]}>提  交</Text>
            </TouchableOpacity>
        </View> : null;
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'估价汇总'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            // leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                {navigationBar}
                <ScrollView
                    bounces={false}
                    style={{ flex: 1, marginBottom: 50 }}
                >
                    <View style={styles.flatlistBackView}>
                        {this.renderTitleView()}
                        {this.renderLine()}
                        <FlatList
                            data={this.state.data}
                            renderItem={data => this.renderItem(data)}
                            keyExtractor={(item, index) => index.toString()}
                            bounces={false}
                        />
                    </View>
                    {this.renderBlameView()}
                    {this.renderDepositView()}
                    <BottomListModal
                        ref="selectDeposiModal"
                        data={this.state.depostArr}
                        callBack={(selectData) => {
                            this.setState({ depostValue: selectData })
                        }}
                    />
                </ScrollView>
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="AlertView"
                    TitleText="确定删除当前数据?"
                    DesText="删除后将不可恢复"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDown}
                    alertCancelDown={this.alertCancelDown}
                />
                <AlertView
                    ref="DepostAlertView"
                    TitleText="是否去交押金?"
                    // DesText=" "
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.depositAlertSureDown}
                    alertCancelDown={this.depositAlertCancelDown}
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                {this.state.IndicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}>
                            <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                            <View style={{
                                backgroundColor: '#333',
                                borderRadius: 3,
                                marginTop: 10,
                                paddingVertical: 10,
                                width: 130,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                            </View>
                        </View>

                    </Modal>
                )}
                {this.renderBottomView()}
            </SafeAreaViewPlus>

        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    flatlistBackView: {
        backgroundColor: GlobalStyles.nav_bar_color,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    textStyle: {             //文字通用样式
        height: 45,
        lineHeight: 45,
        fontSize: 17,
    },
    blameTouchableStyle: {  //船公司,货主,自然锈蚀老化的touchable
        height: 70,
        width: 105,
        borderWidth: 1,
        borderColor: '#BC1920',
        marginTop: 12.5,
    },
    blameTitleText: {        //船公司,货主,自然锈蚀老化文字
        height: 30,
        lineHeight: 30,
        textAlign: 'center',
        fontSize: 14,
        paddingTop: 5,      //使文字稍微往下一点
    },
    blameValueText: {        //船公司,货主,自然锈蚀老化的value
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        fontSize: 18,
        color: GlobalStyles.nav_bar_color,
        paddingBottom: 10,   //使文字稍往上一点
    },
    depostText: {        //交押金
        marginLeft: 20,
        height: 50,
        lineHeight: 50,
        width: 70,
    },
    bottomBackView: {
        position: 'absolute',
        bottom: GlobalStyles.is_iphoneX ? 34 : 0,
        height: 50,
        flexDirection: 'row',
        zIndex: 119,
    },
    bottomTouchable: {
        height: 50,
        width: GlobalStyles.screenWidth / 2,
        borderColor: GlobalStyles.nav_bar_color,
        borderWidth: 1,
    },
    bottomText: {
        height: 50,
        lineHeight: 50,
        width: GlobalStyles.screenWidth / 2,
        textAlign: 'center',
        fontSize: 18,
    },
    cellInput: {
        color: '#666666',
        height: 39,
        width: cell_text_width,
        color: 'black',
        fontSize: 17,
    }

});
