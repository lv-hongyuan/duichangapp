/**
 * 场内验箱的残损估价页面
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Modal, Keyboard, SafeAreaView, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SelectImageView from '../../common/SelectImageView'
import ImageViewer from 'react-native-image-zoom-viewer';   //类似微信朋友圈浏览图片的效果
import BoxStateBadViewUtil from '../../util/BoxStateBadViewUtil';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import BottomListModal from '../../common/BottomListModal'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import NetworkDao from '../../dao/NetworkDao'
import DaoUtil from '../../util/DaoUtil'
import publicDao from '../../dao/publicDao';
import RepariCodeModal from '../boxEvaluate/RepairCodeModal'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_BOXSTATEBADDETAILVIEW_DONE_TYPE } from '../../common/EmitterTypes';
import RepairUnitOrModeModal from '../boxEvaluate/RepairUnitOrModeModal'
import BadTypeOrMoneyTypeModal from '../boxEvaluate/BadTypeOrMoneyTypeModal'
import find from 'lodash/find'
import FormData from 'react-native/Libraries/Network/FormData'
import AlertView from '../../tools/alertView';
import RNFS from 'react-native-fs';


var IS_ADNROID = Platform.OS === 'android';
let networkDao = new NetworkDao()


export default class FieldBoxStateDetailView extends Component {
    constructor(props) {
        super(props)
        this.jsonD = this.props.navigation.state.params;
        this.params = this.jsonD.item;
        console.log('jsonD:', this.jsonD)
        console.log('params:', this.params)
        this.ID = this.params.ID ? this.params.ID : ''
        this.repairType = this.params.RepairType ? this.params.RepairType : ''
        this.CreateName = this.params.CreateName ? this.params.CreateName : ''
        this.CreateTime = this.params.CreateTime ? this.params.CreateTime : ''
        this.TempSaves = this.params.IsTemporary ? this.params.IsTemporary : ''
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        this.TempSave = ''
        this.currencyCode = 'RMB'       //货币code
        if (this.TempSaves == 'Y') {
            this.TempSave = '1'
        } else if (this.TempSaves == 'N') {
            this.TempSave = '0'
        }
        this.state = {
            data: [],       //保存图片数据
            netImgArr: [],      //网络图片数据
            visible: false,  //是否显示大图展示的modal
            currentIndex: 0,//当前显示第几个图片
            mainEntryMode: '',   //主进场方式
            secondEntryMode: '', //子进场方式
            blameValue: this.params.Responser ? this.params.Responser : '',  //责任方默认值
            repairMethod: '',//维修类型的默认值
            repairCode: this.params.RepairCode ? this.params.RepairCode : '',      //维修代码默认值
            repairUnit: this.params.RepairPart ? this.params.RepairPart : '',      //维修部件默认值
            repairMode: this.params.RepairMeans ? this.params.RepairMeans : '',      //维修方式
            projectName: this.params.RepairName ? this.params.RepairName : '',     //项目名称  
            badPosition: this.params.DamageLocation ? this.params.DamageLocation : '',     //残损位置
            badType: this.params.DamageType ? this.params.DamageType : '',         //损坏类型
            long: (this.params.Length || this.params.Length == 0) ? '' + this.params.Length : '',            //长
            wide: (this.params.Width || this.params.Width == 0) ? '' + this.params.Width : '',            //宽
            quantity: (this.params.RepairNum || this.params.RepairNum == 0) ? '' + this.params.RepairNum : '',        //数量
            repairCount: '',     //维修次数
            selectModalArr: [],      //点击cell加载modal时的数据
            blameModalArr: [], //点击责任方的modal
            blameNameArr: [],    //保存责任名的数组(显示的数据),blameModalArr包含责任名和责任代码
            repairMethodArr: [],     //维修类型数组
            repairModalArr: [],  //维修方式的完整数据
            repariNameArr: [],   //维修方式的显示数据
            projectNameArr: [],  //点击项目名称的modal
            isShowDoneView: true,    //是否显示下面的保存和完成的view
            indicatorAnimating: false, //菊花
            IndicatorAnimating: false,
            IndicatorAnimatingText: '',
            repairCodeArr: [],       //维修代码详细信息    
            upLoardArr: [],          //整个页面数据
            uploadDic: {},            //外层大字典
            uploadMaterialArr: [],     //物料费数组
            currency: '人民币',           //货币
            currencyArr: [],             //货币类型数组
            workHours: this.params.Labour ? '' + this.params.Labour : '',               //工时
            workHoursFee: this.params.LabourFee ? '' + this.params.LabourFee : '',             //工时费
            materialFee: this.params.MaterialFee ? '' + this.params.MaterialFee : '',             //物料费
            totalFee: this.params.Amount ? '' + this.params.Amount : '',  //总费用              
            repairUnitArr: [],           //维修部件数组
            repairModeArr: [],           //维修方式数组
            badTypeArr: [],              //损坏类型数组
            bigImgData: '',               //大图数据
            Remark: this.params.Remark ? this.params.Remark : '',            //备注信息  
        }
    }

    isRepeat() {
        let dataArr = this.jsonD.data;
        let index = this.jsonD.index;
        let tempDic = this.getViewDic()
        let dataArr2 = this.state.upLoardArr
        console.log(dataArr2);
        console.log(dataArr);
        let isR = false
        if (dataArr2 && dataArr2.length > 0) {
            for (let i = 0; i < dataArr2.length; i++) {
                let tempJ = dataArr2[i]
                if (tempJ.TempSave != '0') {
                    continue
                }
                if (tempJ.RepairCode == tempDic.RepairCode && tempJ.ProjectName == tempDic.ProjectName && tempJ.BlameValue == tempDic.BlameValue && tempJ.BadPosition == tempDic.BadPosition && tempJ.BadType == tempDic.BadType) {
                    isR = true
                    break
                }
            }
            if (isR) {
                return isR
            }
        }
        if (dataArr && dataArr.length > 0) {
            for (let i = 0; i < dataArr.length; i++) {
                let tempJ = dataArr[i]
                if (i == index || tempJ.IsTemporary != 'N') {
                    continue
                }
                if (tempJ.RepairCode == tempDic.RepairCode && tempJ.RepairName == tempDic.ProjectName && tempJ.Responser == tempDic.BlameValue && tempJ.DamageLocation == tempDic.BadPosition && tempJ.DamageType == tempDic.BadType) {
                    isR = true
                    break
                }
            }
            return isR
        }
    }

    onBackPress(e) {
        //NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    //数据更新至数组
    upDataArr(item) {
        let tempDic = this.getViewDic()
        tempDic.TempSave = item
        tempDic.ID = this.ID
        tempDic.CreateName = this.CreateName
        tempDic.CreateTime = this.CreateTime
        tempDic.ImagesArr = this.state.data
        this.state.upLoardArr.push(tempDic)
    }

    uploadImage2(isLast) {
        let tempDic = this.getViewDic()
        console.log(tempDic);
        console.log(this.props.navigation.state);
        if (this.state.data && this.state.data.length > 0) {
            
            let oriImgArr = this.state.data.concat()
            oriImgArr = oriImgArr.map((oriImag) => {
                let netImgArr = this.state.netImgArr.concat()
                let netImag = find(netImgArr, (netIma) => netIma.uri === oriImag.uri)
                return netImag ? netImag : oriImag
            })
            let formData = new FormData()
            let secVal = this.getToken()
            formData.append('CntrNo', this.params.CntrNo)
            formData.append('HandlingId', this.params.HandlingId)
            formData.append('ID', this.ID)
            formData.append('Token', secVal)

            for (let i = 0; i < oriImgArr.length; i++) {
                let tempDic = oriImgArr[i]
                let a = tempDic.uri;
                let arr = a.split('/');
                if (tempDic.localUri) {
                    formData.append('file', { uri: tempDic.localUri, name: arr[arr.length - 1], type: 'image/jpeg' })
                } else {
                    formData.append('file', { uri: tempDic.uri, name: arr[arr.length - 1], type: 'image/jpeg' })
                }
            }
            console.log(formData);
            networkDao.uploadImgArr2('UploadDamagePhotos', formData)
                .then(data => {
                    this.setState({ IndicatorAnimating: false })
                    if (data._backcode == '201') {
                        if (isLast == '1') {
                            if ((this.state.totalFee && this.state.totalFee.length > 0) && (this.state.data && this.state.data.length > 0)) {
                                this.upDataArr('0')
                            } else {
                                this.upDataArr('1')
                            }
                            NavigationUtil.goBack(this.props.navigation)
                            DeviceEventEmitter.emit(EMITTER_BOXSTATEBADDETAILVIEW_DONE_TYPE, this.state.upLoardArr)
                        }
                        if (isLast == '0') {
                            this.upDataArr('1')
                            this.resetViewDic()
                            this.clickToScroll()
                            this.getCurrentID()
                            if (this.state.mainEntryMode == '一般进场' && this.state.secondEntryMode == '进口空箱') {
                                this.setState({
                                    blameValue: '船公司'
                                })
                                this.blameCode = '船公司'
                            }
                        }

                        if (isLast == '2') {
                            if(this.state.totalFee && this.state.totalFee.length > 0){
                                this.upDataArr('0')
                                this.resetViewDic()
                                this.clickToScroll()
                                this.getCurrentID()
                                if (this.state.mainEntryMode == '一般进场' && this.state.secondEntryMode == '进口空箱') {
                                    this.setState({
                                        blameValue: '船公司'
                                    })
                                    this.blameCode = '船公司'
                                }
                            }
                        }

                        for (let i = 0; i < this.state.netImgArr.length; i++) {
                            let dic = this.state.netImgArr[i].uri
                            let a = dic.substring(dic.lastIndexOf("/") + 1);
                            let urii = RNFS.ExternalStorageDirectoryPath + '/DCIM/' + a
                            networkDao.deleteLocalImage(dic)
                            networkDao.deleteLocalImage2(urii)
                        }
                    } else {
                        this.setState({ IndicatorAnimating: false })
                        this.refs.toast.show('图片上传失败', 800)
                        console.log('图片上传失败 800')
                    }
                })
                .catch(error => {
                    this.setState({ IndicatorAnimating: false })
                    console.log('图片上传失败2:', error)
                    this.refs.toast.show('图片上传失败', 800)
                })
        } else {
            let formData = new FormData()
            formData.append('CntrNo', this.params.CntrNo)
            formData.append('HandlingId', this.params.HandlingId)
            formData.append('ID', this.ID)
            formData.append('Token', secVal)
            networkDao.uploadImgArr2('UploadDamagePhotos', formData)
                .then(data => {
                    this.setState({ IndicatorAnimating: false })
                    if (data._backcode == '201') {
                        console.log('照片清除成功')
                    }
                })
                .catch(error => {
                    this.setState({ IndicatorAnimating: false })
                    console.log('清除照片失败')
                })

            if (isLast == '1') {
                if ((this.state.repairCode && this.state.repairCode.length > 0) ||
                    (this.state.repairUnit && this.state.repairUnit.length > 0) ||
                    (this.state.repairMode && this.state.repairMode.length > 0) ||
                    (this.state.projectName && this.state.projectName.length > 0) ||
                    (this.state.badPosition && this.state.badPosition.length > 0) ||
                    (this.state.badType && this.state.badType.length > 0)) {
                    let tempDic = this.getViewDic()
                    tempDic.TempSave = '1'
                    tempDic.ID = this.ID
                    tempDic.CreateName = this.CreateName
                    tempDic.CreateTime = this.CreateTime
                    this.state.upLoardArr.push(tempDic)
                }
                NavigationUtil.goBack(this.props.navigation)
                DeviceEventEmitter.emit(EMITTER_BOXSTATEBADDETAILVIEW_DONE_TYPE, this.state.upLoardArr)
            } else if ((this.state.repairCode && this.state.repairCode.length > 0) ||
                (this.state.repairUnit && this.state.repairUnit.length > 0) ||
                (this.state.repairMode && this.state.repairMode.length > 0) ||
                (this.state.projectName && this.state.projectName.length > 0) ||
                (this.state.badPosition && this.state.badPosition.length > 0) ||
                (this.state.badType && this.state.badType.length > 0)) {
                let tempDic = this.getViewDic()
                tempDic.TempSave = '1'
                tempDic.ID = this.ID
                tempDic.CreateName = this.CreateName
                tempDic.CreateTime = this.CreateTime
                this.state.upLoardArr.push(tempDic)
                this.resetViewDic()
                this.clickToScroll()
                this.getCurrentID()
                if (this.state.mainEntryMode == '一般进场' && this.state.secondEntryMode == '进口空箱') {
                    this.setState({
                        blameValue: '船公司'
                    })
                    this.blameCode = '船公司'
                }
            } else {
                this.refs.toast.show('请填写完整数据', 800)
            }
        }
    }


    async saveImgToLocal(dataA) {
        let netArr = []
        for (let i = 0; i < dataA.length; i++) {
            let url = dataA[i]
            await networkDao.downloadImage(url)
                .then(data => {
                    console.log(data);
                    let dic1 = {}
                    dic1.uri = data
                    dic1.localUri = data
                    netArr.push(dic1)
                })
                .catch(error => {
                    // this.refs.permissionsView.showAlert();
                    console.log('图片下载失败:', error)
                    this.refs.toast.show('图片下载失败', 800)
                })
        }
        if (netArr && netArr.length > 0) {
            this.setState({ data: netArr, netImgArr: netArr })
        }
        this.setState({ IndicatorAnimating: false })
    }

    //图片下载
    downloadImg() {
        this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '正在获取数据' })
        let param = {}
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        param.CntrNo = this.params.CntrNo
        param.ID = this.ID
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('DownloadPhotos', paramStr)
            .then(data => {
                let netArr = []

                if (data._backcode == '200') {
                    let tempArr = data.PhotoList
                    this.saveImgToLocal(tempArr)
                } else if (data._backcode == '303') {
                    this.setState({ IndicatorAnimating: false })
                    console.log('没有网络图片');
                } else {
                    this.setState({ IndicatorAnimating: false })
                    this.refs.toast.show('图片下载失败', 800)
                }

            })
            .catch(error => {
                console.log(error);
                this.refs.toast.show('图片下载失败', 800)
            })

    }

    //获取明细主键
    getCurrentID() {
        let param = {}
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetNextCommonID', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    this.ID = data.ID
                    this.downloadImg()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取明细主键失败', 800)
                    NavigationUtil.goBack(this.props.navigation)
                }
            })
            .catch(error => {
                console.log('获取明细主键失败:', error)
            })
    }

    //获取进场方式数据
    getEntryType() {
        let param = {}
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetInYardType', paramStr)
            .then(data => {
                // console.log('==data:', data)
                if (data._backcode == '200') {
                    this.setState({ mainEntryMode: data.OrderType, secondEntryMode: data.OrderSubType })
                    if (data.OrderType == '一般进场' && data.OrderSubType == '进口空箱') {
                        this.setState({
                            blameValue: '船公司'
                        })
                        this.blameCode = '船公司'
                    }
                    this.getBlameArrData()
                } else if (data._backcode == '500') {
                    // NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取进场方式失败', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                // console.log('获取进场方式失败error:', error)
                this.refs.toast.show('获取进场方式失败', 800)
            })
    }

    //获取责任方数组
    getBlameArrData() {
        let param = {}
        param.Token = secVal
        param.Type = 'RESPONSER_TYPE'
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetBasicInformation', paramStr)
            .then(data => {
                //console.log('==data:', data)
                if (data._backcode == '200') {
                    var temp = []
                    for (var i = 0, len = data.BasicInformationList.length; i < len; i++) {
                        var dic = data.BasicInformationList[i]
                        temp.push(dic.Name)
                        if (this.params.Responser) {
                            if (this.params.Responser == dic.Name) {
                                this.blameCode = dic.Code
                            }
                        }
                    }
                    this.setState({ blameNameArr: temp, blameModalArr: data.BasicInformationList, selectModalArr: temp })
                    this.getRepairMethodArrData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取责任方失败', 800)
                }
            })
            .catch(error => {
                // console.log('获取责任方失败error:', error)
                this.refs.toast.show('获取责任方失败', 800)
            })
    }

    //获取维修类型数组
    getRepairMethodArrData() {
        let param = {}
        param.Token = secVal
        param.Type = 'REPAIR_TYPE'
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetBasicInformation', paramStr)
            .then(data => {
                //console.log('==data:', data)
                if (data._backcode == '200') {
                    var temp = []
                    for (var i = 0, len = data.BasicInformationList.length; i < len; i++) {
                        var dic = data.BasicInformationList[i]
                        temp.push(dic.Name)
                        if (this.params.RepairType == dic.Code) {
                            this.repairType = dic.Code
                            this.setState({ repairMethod: dic.Name })
                        }

                    }

                    this.setState({ repariNameArr: temp, repairMethodArr: data.BasicInformationList, selectModalArr: temp })
                    this.getMoneyTypeArrData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取维修类型失败', 800)
                }
            })
            .catch(error => {
                // console.log('获取维修类型失败error:', error)
                this.refs.toast.show('获取维修类型失败', 800)
            })
    }

    //获取货币类型数据
    getMoneyTypeArrData() {
        let param = {}
        param.Token = secVal
        param.Type = 'CURRENCY_TYPE'
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetBasicInformation', paramStr)
            .then(data => {
                //console.log('===data:', data)
                if (data._backcode == '200') {

                    for (let i = 0; i < data.BasicInformationList.length; i++) {
                        let dic = data.BasicInformationList[i]
                        if (this.params.CurrencyCode) {
                            this.currencyCode = this.params.CurrencyCode
                            if (this.currencyCode == dic.Code) {
                                this.setState({ currency: dic.Name })
                            }
                        }
                    }
                    this.setState({ currencyArr: data.BasicInformationList })
                    this.getRepairCodeData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取货币类型失败', 800)
                }
            })
            .catch(error => {
                // console.log('获取货币类型失败error:', error)
                this.refs.toast.show('获取货币类型失败', 800)
            })
    }

    //获取维修项目代码下拉列表数据
    getRepairCodeData() {
        let param = {}
        param.Token = secVal
        param.CntrOperator = this.params.CntrOperator
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetRepairCode', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    this.setState({ repairCodeArr: data.RepairList })
                    this.getRepairUnitArrData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取维修代码失败', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                // console.log('error:', error)
                this.refs.toast.show('获取维修代码失败', 800)
            })
    }

    //获取维修部件数组
    getRepairUnitArrData() {
        let param = {}
        param.Token = secVal
        param.CntrOperator = this.params.CntrOperator
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetCntrPartList', paramStr)
            .then(data => {

                if (data._backcode == '200') {
                    this.setState({ repairUnitArr: data.CntrPartList })
                    this.getRepairModeArrData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取维修部件错误', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                // console.log('获取维修部件错误:', error)
                this.refs.toast.show('获取维修部件错误', 800)
            })
    }

    //获取维修方式数组
    getRepairModeArrData() {
        let param = {}
        param.Token = secVal
        param.CntrOperator = this.params.CntrOperator
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetRepairTypeList', paramStr)
            .then(data => {

                if (data._backcode == '200') {
                    this.setState({ repairModalArr: data.RepairTypeList })
                    this.getBadTypeArrData()
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取维修方式失败', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                // console.log('获取维修方式错误:', error)
                this.refs.toast.show('获取维修方式失败', 800)
            })
    }

    //获取损坏类型
    getBadTypeArrData() {
        let param = {}
        param.Token = secVal
        param.Type = 'CNTR'
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetDamageTypeList', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    this.setState({ badTypeArr: data.DamageTypeList })
                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取损坏类型失败', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                // console.log('获取损坏类型错误:', error)
                this.refs.toast.show('获取损坏类型失败', 800)
            })
    }



    //根据维修部件和维修方式的输入筛选维修代码数组
    getPickArr() {
        var tempArr = []
        var unit = this.state.repairUnit
        var mode = this.state.repairMode
        for (var i = 0, len = this.state.repairCodeArr.length; i < len; i++) {
            var dic = this.state.repairCodeArr[i]
            var parts = dic.RepairPart
            var means = dic.RepairMeans
            if (unit.length > 0 && mode.length == 0) {
                if (parts.indexOf(unit) > -1) {
                    tempArr.push(dic)
                }
            } else if (unit.length == 0 && mode.length > 0) {
                if (means.indexOf(mode) > -1) {
                    tempArr.push(dic)
                }
            } else if (unit.length > 0 && mode.length > 0) {
                if (parts.indexOf(unit) > -1 && means.indexOf(mode) > -1) {
                    tempArr.push(dic)
                }
            }
        }

        return tempArr
    }

    //根据输入的维修部件筛选维修方式
    getRepairModeArrWithRepairunit() {
        var tempArr = []
        var unit = this.state.repairUnit

        for (var i = 0, len = this.state.repairCodeArr.length; i < len; i++) {
            var dic = this.state.repairCodeArr[i]
            var parts = dic.RepairPart
            if ((!unit) || (unit.length == 0)) {
                tempArr = this.state.repairCodeArr
                break
            } else {
                if (parts.indexOf(unit) > -1) {
                    tempArr.push(dic)
                }
            }

        }
        console.log('维修方式:', tempArr)
        var result = [];
        var obj = {};
        for (var i = 0; i < tempArr.length; i++) {
            if (!obj[tempArr[i].RepairMeans]) {
                result.push(tempArr[i]);
                obj[tempArr[i].RepairMeans] = true;
            }

        }
        return result
    }

    hangleNumber(num) {
        // num = num == '' ? '0' : num      //如果是空串就赋值为0
        // num = parseFloat(num)
        // num = num == 0 ? 1 : num         //如果是0就赋值为1 
        //return num            
        return parseFloat(num) ? parseFloat(num) : 1
    }

    hangleNumber2(num) {
        return parseFloat(num) ? parseFloat(num) : 0
    }


    //计算物料费率
    calcuteFee() {
        let tempBadArr = []
        for (let i = 0, len = this.state.badTypeArr.length; i < len; i++) {
            let dic = this.state.badTypeArr[i]
            tempBadArr.push(dic.DamageCode)
        }
        if (tempBadArr.indexOf(this.state.badType) >= 0) {

        } else {  //输入的损坏类型不存在
            this.refs.toast.show('请输入正确的损坏类型', 800)
            this.setState({ badType: '' })
            return
        }


        let dic2 = this.getViewDic()
        var repairCode = dic2.RepairCode
        //console.log('repairCode:', repairCode)
        var blame = dic2.BlameValue      //责任方
        let long = this.hangleNumber(dic2.Long)     //长
        console.log('long is :', long)
        let wide = this.hangleNumber(dic2.Wide)
        let repairMete = this.hangleNumber(dic2.Quantity)       //数量
        let repairTimes = this.hangleNumber(dic2.RepairTimes)   //维修次数
        var b_rePairMete = long * wide * repairMete

        //console.log('liang is :', b_rePairMete)

        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.RepairCode = repairCode
        let paramStr = JSON.stringify(param)

        networkDao.fetchPostNet('GetMaterialRate', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    let dic3 = {} //带有物料费用的字典
                    var tempArr = data.MaterIalRateList //费率数组
                    if (tempArr.length == 1) {     //不计修补量数据
                        console.log('不计修补量费率')
                        dic3 = tempArr[0]

                        var SectionMin = parseFloat(dic3.SectionMin)
                        var SectionMax = parseFloat(dic3.SectionMax)
                        if (b_rePairMete > SectionMin && b_rePairMete <= SectionMax) {

                            var MaterialFee = (parseFloat(dic3.BaseAmount)) * repairTimes
                            //console.log('物料费:', MaterialFee)
                            MaterialFee = MaterialFee.toFixed(3)
                            dic3.MaterialFee = MaterialFee
                            dic3.multiStep = 0
                            dic3.RepairCode = repairCode

                            dic3.BlameValue = this.state.blameValue      //责任方
                            dic3.RepairPart = this.state.repairUnit      //维修组件
                            dic3.RepairMethodCode = this.repairType    //维修类型
                            dic3.RepairMeans = this.state.repairMode     //维修方式
                            dic3.ProjectName = this.state.projectName    //项目名称
                            dic3.BadPosition = this.state.badPosition    //残损位置
                            dic3.BadType = this.state.badType            //损坏类型
                            dic3.Long = this.state.long                  //长
                            dic3.Wide = this.state.wide                  //宽
                            dic3.Quantity = this.state.quantity          //量
                            dic3.b_rePairMete = b_rePairMete            //修补量
                            dic3.ID = this.ID                   //页面数据的唯一标识符
                            dic3.CreateName = this.CreateName
                            dic3.CreateTime = this.CreateTime


                            //获取工时
                            this.getLabourData(dic3)

                        } else {
                            this.setState({ indicatorAnimating: false })
                            this.refs.toast.show('联系管理员维护物料费率', 1200)
                            return
                        }

                    } else {
                        var materialRate = -1

                        for (var i = 0; i < tempArr.length; i++) {
                            var dic4 = tempArr[i]
                            var SectionMin = parseFloat(dic4.SectionMin)
                            var SectionMax = parseFloat(dic4.SectionMax)
                            if (b_rePairMete > SectionMin && b_rePairMete <= SectionMax) {
                                materialRate = parseFloat(dic4.MaterialRate)
                                dic3 = dic4
                            }
                        }
                        if (materialRate >= 0) {
                            var multiSection = b_rePairMete - parseFloat(dic3.SectionMin)
                            var multiStep = 0
                            if (parseFloat(dic3.SectionGrade) > 0) {
                                multiStep = multiSection / parseFloat(dic3.SectionGrade)
                                multiStep = Math.ceil(multiStep)    //向上取整
                                //console.log('递增次数:', multiStep)
                            }
                            var MaterialFee = (parseFloat(dic3.BaseAmount) + multiStep * materialRate) * repairTimes
                            MaterialFee = MaterialFee.toFixed(3)
                            //console.log('物料费:', MaterialFee)
                            dic3.MaterialFee = MaterialFee
                            dic3.multiStep = multiStep
                            dic3.RepairCode = repairCode

                            dic3.BlameValue = this.state.blameValue      //责任方
                            dic3.RepairPart = this.state.repairUnit      //维修组件
                            dic3.RepairMethodCode = this.repairType    //维修类型
                            dic3.RepairMeans = this.state.repairMode     //维修方式
                            dic3.ProjectName = this.state.projectName    //项目名称
                            dic3.BadPosition = this.state.badPosition    //残损位置
                            dic3.BadType = this.state.badType            //损坏类型
                            dic3.Long = this.state.long                  //长
                            dic3.Wide = this.state.wide                  //宽
                            dic3.Quantity = this.state.quantity          //量
                            dic3.b_rePairMete = b_rePairMete
                            dic3.ID = this.ID                   //页面数据的唯一标识符
                            dic3.CreateName = this.CreateName
                            dic3.CreateTime = this.CreateTime

                            //获取工时
                            this.getLabourData(dic3)
                        } else {
                            this.setState({ indicatorAnimating: false })
                            this.refs.toast.show('联系管理员维护物料费率', 1200)
                        }

                    }

                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    //NavigationUtil.resetToLoginView()
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backMessage, 1200)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('物料费率error:', error)

            })

    }

    //获取工时
    getLabourData(materialDic) {
        let param = {}
        param.Token = secVal
        param.Responser = this.blameCode
        param.Customer = this.params.CntrOperator
        param.WorkerType = this.repairType
        let paramStr = JSON.stringify(param)

        // var labourData = new Promise((resolve, reject) => {
        networkDao.fetchPostNet('GetLabourRate', paramStr)
            .then(data => {

                if (data._backcode == '200') {
                    var tempArr = data.LabourRateList
                    if (tempArr.length == 1) {
                        var tempDic = tempArr[0]
                        var labourRate = parseFloat(tempDic.Rate)   //每个小时的费率
                        var baseL = parseFloat(materialDic.BaseLabour) //工时基数
                        var lMultiplier = parseFloat(materialDic.LabourMultiplier) //工时乘数
                        var labourHour = baseL + lMultiplier * materialDic.multiStep   //工时
                        var labourFee = labourHour * labourRate //工时费
                        labourFee = labourFee.toFixed(3)
                        materialDic.LabourHour = labourHour
                        materialDic.LabourFee = labourFee
                        materialDic.CurrencyType = tempDic.CurrencyType
                        this.getExchangeRateData(materialDic)
                    }
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取工时失败', 800)
                }
            })
            .catch(error => {
                // reject(error)
                this.setState({ indicatorAnimating: false })
                console.log('获取工时失败:', error)
            })
        // })

        // return labourData
    }

    //获取汇率接口
    getExchangeRateData(materialDic) {
        let param = {}
        param.Token = secVal
        param.CntrOperator = this.params.CntrOperator
        param.Time = DaoUtil.getCurrentDate()
        param.SourceCurrency = materialDic.CurrencyType
        param.DestCurrency = this.currencyCode
        let paramStr = JSON.stringify(param)

        networkDao.fetchPostNet('GetExchangeRate', paramStr)
            .then(data => {
                console.log('汇率数据')
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    let exR = data.ExchangeRate //汇率
                    exR = parseFloat(exR)
                    let mFee = (parseFloat(materialDic.MaterialFee) * exR).toFixed(2)
                    let lFee = (parseFloat(materialDic.LabourFee) * exR).toFixed(2)
                    let totalFee = Number(lFee) + Number(mFee)
                    materialDic.ID = this.ID
                    materialDic.TotalFee = totalFee.toFixed(2)
                    materialDic.MaterialFee = mFee
                    materialDic.LabourFee = lFee
                    this.setState({
                        workHours: materialDic.LabourHour,
                        workHoursFee: materialDic.LabourFee,
                        materialFee: materialDic.MaterialFee,
                        totalFee: materialDic.TotalFee,
                    })

                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('请联系业务人员维护汇率', 1200)
                }

            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取汇率失败', error)
            })
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
            //监听键盘弹出事件
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
                this.keyboardDidShowHandler.bind(this));
            //监听键盘隐藏事件
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
                this.keyboardDidHideHandler.bind(this));
        }

        if ((!this.ID) || (this.ID.length == 0)) {
            this.getCurrentID()
        } else {
            this.downloadImg()
        }
        secVal = this.getToken()
        this.getEntryType()             //获取进场方式
    }
    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        //卸载键盘弹出事件监听
        if (this.keyboardDidShowListener != null) {
            this.keyboardDidShowListener.remove();
        }
        //卸载键盘隐藏事件监听
        if (this.keyboardDidHideListener != null) {
            this.keyboardDidHideListener.remove();
        }

    }

    //键盘弹出事件响应
    keyboardDidShowHandler(event) {
        this.setState({ isShowDoneView: false });
    }

    //键盘隐藏事件响应
    keyboardDidHideHandler(event) {
        this.setState({ isShowDoneView: true });
    }

    leftButtonClick() {
        // NavigationUtil.goBack(this.props.navigation)
        this.refs.BackAlertView.showAlert();
    }

    backAlertSureDown = () => {
        this.cleanNetImgArr()
        NavigationUtil.goBack(this.props.navigation)
    }
    cleanNetImgArr() {
        if (this.state.netImgArr && this.state.netImgArr.length > 0) {
            for (let i = 0; i < this.state.netImgArr.length; i++) {
                let dic = this.state.netImgArr[i].uri
                let a = dic.substring(dic.lastIndexOf("/") + 1);
                let urii = RNFS.ExternalStorageDirectoryPath + '/DCIM/' + a
                networkDao.deleteLocalImage(dic)
                networkDao.deleteLocalImage2(urii)
            }
        }
    }

    backAlertCancelDown = () => {
        console.log('点击了alert取消按钮')
    }

    //获取页面数据
    getViewDic() {
        var tempDic = {}
        tempDic.BlameValue = this.state.blameValue      //责任方
        tempDic.RepairCode = this.state.repairCode      //维修代码
        tempDic.RepairPart = this.state.repairUnit      //维修组件
        tempDic.RepairType = this.state.repairMethod    //维修类型
        tempDic.RepairMeans = this.state.repairMode     //维修方式
        tempDic.BadPosition = this.state.badPosition    //残损位置
        tempDic.BadType = this.state.badType            //损坏类型
        tempDic.Long = this.state.long                  //长
        tempDic.Wide = this.state.wide                  //宽
        tempDic.Quantity = this.state.quantity          //量
        tempDic.RepairTimes = this.state.repairCount    //维修次数
        tempDic.ProjectName = this.state.projectName    //项目名称
        tempDic.RepairMethodCode = this.repairType      //维修类型代码
        tempDic.WorkHours = this.state.workHours        //工时
        tempDic.WorkHoursFee = this.state.workHoursFee  //工时费
        tempDic.MaterialFee = this.state.materialFee    //物料费
        tempDic.TotalFee = this.state.totalFee          //总费用
        tempDic.CurrencyCode = this.currencyCode        //货币类型
        tempDic.TempSave = this.TempSave                //背景色
        tempDic.Remark = this.state.Remark              //备注信息
        return tempDic
    }

    resetViewDic() {
        this.setState({
            blameValue: '',
            repairMethod: '',
            repairCode: '',
            repairUnit: '',
            repairMode: '',
            projectName: '',
            badPosition: '',
            badType: '',
            long: '',
            wide: '',
            quantity: '',
            repairCount: '',
            totalFee: '',
            workHours: '',
            workHoursFee: '',
            materialFee: '',
            data: [],
            Remark: ''
        })
    }

    //重置计算页面
    resetCalcuteView() {
        if (this.state.totalFee.length > 0) {
            this.setState({
                totalFee: '',
                workHours: '',
                workHoursFee: '',
                materialFee: ''
            })
        }
    }

    setViewDic(dic) {
        this.setState({
            blameValue: dic.BlameValue,
            repairCode: dic.RepairCode,
            repairUnit: dic.RepairPart,
            repairMethod: dic.RepairType,
            repairMode: dic.RepairMeans,
            badPosition: dic.BadPosition,
            badType: dic.BadType,
            long: dic.Long,
            wide: dic.Wide,
            quantity: dic.Quantity,
            repairCount: dic.RepairTimes,
            data: dic.ImagesArr
        })
    }

    //判断字符串是否为空
    textIsEmpty(text) {
        if (typeof (text) == 'undefined' || text == null || text == '') {
            return true
        } else {
            return false
        }

    }
    //图片数组是否是空
    imageArrIsEmpty() {
        if (typeof (this.state.data) == 'undefined' || this.state.data.length == 0) {
            return true
        } else {
            return false
        }
    }



    //分割线
    renderLine() {
        return <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    renderTableViewCellLine() {
        return <View style={{ backgroundColor: 'white', paddingLeft: 15, paddingRight: 15, height: 0.5 }}>
            <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }

    //箱号
    renderBoxNumView() {
        return <View style={{ width: GlobalStyles.screenWidth, height: 60, backgroundColor: GlobalStyles.separate_line_color }}>
            <View style={{ flexDirection: 'row', width: GlobalStyles.screenWidth, backgroundColor: 'white', marginTop: 10, height: 50 }}>
                <Text style={{ marginLeft: 15, fontSize: 17, color: 'black', height: 50, lineHeight: 50, fontWeight: '500' }}>箱号</Text>
                <Text style={{ marginLeft: 15, fontSize: 17, color: '#187ADB', height: 50, lineHeight: 50 }}>{this.params.CntrNo}</Text>
            </View>
            {this.renderTableViewCellLine()}
        </View>
    }

    //添加图片的view
    renderAddImgView() {
        return <View style={styles.addImgBackView}>
            {this.state.data.map((item, index) => (
                <View key={`${item.uri || item}`} style={styles.imgCell}>
                    <SelectImageView
                        source={item.uri ? item : { uri: item }}
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                        onPress={() => {
                            this.setState({
                                visible: true,  //显示大图
                                currentIndex: index,
                            });
                        }}
                        onDelete={() => {
                            const self = this;
                            const data = self.state.data.slice(0);
                            data.splice(index, 1);  //删除图片
                            self.setState({
                                data,
                            });
                            //console.log(this.state.data)
                        }}
                    />
                </View>
            ))}
            {this.state.data.length < 15 && (
                <View style={styles.imgCell}>
                    <SelectImageView
                        style={{
                            width: '100%',
                            height: '100%',
                        }}
                        options={{
                            imageCount: 15,
                        }}
                        onPickImages={(images) => {
                            let data = this.state.data.slice(0);
                            data = data.concat(images.map(image => ({
                                // fileName: image.uri.split('/').reverse()[0],
                                // fileName: image.filename,
                                fileSize: image.size,
                                path: image.path,
                                uri: image.path,
                            })));
                            this.setState({ data });
                        }}
                    />
                </View>
            )}

        </View>
    }

    //查看图片大图的modal
    checkBigImgModal() {
        return <Modal visible={this.state.visible} transparent>
            <ImageViewer
                index={this.state.currentIndex}
                imageUrls={this.state.data.map(item => ({
                    url: item.uri ? item.uri : item,
                }))}
                onClick={() => {
                    this.setState({ visible: false });
                }}
                onSwipeDown={() => {
                    this.setState({ visible: false });
                }}
            />
        </Modal>
    }

    //进场方式
    renderEntryModeView() {
        return <View style={{ backgroundColor: GlobalStyles.separate_line_color, height: 155, paddingTop: 10 }}>
            {BoxStateBadViewUtil.renderTextCell(false, '主进场方式', this.state.mainEntryMode, 45, 100)}
            {this.renderTableViewCellLine()}
            {BoxStateBadViewUtil.renderTextCell(false, '子进场方式', this.state.secondEntryMode, 45, 100)}
            {this.renderTableViewCellLine()}
            {BoxStateBadViewUtil.renderChooseTableViewCell(() => {
                //console.log('点击责任方的回调')
                if (this.state.blameModalArr && this.state.blameModalArr.length > 0) {
                    this.setState({ selectModalArr: this.state.blameNameArr })
                    this.refs.bottomSelectModal.show();
                }
            }, true, '责任方', this.state.blameValue, 45)}
            {this.renderTableViewCellLine()}
        </View>

    }

    //中间详情的仿flatlistView
    renderFlatListView() {
        return <View style={{ backgroundColor: GlobalStyles.backgroundColor, marginTop: 10 }}>
            {BoxStateBadViewUtil.renderTextCell(false, '维修类型', this.state.repairMethod, 45, 90)}
            {this.renderTableViewCellLine()}


            {
                BoxStateBadViewUtil.renderChooseTableViewCell(
                    () => {
                        if (this.state.repairUnit && this.state.repairUnit.length > 0 || this.state.repairMode && this.state.repairMode.length > 0) {
                            var tempArr = this.getPickArr()
                            this.refs.repairCodeModal.show('RepairCode', tempArr)
                        } else {
                            this.refs.repairCodeModal.show('RepairCode', this.state.repairCodeArr)
                        }
                        this.resetCalcuteView()
                    }, true, '维修代码', this.state.repairCode, 45
                )
            }



            {this.renderTableViewCellLine()}




            {
                BoxStateBadViewUtil.renderInputChooseCell(
                    (text) => {
                        console.log('维修部件:', text)
                        text = text.toUpperCase()
                        this.setState({ repairUnit: text, repairCode: '', projectName: '', repairMode: '', long: '', wide: '', quantity: '' })
                        this.resetCalcuteView()
                    },
                    () => {
                        console.log('点击了维修部件下拉三角')
                        this.resetCalcuteView()
                        this.refs.repairUnitOrModeModal.show('RepairUnit', this.state.repairUnitArr)
                    },
                    true,
                    '维修部件',
                    '请输入维修部件',
                    45,
                    'default',
                    this.state.repairUnit,
                    () => { console.log('kong') }
                )
            }


            {this.renderTableViewCellLine()}



            {
                BoxStateBadViewUtil.renderInputChooseCell(
                    (text) => {
                        //console.log('维修方式:', text)
                        text = text.toUpperCase()
                        this.setState({ repairMode: text, repairCode: '', projectName: '', long: '', wide: '', quantity: '' })
                        this.resetCalcuteView()
                    },
                    () => {
                        console.log('点击了维修方式下拉三角')
                        this.resetCalcuteView()
                        var tempArr = this.getRepairModeArrWithRepairunit()
                        this.refs.repairUnitOrModeModal.show('RepairMode', tempArr)
                    },
                    true,
                    '维修方式',
                    '请输入维修方式',
                    45,
                    'default',
                    this.state.repairMode,
                    () => { console.log('kong') }
                )
            }

            {this.renderTableViewCellLine()}

            {
                BoxStateBadViewUtil.renderChooseTableViewCell(
                    () => {
                        if (this.state.repairUnit && this.state.repairUnit.length > 0 || this.state.repairMode && this.state.repairMode.length > 0) {
                            var tempArr = this.getPickArr()
                            this.refs.repairCodeModal.show('RepairName', tempArr)
                        } else {
                            this.refs.repairCodeModal.show('RepairName', this.state.repairCodeArr)
                        }
                        for (let i = 0; i < this.state.repairMethodArr; i++) {
                            let dic = this.state.repairMethodArr[i]
                            if (this.state.repairMethod == dic.Name) {
                                this.repairType = dic.Code
                            }
                        }
                        this.resetCalcuteView()
                    },
                    true,
                    '项目名称',
                    this.state.projectName,
                    45,
                )
            }


            {this.renderTableViewCellLine()}


            {
                BoxStateBadViewUtil.renderInputTextCell(
                    (text) => {
                        //残损位置
                        text = text.toUpperCase()
                        this.setState({ badPosition: text })
                        this.resetCalcuteView()
                    },
                    true,
                    '残损位置',
                    '请输入残损位置',
                    45,
                    'email-address',
                    this.state.badPosition,
                    'characters',
                    4,
                    (event) => {
                        if (this.state.badPosition.length == 4) {
                            this.state.badPosition = this.state.badPosition.toUpperCase()
                            regexp = /^[RLTBFDUIEX][RLTBGHX][0-9X][0-9XN]$/
                            let res = regexp.exec(this.state.badPosition)
                            if (!res) {
                                this.refs.toast.show('请填写正确的残损位置', 800)
                                this.setState({ badPosition: '' })
                            }
                        } else {
                            this.refs.toast.show('请填写正确的残损位置', 800)
                            this.setState({ badPosition: '' })
                        }
                    }
                )
            }

            {this.renderTableViewCellLine()}



            {
                BoxStateBadViewUtil.renderInputChooseCell(
                    (text) => {
                        //console.log('损坏类型方式:', text)
                        text = text.toUpperCase()
                        this.setState({ badType: text })
                        this.resetCalcuteView()
                    },
                    () => {
                        this.refs.badTypeOrMoneyTypeModal.show('BadType', this.state.badTypeArr)
                        //console.log('点击了损坏类型下拉三角',this.state.badTypeArr)
                    },
                    true,
                    '损坏类型',
                    '请输入损坏类型',
                    45,
                    'default',
                    this.state.badType,
                    () => { console.log('选择了损坏类型') }
                )
            }

            {this.renderTableViewCellLine()}


            {
                BoxStateBadViewUtil.renderInputTextCell(
                    (text) => {
                        //长
                        this.setState({ long: text })
                        this.resetCalcuteView()
                    },
                    true,
                    '长(CM)',
                    '请输入长度',
                    45,
                    'numeric',
                    this.state.long,
                    'none',
                    10,
                    (event) => {
                        regexp = /^(0|\+?[1-9][0-9]*)$/
                        let res = regexp.exec(this.state.long)
                        if (!res) {
                            this.refs.toast.show('请填写正确的长度', 800)
                            this.setState({ long: '' })
                        }
                    }
                )
            }

            {this.renderTableViewCellLine()}


            {
                //宽
                BoxStateBadViewUtil.renderInputTextCell(
                    (text) => {
                        this.setState({ wide: text })
                        this.resetCalcuteView()
                    },
                    true,
                    '宽(CM)',
                    '请输入宽度',
                    45,
                    'numeric',
                    this.state.wide,
                    'none',
                    10,
                    (event) => {
                        regexp = /^(0|\+?[1-9][0-9]*)$/
                        let res = regexp.exec(this.state.wide)
                        if (!res) {
                            this.refs.toast.show('请填写正确的宽度', 800)
                            this.setState({ wide: '' })
                        }
                    }
                )
            }

            {this.renderTableViewCellLine()}

            {
                BoxStateBadViewUtil.renderInputTextCell(
                    (text) => {
                        //数量
                        this.setState({ quantity: text })
                        this.resetCalcuteView()
                    },
                    true,
                    '数量',
                    '请输入数量',
                    45,
                    'numeric',
                    this.state.quantity,
                    'none',
                    10,
                    (event) => {
                        regexp = /^[1-9]\d*$/
                        let res = regexp.exec(this.state.quantity)
                        if (!res) {
                            this.refs.toast.show('请填写正确的数量', 800)
                            this.setState({ quantity: '' })
                        }
                    }
                )
            }

            {this.renderTableViewCellLine()}


            {
                BoxStateBadViewUtil.renderChooseTableViewCell(
                    () => {
                        console.log('点击货币的回调')
                        this.resetCalcuteView()
                        this.refs.badTypeOrMoneyTypeModal.show('MoneyType', this.state.currencyArr)
                    },
                    true,
                    '货币',
                    this.state.currency,
                    45
                )
            }

            {this.renderTableViewCellLine()}


            {
                BoxStateBadViewUtil.renderInputTextCell(
                    (text) => {
                        //备注
                        this.setState({ Remark: text })
                    },
                    false,
                    '备注',
                    '备注信息',
                    45,
                    'default',
                    this.state.Remark,
                    'none',
                    50,
                    (event) => {
                        console.log('备注信息');
                    }
                )
            }
            <View style={{ height: 10, width: GlobalStyles.screenWidth, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }

    //计算费率
    renderCalcuteView() {
        return <View style={{ marginTop: 10, paddingLeft: 15, paddingRight: 15, width: GlobalStyles.screenWidth, height: 220 }}>
            <View style={{ flexDirection: 'row', height: 45, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ marginLeft: 10, height: 45, lineHeight: 45, fontSize: 16 }}>费用合计</Text>
                    <Text style={{ marginLeft: 30, height: 45, lineHeight: 45, fontSize: 15, color: '#666666' }}>{this.state.totalFee}</Text>
                </View>
                <TouchableOpacity onPress={() => {
                    if ((this.state.blameValue && this.state.blameValue.length > 0) &&
                        (this.state.repairMethod && this.state.repairMethod.length > 0) &&
                        (this.state.repairCode && this.state.repairCode.length > 0) &&
                        (this.state.repairUnit && this.state.repairUnit.length > 0) &&
                        (this.state.repairMode && this.state.repairMode.length > 0) &&
                        (this.state.projectName && this.state.projectName.length > 0) &&
                        (this.state.badPosition && this.state.badPosition.length > 0) &&
                        (this.state.badType && this.state.badType.length > 0) &&
                        (this.state.currency && this.state.currency.length > 0) &&
                        (this.state.long && this.state.long.length > 0) &&
                        (this.state.wide && this.state.wide.length > 0) &&
                        (this.state.quantity && this.state.quantity.length > 0)) {
                        this.calcuteFee()
                    } else {
                        this.refs.toast.show('请填写完整数据', 800)
                    }

                }}>
                    <Image
                        style={{ width: 20, height: 20, marginTop: 10 }}
                        source={require('../../../resource/CalcuteSmall.png')}
                    />
                </TouchableOpacity>
            </View>
            {this.renderLine()}
            <View style={{ flexDirection: 'row', height: 135 }}>
                <View style={{ height: 135, overflow: 'hidden' }}>
                    <Image
                        style={{ marginLeft: -30, width: 125, height: 125, marginTop: 5 }}
                        source={require('../../../resource/CalcuteBig.png')}
                    />
                </View>
                <View style={{ marginLeft: 20, height: 135, width: GlobalStyles.screenWidth - 140 }}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ height: 45, lineHeight: 45, fontSize: 16 }}>工时</Text>
                        <Text style={{ marginLeft: 30, height: 45, lineHeight: 45, fontSize: 15, color: '#666666' }}>{this.state.workHours}</Text>
                    </View>
                    {this.renderLine()}
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ height: 45, lineHeight: 45, fontSize: 16 }}>工时费</Text>
                        <Text style={{ marginLeft: 30, height: 45, lineHeight: 45, fontSize: 15, color: '#666666' }}>{this.state.workHoursFee}</Text>
                    </View>
                    {this.renderLine()}
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ height: 45, lineHeight: 45, fontSize: 16 }}>物料费</Text>
                        <Text style={{ marginLeft: 30, height: 45, lineHeight: 45, fontSize: 15, color: '#666666' }}>{this.state.materialFee}</Text>
                    </View>
                    {this.renderLine()}
                </View>
            </View>
        </View>
    }

    clickToScroll() {
        this.refs.to_top.scrollTo({ x: 0, y: 0, animated: false });
    }

    //新增,暂存,完成
    renderAddSaveDoneView() {
        return this.state.isShowDoneView ? <SafeAreaView style={styles.bottomBackView}>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.addSaveDone, { backgroundColor: GlobalStyles.nav_bar_color }]}
                onPress={() => {
                    // console.log('1:',this.state.blameValue,'2:',this.state.repairMethod,'3:',this.state.repairCode,
                    //     '4:',this.state.repairUnit,'5:',this.state.repairMode,'6:',this.state.projectName,
                    //     '7:',this.state.badPosition,'8:',this.state.badType,'9:',this.state.currency,
                    //     '10:',this.state.long,'11:',this.state.wide,'12:',this.state.quantity,
                    //     '13:',this.state.totalFee,'14:',this.state.data);
                    if ((this.state.totalFee && this.state.totalFee.length > 0) &&
                        (this.state.data && this.state.data.length > 0)) {
                        let isR = this.isRepeat()
                        if (isR) {
                            this.refs.toast.show('残损位置存在相同类别的估价，不可重复录入')
                        } else {
                            this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '请等待数据提交' })
                            this.uploadImage2('2')
                        }

                    } else {
                        this.refs.toast.show('请填写完整数据', 800)
                    }
                }}
            >
                <Text style={[styles.bottomText, { color: 'white' }]}>新  增</Text>
            </TouchableOpacity>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.addSaveDone, { backgroundColor: GlobalStyles.separate_line_color }]}
                onPress={() => {
                    if (this.imageArrIsEmpty() &&
                        this.textIsEmpty(this.state.blameValue) &&
                        this.textIsEmpty(this.state.repairCode) &&
                        this.textIsEmpty(this.state.repairUnit) &&
                        this.textIsEmpty(this.state.repairMethod) &&
                        this.textIsEmpty(this.state.repairMode) &&
                        this.textIsEmpty(this.state.badPosition) &&
                        this.textIsEmpty(this.state.badType) &&
                        this.textIsEmpty(this.state.long) &&
                        this.textIsEmpty(this.state.wide) &&
                        this.textIsEmpty(this.state.quantity) &&
                        this.textIsEmpty(this.state.projectName)) {
                        this.refs.toast.show('请填写数据', 800)
                        return
                    } else {
                        this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '请等待数据提交' })
                        this.uploadImage2('0')
                    }
                }}
            >
                <Text style={[styles.bottomText, { color: GlobalStyles.nav_bar_color }]}>暂  存</Text>
            </TouchableOpacity>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.addSaveDone, { backgroundColor: '#187ADB' }]}
                onPress={() => {
                    if ((this.state.blameValue && this.state.blameValue.length > 0) &&
                        (this.state.repairMethod && this.state.repairMethod.length > 0) &&
                        (this.state.repairCode && this.state.repairCode.length > 0) &&
                        (this.state.repairUnit && this.state.repairUnit.length > 0) &&
                        (this.state.repairMode && this.state.repairMode.length > 0) &&
                        (this.state.projectName && this.state.projectName.length > 0) &&
                        (this.state.badPosition && this.state.badPosition.length > 0) &&
                        (this.state.badType && this.state.badType.length > 0) &&
                        (this.state.currency && this.state.currency.length > 0) &&
                        (this.state.long && this.state.long.length > 0) &&
                        (this.state.wide && this.state.wide.length > 0) &&
                        (this.state.quantity && this.state.quantity.length > 0) &&
                        (this.state.totalFee && this.state.totalFee.length > 0) &&
                        (this.state.data && this.state.data.length > 0)) {
                        let isR = this.isRepeat()
                        if (isR) {
                            this.refs.toast.show('残损位置存在相同类别的估价，不可重复录入')
                        } else {
                            this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '请等待数据提交' })
                            this.uploadImage2('1')
                        }
                    } else {
                        this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '请等待数据提交' })
                        this.uploadImage2('1')
                    }

                }}
            >
                <Text style={[styles.bottomText, { color: 'white' }]}>完  成</Text>
            </TouchableOpacity>
        </SafeAreaView> : null;
    }



    render() {
        let navigationBar =
            <NavigationBar
                title={'残损估价'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <ScrollView bounces={false} ref='to_top'>
                    <KeyboardAwareScrollView enableOnAndroid={true} >
                        {this.renderBoxNumView()}
                        <View style={{ marginTop: 10, paddingLeft: 15, paddingRight: 15, backgroundColor: 'white', flexDirection: 'row' }}>
                            <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', marginTop: 7 }}>*</Text>
                            <Text style={styles.title}>残损照片</Text>
                            {this.renderLine()}
                        </View>
                        {this.renderAddImgView()}
                        {this.renderEntryModeView()}
                        {this.renderFlatListView()}
                        {this.renderCalcuteView()}
                        {this.renderAddSaveDoneView()}
                    </KeyboardAwareScrollView>
                </ScrollView>
                {this.checkBigImgModal()}
                <BottomListModal
                    ref="bottomSelectModal"
                    data={this.state.selectModalArr}
                    callBack={(selectData) => {
                        if (this.state.blameNameArr.indexOf(selectData) >= 0) {     //责任方
                            let index = this.state.blameNameArr.indexOf(selectData)
                            let dic = this.state.blameModalArr[index]
                            this.blameCode = dic.Code  //责任码
                            this.setState({ blameValue: selectData })                   //将点击的text赋给相应的state
                        } else if (this.state.projectNameArr.indexOf(selectData) >= 0) {    //项目名称
                            this.setState({ projectName: selectData })
                        } else if (this.state.repariNameArr.indexOf(selectData) >= 0) {     //维修类型
                            let index = this.state.repariNameArr.indexOf(selectData)
                            let dic = this.state.repairMethodArr[index]
                            this.repairType = dic.Code
                            this.setState({ repairMethod: selectData })
                        }
                    }}
                />
                <RepariCodeModal
                    ref="repairCodeModal"
                    callBack={(data) => {
                        let repairType = data.RepairType
                        this.repairType = repairType
                        var repairName = ''
                        var repairDic = {}  //维修类型
                        // for (var i = 0; i < this.state.repairModalArr.length; i++) {
                        //     if (repairType == this.state.repairModalArr[i].Code) {
                        //         repairName = this.state.repairModalArr[i].Name
                        //         repairDic = this.state.repairModalArr[i]
                        //         this.repairType = repairType
                        //     }
                        // }
                        for (var i = 0; i < this.state.repairMethodArr.length; i++) {
                            let tempDic = this.state.repairMethodArr[i]
                            if (repairType == tempDic.Code) {
                                repairName = tempDic.Name
                                break
                            }
                        }

                        let long = ''        //长
                        let wide = ''        //宽
                        let quantity = ''  //数量
                        if (data.RepairName && data.RepairName.length > 0) {
                            let regexp = /\[(\d*) \* (\d*) - (\d*)\]/g
                            let res;
                            res = regexp.exec(data.RepairName)
                            if (!(res == null)) {
                                res = regexp.exec(data.RepairName)
                                res = regexp.exec(data.RepairName)
                                long = res[1]
                                wide = res[2]
                                quantity = res[3]
                            }
                        }


                        this.setState({
                            repairCode: data.RepairCode,
                            projectName: data.RepairName,
                            repairUnit: data.RepairPart,
                            repairMethod: repairName,
                            repairMode: data.RepairMeans,
                            long: long,
                            wide: wide,
                            quantity: quantity
                        })
                    }}
                />
                <RepairUnitOrModeModal
                    ref="repairUnitOrModeModal"
                    callBack={(data) => {

                        if (data.showText == 'RepairUnit') {
                            this.setState({ repairUnit: data.PartCode, repairCode: '', projectName: '', repairMode: '', long: '', wide: '', quantity: '' })
                        } else if (data.showText == 'RepairMode') {
                            this.setState({ repairMode: data.RepairMeans, repairCode: '', projectName: '', long: '', wide: '', quantity: '' })
                        }

                    }}
                />
                <BadTypeOrMoneyTypeModal
                    ref="badTypeOrMoneyTypeModal"
                    callBack={(data) => {
                        if (data.showText == 'BadType') {
                            this.setState({ badType: data.DamageCode })
                        } else if (data.showText == 'MoneyType') {
                            this.currencyCode = data.Code
                            this.setState({
                                currency: data.Name,
                                currencyCode: data.Code
                            })
                        }
                    }}
                />
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='#c00'
                    />)}
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="BackAlertView"
                    TitleText="确定放弃编辑?"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.backAlertSureDown}
                    alertCancelDown={this.backAlertCancelDown}
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                {this.state.IndicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}>
                            <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                            <View style={{
                                backgroundColor: '#333',
                                borderRadius: 3,
                                marginTop: 10,
                                paddingVertical: 10,
                                width: 130,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{ color: '#fff', fontSize: 15 }}>{this.state.IndicatorAnimatingText}</Text>
                            </View>
                        </View>

                    </Modal>
                )}
            </SafeAreaViewPlus>

        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    title: {     //残损照片文字
        color: 'black',
        fontSize: 17,
        fontWeight: '500',
        height: 35,
        lineHeight: 35,
        marginLeft: 2
    },
    addImgBackView: {    //添加图片的背景view
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    imgCell: {          //添加图片的view
        width: '33%',
        aspectRatio: 1,
        justifyContent: 'center',
        // paddingLeft:5,
        // paddingRight:5,
        maxHeight: 90,
        alignItems: 'center',
    },
    bottomBackView: {
        // position: 'absolute',
        bottom: GlobalStyles.is_iphoneX ? 34 : 0,
        height: 50,
        flexDirection: 'row',
    },
    bottomTouchable: {
        height: 50,
        width: GlobalStyles.screenWidth / 2,
        borderColor: GlobalStyles.nav_bar_color,
        borderWidth: 1,
    },
    bottomText: {
        height: 50,
        lineHeight: 50,
        width: GlobalStyles.screenWidth / 3,
        textAlign: 'center',
        fontSize: 18,
    },
    addSaveDone: {   //新增,暂存,完成
        height: 50,
        width: GlobalStyles.screenWidth / 3,
    }
});