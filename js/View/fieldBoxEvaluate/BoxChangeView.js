
//箱况变更

import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, Modal, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import publicDao from '../../dao/publicDao';
import NetworkDao from '../../dao/NetworkDao'
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_BOX_UPDATE_OK_TYPE } from '../../common/EmitterTypes'
import AlertView from '../../tools/alertView'

const buttonWidth = 70 //残和好按钮的大小
let networkDao = new NetworkDao()

export default class BoxChangeView extends Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        // console.log('params:', this.params)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.state = {
            modalVisiable: false,    //好按钮点击的modal是否可见
            indicatorAnimating: false, //菊花 
            IndicatorAnimating: false, //遮罩层
            NameCn: ''           //箱属
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    componentDidMount() {
        this.backPress.componentDidMount();
        publicDao.FieldBoxStateView_NavigationKey = this.props.navigation.state.key
        this.getBoxSourceName()
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //modal的显示和隐藏
    _setModalVisible = (visible) => {
        this.setState({ modalVisiable: visible });
    }

    //获取箱属中文名称
    getBoxSourceName() {
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.CntrOperator = this.params.CntrOperator
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetCntrOperatorName', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this.setState({
                        NameCn: data.NameCn
                    })
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('获取箱属中文名失败', 800)
                }

            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('箱状态更新失败error:', error)
            })
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }
    //标记残箱接口
    markBadBox() {
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.CntrId = this.params.CntrId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('UpdateCntrCondition', paramStr)
            .then(data => {
                // console.log('残:', data)
                if (data._backcode == '201') {
                    console.log('箱状态更新成功:')
                    this.createBadBox()
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ IndicatorAnimating: false })
                    this.refs.toast.show('箱状态更新失败', 800)
                }

            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                console.log('箱状态更新失败error:', error)
            })
    }

    //新建修箱委托及计划
    createBadBox() {
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.InHandlingId
        param.CntrId = this.params.CntrId
        let paramStr = JSON.stringify(param)
        console.log(paramStr);
        networkDao.fetchPostNet('CreateOrderAndHandlingByInYard', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                console.log('残:', data)
                if (data._backcode == '201') {
                    console.log('箱计划创建成功:')
                    this.params.InHandlingId = data.RepairHandlingId
                    NavigationUtil.goPage(this.params, 'FieldBoxStateView')
                    DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show('委托已经存在', 800)
                }

            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                console.log('箱状态更新失败error:', error)
            })
    }
    renderOkModal() {
        return <Modal
            animationType='none'
            transparent={true}
            visible={this.state.modalVisiable}
        >
            <View style={styles.modalBackgroundStyle}>
                <View style={styles.modalContentView}>
                    <Text style={styles.modalTitle}>请核对无误后提交</Text>
                    <View style={[styles.modalDetail, { borderTopWidth: 0.5 }]}>
                        <Text style={styles.modalDetailLeft}>箱属</Text>
                        <Text style={[styles.modalDetailRight, { color: '#0877DE' }]}>{this.state.NameCn}</Text>
                    </View>
                    <View style={[styles.modalDetail, { borderTopWidth: 0.5 }]}>
                        <Text style={styles.modalDetailLeft}>箱号</Text>
                        <Text style={[styles.modalDetailRight, { color: '#0877DE' }]}>{this.params.CntrNo}</Text>
                    </View>
                    <View style={[styles.modalDetail, { borderTopWidth: 0.5, borderBottomWidth: 0.5 }]}>
                        <Text style={styles.modalDetailLeft}>箱况</Text>
                        <Text style={[styles.modalDetailRight, { color: '#76AE17' }]}>残损</Text>
                    </View>
                    <View style={{ height: 50, marginTop: 30, flexDirection: 'row', backgroundColor: '#c00', justifyContent: 'center', borderTopWidth: 0.5, borderTopColor: '#eee', }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setState({ modalVisiable: false })
                            }}
                            style={{ flex: 1 }}
                        >
                            <View style={{ height: 50, backgroundColor: '#fff' }}>
                                <Text style={{ height: 50, lineHeight: 50, fontSize: 10, textAlign: 'center', color: '#aaa', fontSize: 18, }}>取 消</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                            this.setState({ modalVisiable: false, IndicatorAnimating: true })
                        }}>
                            <View style={{ height: 50, backgroundColor: GlobalStyles.nav_bar_color, flex: 1 }}>
                                <Text style={styles.modalConfirm}>提  交</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>

        </Modal>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'箱况变更'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                {navigationBar}
                {this.renderOkModal()}
                <View style={[styles.carAndBox, { marginTop: 1 }]}>
                    <Text style={styles.leftText}>箱属</Text>
                    <Text style={[styles.rightText, { color: '#0877DE' }]}>{this.state.NameCn}</Text>
                </View>
                <View style={[styles.carAndBox, { marginTop: 1 }]}>
                    <Text style={styles.leftText}>箱号</Text>
                    <Text style={[styles.rightText, { color: '#0877DE' }]}>{this.params.CntrNo}</Text>
                </View>
                <Text style={styles.boxState}>箱况</Text>
                <View style={styles.buttonsBackView}>
                    <View style={{ marginTop: 18, width: 72, height: 70, marginLeft: 40 }}>
                        <Image style={{ width: 56, height: 56 }} source={require('../../../resource/hao-n.png')} />
                    </View>
                    <View style={{ marginTop: 40 }}>
                        <Image style={{ height: 12, width: 80 }} source={require('../../../resource/Thearrow.png')} />
                    </View>
                    <View style={{ marginTop: 10, width: 70, height: 70, marginRight: 38 }}>
                        <Image style={{ width: 80, height: 80 }} source={require('../../../resource/can-h.png')} />
                    </View>
                </View>
                <TouchableOpacity
                    style={styles.confirmBtn}
                    onPress={() => { this.setState({ modalVisiable: true }) }}>
                    <View >
                        <Text style={styles.confirmText}>确  认</Text>
                    </View>
                </TouchableOpacity>
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <Modal
                    animationType="none"
                    transparent={true}
                    visible={this.state.IndicatorAnimating}
                    onShow={()=>{
                        this.markBadBox()
                    }}
                >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}>
                        <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                        <View style={{
                            backgroundColor: '#333',
                            borderRadius: 3,
                            marginTop: 10,
                            paddingVertical: 10,
                            width: 130,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                        </View>
                    </View>
                </Modal>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    carAndBox: {
        flexDirection: 'row',
        height: 50,
        backgroundColor: 'white',
    },
    leftText: {          //车号,箱号
        marginLeft: 15,
        height: 50,
        width: 100,
        fontSize: 17,
        lineHeight: 50,
        color: 'black',
        textAlign: 'left',
    },
    rightText: {         //车号,箱号后面的值
        marginRight: 15,
        height: 50,
        width: GlobalStyles.screenWidth - 100 - 30,
        textAlign: 'right',
        fontSize: 16,
        lineHeight: 50,
    },
    boxState: {      //箱状态
        height: 40,
        lineHeight: 40,
        paddingLeft: 15,
        textAlign: 'left',
        fontSize: 17,
        fontWeight: '500',
        backgroundColor: 'white',
        marginTop: 10,
    },
    buttonsBackView: {   //残和好按钮背景
        marginTop: 1,
        height: 100,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    confirmBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
        marginLeft: 15,
        marginRight: 15,
        backgroundColor: '#BC1920',
        height: 50,
        width: GlobalStyles.screenWidth - 30,
        borderRadius: 5,
    },
    confirmText: {
        fontSize: 18,
        color: 'white'
    },
    modalBackgroundStyle: {      //modal
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        width: GlobalStyles.screenWidth,
        height: GlobalStyles.screenHeight,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContentView: {
        backgroundColor: 'white',
        borderRadius: 2,
        width: '80%'
    },
    closeImg: {
        width: 20,
        height: 20,
        marginTop: 5,
        marginLeft: GlobalStyles.screenWidth * 0.8 - 20,
    },
    modalTitle: {
        color: 'black',
        height: 50,
        textAlign: 'center',
        fontSize: 18,
        lineHeight: 50,
        fontWeight: '500',
    },
    modalDetail: {
        flexDirection: 'row',
        borderLeftWidth: 0.5,
        borderRightWidth: 0.5,
        borderColor: '#DCDFE6',
        height: 35,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 10,
        paddingRight: 10,
        // justifyContent: 'space-between',
    },
    modalDetailLeft: {
        height: 35,
        textAlign: 'left',
        lineHeight: 35,
        color: 'black',
        fontSize: 16,
        width: '30%'
    },
    modalDetailRight: {
        // textAlign: 'right',
        fontSize: 16,
        height: 35,
        lineHeight: 35,
    },
    modalConfirm: {      //提交
        height: 50,
        lineHeight: 50,
        fontSize: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
    }
});

