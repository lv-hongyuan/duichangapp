
//场内验残

import React, { Component } from 'react';
import { Modal, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import Entypo from 'react-native-vector-icons/Entypo'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_BOX_UPDATE_OK_TYPE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
let networkDao = new NetworkDao()


export default class fieldBoxEvaluateView extends Component {

    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params;
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.oriArr = []
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],                 //数据
            refreshing: true,
            toTopView: false
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }
    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    // 数据获取
    getData() {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        //console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetInYardIntactCntr', paramStr)
            .then(data => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this._largelist.endRefresh();
                    let tempArr = new Array().concat(data.InYardCntrList)
                    let Dataa = this.handleLargeListData(tempArr)
                    this.oriArr = tempArr
                    this.setState({ dataArr: Dataa, value: '' })
                    this.timer = setTimeout(() => {
                        this.setState({ refreshing: true })
                    }, 5000)
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)

                } else if (data._backcode == '401') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.timer && clearTimeout(this.timer);
                // this.setState({ indicatorAnimating: false, })
                // console.log('验箱估价error:', error)
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    componentDidMount() {
        this.backPress.componentDidMount();
        this.timer = setTimeout(() => {
            this.setState({ indicatorAnimating: false })
            this.refs.toast.show('网络不佳,请重试', 1200)
        }, 10000)
        this.getData()

        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_BOX_UPDATE_OK_TYPE, () => {
            this.getData()
            this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
        });
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.upDataOkListener) {
            this.upDataOkListener.remove();   //移除事件监听
        }

        this.timer && clearTimeout(this.timer);
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //头部搜索
    headerView = () => {
        return <View style={{ height: 90, backgroundColor: GlobalStyles.backgroundColor }}>
            <View style={styles.searchBackView}>
                <Image style={{ marginLeft: 15, marginTop: 10, width: 15, height: 15 }}
                    source={require('../../../resource/search.png')}
                />
                <TextInput
                    style={styles.headerTextInput}
                    placeholder='箱号检索'
                    clearButtonMode='always'
                    returnKeyType='search'
                    defaultValue={this.state.value}
                    autoCapitalize='characters'
                    onChangeText={text => {
                        text = text.replace(/\s/gi, '').toUpperCase()
                        this.setState({
                            value: text
                        });
                        const newData = this.oriArr.filter(item => {
                            const itemData = `${item.CntrNo.toUpperCase()}`;
                            const textData = text
                            return itemData.indexOf(textData) > -1;
                        });
                        let Dataa = this.handleLargeListData(newData)
                        this.setState({
                            dataArr: Dataa,
                        });
                    }}
                />
            </View>
            <Text style={styles.headerTextView}>在场箱列表</Text>
        </View>;
    }

    //新建修箱委托及计划
    createBadBox(item) {
        this.params.CntrOperator = item.CntrOperator
        this.params.viewKey = 'fieldBoxEvaluateView'
        this.params.CntrNo = item.CntrNo
        this.params.CntrType = item.CntrType
        this.params.CntrSize = item.CntrSize
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = item.InHandlingId
        param.CntrId = item.CntrId
        
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('CreateOrderAndHandlingByInYard', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '201') {
                    this.params.InHandlingId = data.RepairHandlingId
                    NavigationUtil.goPage(this.params, 'FieldBoxStateView')
                    DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show('委托已经存在', 800)
                }

            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('箱状态更新失败error:', error)
            })
    }

    //cell
    renderItem = ({ row: row }) => {
        let item = this.state.dataArr[0].items[row]
        let isTemp = item.IsTemporary  //是否是暂存
        let isHand = item.IsNoHandling
        let bgColor = isTemp == 'Y' ? isHand == 'Y' ? '#fdd' : '#FFF7CF' : 'white'
        return <TouchableOpacity onPress={() => {
            //cell点击事件
            if(isHand == 'Y'){
                this.setState({indicatorAnimating: true })
                this.createBadBox(item)
            }else if (isTemp == 'Y') {
                NavigationUtil.goPage(item, 'FieldEvaluateSummeryDetail')
            } else {
                NavigationUtil.goPage(item, 'BoxChangeView')
            }

        }}>
            <View style={{ height: 59.5, flexDirection: 'row', backgroundColor: bgColor, borderBottomColor: '#eee', borderBottomWidth: 0.5, }}>
                <View style={{ width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                    <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                    <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                    <Text style={styles.cellRightText}>{item.BeginTime}</Text>
                    <Ionicons
                        name={'ios-arrow-forward'}
                        size={20}
                        style={{
                            position: 'absolute',
                            right: 15,
                            alignSelf: 'center',
                            color: '#CCCCCC',
                        }} />
                </View>
            </View>
        </TouchableOpacity>
    }

    render() {

        let navigationBar =
            <NavigationBar
                title={'场内验残'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {this.state.indicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='#c00'
                        />
                    </Modal>
                )}
                {navigationBar}
                <View style={{ flex: 1 }}>
                    <LargeList
                        ref={ref => (this._largelist = ref)}
                        style={styles.container}
                        data={this.state.dataArr}
                        renderHeader={this.headerView}
                        heightForIndexPath={() => 60}
                        renderIndexPath={this.renderItem}
                        refreshHeader={ChineseWithLastDateHeader}
                        onRefresh={() => {
                            if (this.state.refreshing == true) {
                                this.timer && clearTimeout(this.timer);
                                this.setState({ refreshing: false })
                                this.getData()
                                
                            } else {
                                this._largelist.endRefresh();
                                this.refs.toast.show('刷新过于频繁,请稍后重试',2500)

                            }
                        }}
                        onScroll={({ nativeEvent: { contentOffset: { x, y } } }) => {
                            if (y > 800) {
                                this.setState({ toTopView: true })
                            } else {
                                this.setState({ toTopView: false })
                            }
                        }}
                    />
                    {this.state.toTopView && (
                        <TouchableOpacity onPress={() => {
                            this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
                        }} style={styles.floatView}>
                            <Entypo
                                name={'align-top'}
                                size={30}
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                }}
                            />
                        </TouchableOpacity>
                    )}
                </View>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBackView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 35,
    },
    headerTextInput: {
        marginLeft: 10,
        marginTop: 2.5,
        width: GlobalStyles.screenWidth - 75,
        height: 30,
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: 15,
    },
    headerTextView: {
        marginTop: 10,
        marginLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'left',
        lineHeight: 30   //为了居中
    },
    cellLeftText: {
        textAlign: 'left',
        fontSize: 16,
        height: 30,
        marginLeft: 15
    },
    cellRightText: {
        height: 60,
        fontSize: 14,
        textAlign: 'center',
        color: '#999999',
        lineHeight: 60,
    },
    floatView: {
        width: 35,
        height: 35,
        backgroundColor: '#bbb',
        position: 'absolute',
        bottom: 100,
        right: 30,
        alignItems: 'center',
        justifyContent: 'center',
    }
});