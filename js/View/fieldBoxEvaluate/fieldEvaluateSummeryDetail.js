/**
 * 估价汇总
 */
import React, { Component } from 'react';
import { FlatList, Platform, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Modal, SafeAreaView, Keyboard, ActivityIndicator, DeviceEventEmitter, Alert } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import Ionicons from 'react-native-vector-icons/Ionicons'
import BottomListModal from '../../common/BottomListModal'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import AlertView from '../../tools/alertView'
import { EMITTER_BOX_UPDATE_OK_TYPE, EMITTER_BOXSTATEBADDETAILVIEW_DONE_TYPE } from '../../common/EmitterTypes'
import unionWith from 'lodash/unionWith'
import FormData from '../../../node_modules/react-native/Libraries/Network/FormData';
import RNFS from 'react-native-fs';

const blame_selected_color = '#FFF4F4'  //船公司,货主或者自然锈蚀老化选择时的颜色
var IS_ADNROID = Platform.OS === 'android';
var cell_text_width = (GlobalStyles.screenWidth - 30) / 4
let networkDao = new NetworkDao()


export default class FieldEvaluateSummeryDetail extends Component {

    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params;
        // console.log('this.params:', this.params)
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        this.handlingQuoteId = ''
        this.state = {
            data: [],   //flatlist数据
            blame1Color: 'white',    //船公司
            blame2Color: 'white',  //货主
            blame3Color: 'white',    //自然锈蚀老化
            depostArr: ['是', '否'],   //是否交押金的数组
            depostValue: '否',       //是否交押金
            isShowDoneView: true,    //是否显示下面的保存和提交的view
            indicatorAnimating: false, //菊花
            IndicatorAnimating: false,
            CntrNo: this.params.CntrNo,            //箱号
            ownerTotalMoney: '',     //货主总钱数
            shiperTotalMoney: '',    //船公司总钱数
            date: []
        }
    }

    onBackPress(e) {
        // NavigationUtil.goBack(this.props.navigation);
        //this.refs.BackAlertView.showAlert();
        return true

    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
            //监听键盘弹出事件
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
                this.keyboardDidShowHandler.bind(this));
            //监听键盘隐藏事件
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
                this.keyboardDidHideHandler.bind(this));
        }
        publicDao.FieldEvaluateSummeryDetail_NavigationKey = this.props.navigation.state.key

        this.getTemporarySummeryData()

        this.onDoneListener = DeviceEventEmitter.addListener(EMITTER_BOXSTATEBADDETAILVIEW_DONE_TYPE, (data) => {
            console.log('====', data);

            this.refreshData(data)
            this.setState({
                date: data
            })
        })

    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        //卸载键盘弹出事件监听
        if (this.keyboardDidShowListener != null) {
            this.keyboardDidShowListener.remove();
        }
        //卸载键盘隐藏事件监听
        if (this.keyboardDidHideListener != null) {
            this.keyboardDidHideListener.remove();
        }

        if (this.onDoneListener) {
            this.onDoneListener.remove()
        }
    }

    //键盘弹出事件响应
    keyboardDidShowHandler(event) {
        this.setState({ isShowDoneView: false });
    }

    //键盘隐藏事件响应
    keyboardDidHideHandler(event) {
        this.setState({ isShowDoneView: true });
    }


    alertSureDown = () => {
        var tempArr = this.state.data
        let tempDic = tempArr[this.current_delete_index]
        let cellID = tempDic.ID
        if (cellID.length > 0) {
            this.deleteCellData(cellID)
            this.ClearCellImg(tempDic)
        }
        tempArr.splice(this.current_delete_index, 1)
        if (tempArr.length === 0) {
            this.setState({
                data: [],
                ownerTotalMoney: '',
                shiperTotalMoney: ''
            })
        } else {
            let ownerM = 0  //货主的总钱数
            let shiperM = 0  //船公司的总钱数
            for (let i = 0; i < tempArr.length; i++) {
                let tempDic1 = tempArr[i]
                if (tempDic1.Responser == '货主') {
                    let o1 = this.handleNumber(tempDic1.Amount)
                    ownerM += o1
                } else if (tempDic1.Responser == '船公司') {
                    let s1 = this.handleNumber(tempDic1.Amount)
                    shiperM += s1
                }
            }
            this.setState({
                data: tempArr,
                ownerTotalMoney: ownerM,
                shiperTotalMoney: shiperM
            })
        }
    }

    alertCancelDown = () => {
        console.log('点击了alert取消按钮')
    }

    //去交押金
    depositAlertSureDown = () => {
        let dic = {}
        dic.viewKey = 'FieldEvaluateSummeryDetail'
        dic.handlingId = this.handlingQuoteId
        dic.cntrNo = this.state.CntrNo
        dic.shiperTotalMoney = this.state.shiperTotalMoney
        dic.ownerTotalMoney = this.state.ownerTotalMoney
        dic.depositType = 'IN'
        NavigationUtil.goPage(dic, 'RegisteFormView')
    }

    depositAlertCancelDown = () => {
        NavigationUtil.goBack(this.props.navigation)
        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
    }

    BackAlertCancelDown = () => {

    }

    handleNumber(num) {
        return parseFloat(num) ? parseFloat(num) : 0
    }

    //删除cell数据
    deleteCellData(cellID) {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.ID = cellID
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('DeleteHandlingQuoteByID', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '201') {

                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('删除cell数据失败:', error)
            })
    }

    //点击删除明细时删除服务器照片
    ClearCellImg(tempDic) {
        let secVal = this.getToken()
        let formData = new FormData()
        formData.append('CntrNo', this.state.CntrNo)
        formData.append('HandlingId', this.handlingQuoteId)
        formData.append('ID', tempDic.ID)
        formData.append('Token', secVal)
        networkDao.uploadImgArr2('UploadDamagePhotos', formData)
            .then(data => {
                if (data._backcode == '201') {
                    console.log('照片清除成功')
                }
            })
            .catch(error => {
                console.log('清除照片失败')
            })
    }
    //获取相同字段的dic
    getCommonDic(dic) {
        let tempDic = {}
        let isTemporary1 = dic.TempSave == '1' ? 'Y' : 'N'
        tempDic.HandlingId = this.handlingQuoteId
        tempDic.CntrNo = this.params.CntrNo
        tempDic.CntrType = this.params.CntrType
        tempDic.CntrSize = this.params.CntrSize
        tempDic.CntrOperator = this.params.CntrOperator
        tempDic.RepairCode = dic.RepairCode ? dic.RepairCode : ''
        tempDic.RepairPart = dic.RepairPart ? dic.RepairPart : ''
        tempDic.RepairTimes = '1'
        tempDic.RepairMete = dic.b_rePairMete ? dic.b_rePairMete : ''
        tempDic.RepairName = dic.ProjectName ? dic.ProjectName : ''
        // tempDic.ImagesArr = dic.ImagesArr ? dic.ImagesArr : ''
        tempDic.CurrencyCode = dic.CurrencyCode ? dic.CurrencyCode : ''
        tempDic.MaterialFee = dic.MaterialFee ? dic.MaterialFee : ''
        tempDic.Labour = dic.WorkHours ? dic.WorkHours : ''
        tempDic.LabourFee = dic.WorkHoursFee ? dic.WorkHoursFee : ''
        tempDic.Amount = dic.TotalFee ? dic.TotalFee : ''
        tempDic.Responser = dic.BlameValue ? dic.BlameValue : ''
        tempDic.QuoteNo = ''
        tempDic.RepairMeans = dic.RepairMeans ? dic.RepairMeans : ''
        tempDic.DamageLocation = dic.BadPosition ? dic.BadPosition : ''
        tempDic.DamageType = dic.BadType ? dic.BadType : ''
        tempDic.RepairType = dic.RepairMethodCode ? dic.RepairMethodCode : ''
        tempDic.Length = dic.Long ? dic.Long : ''
        tempDic.Width = dic.Wide ? dic.Wide : ''
        tempDic.RepairNum = dic.Quantity ? dic.Quantity : ''
        tempDic.ID = dic.ID
        tempDic.IsTemporary = isTemporary1
        tempDic.CreateName = dic.CreateName ? dic.CreateName : ''
        tempDic.CreateTime = dic.CreateTime ? dic.CreateTime : ''
        tempDic.Remark = dic.Remark ? dic.Remark : ''
        return tempDic
    }

    //更新从详情页传过来的数据
    refreshData(addArr) {
        // let oriArr = new Array().concat(this.state.data)
        let oriArr = this.state.data.concat()
        let tempArr = []
        for (let i = 0; i < addArr.length; i++) {
            let tempJson = this.getCommonDic(addArr[i])
            tempArr.push(tempJson)
        }
        addArr = tempArr
        oriArr = unionWith(addArr, oriArr, (a1, a2) => a1.ID === a2.ID)

        let ownerM = 0  //货主的总钱数
        let shiperM = 0  //船公司的总钱数
        for (let i = 0; i < oriArr.length; i++) {
            let tempDic1 = oriArr[i]
            if (tempDic1.Responser == '货主') {
                let o1 = this.handleNumber(tempDic1.Amount)
                ownerM += o1
            } else if (tempDic1.Responser == '船公司') {
                let s1 = this.handleNumber(tempDic1.Amount)
                shiperM += s1
            }
        }

        this.setState({
            ownerTotalMoney: ownerM,
            shiperTotalMoney: shiperM,
            blame1Color: shiperM > 0 ? blame_selected_color : 'white',
            blame2Color: ownerM > 0 ? blame_selected_color : 'white',
            data: oriArr
        })
    }

    //获取暂存估价信息
    getTemporarySummeryData() {
        console.log(this.params);
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.InHandlingId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetSummaryHandlingQuote', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this.handlingQuoteId = data.HandlingId
                    let isHandInDeposit = data.IsHandInDeposit == 'Y' ? '是' : '否'
                    let tempArr = new Array().concat(data.HandlingQuoteList)
                    let ownerM = 0  //货主的总钱数
                    let shiperM = 0  //船公司的总钱数
                    for (let i = 0; i < tempArr.length; i++) {
                        let tempDic1 = tempArr[i]
                        if (tempDic1.Responser == '货主') {
                            let o1 = this.handleNumber(tempDic1.Amount)
                            ownerM += o1
                        } else if (tempDic1.Responser == '船公司') {
                            let s1 = this.handleNumber(tempDic1.Amount)
                            shiperM += s1
                        }
                    }
                    if (tempArr.length > 0) {
                        this.setState({
                            depostValue: isHandInDeposit,
                            ownerTotalMoney: ownerM,
                            shiperTotalMoney: shiperM,
                            blame1Color: shiperM > 0 ? blame_selected_color : 'white',
                            blame2Color: ownerM > 0 ? blame_selected_color : 'white',
                            data: tempArr,
                            CntrNo: tempArr[0].CntrNo
                        })
                    }

                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.setState({ indicatorAnimating: false })
                    console.log('获取暂存估价信息失败:', error)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取暂存估价信息失败:', error)
            })
    }

    //获取本地照片数组
    // deleteFile() {
    //     let data = this.state.date
    //     let urlArr = []
    //     console.log(data);
    //     if (data && data.length > 0) {
    //         for (var i = 0; i < data.length; i++) {
    //             if (data[i].ImagesArr && data[i].ImagesArr.length > 0) {
    //                 for (var j = 0; j < data[i].ImagesArr.length; j++) {
    //                     if(data[i].ImagesArr[j].path){
    //                         let url = data[i].ImagesArr[j].path
    //                         urlArr.push(url)
    //                     }
    //                 }
    //             }
    //         }
    //         console.log(urlArr);
    //         if (urlArr && urlArr.length > 0) {
    //             let rnfsPath = Platform.OS === 'ios' ? RNFS.LibraryDirectoryPath : RNFS.ExternalStorageDirectoryPath
    //             let path = rnfsPath + '/Pictures'
    //             RNFS.unlink(path).then(() => {
    //                 console.log('照片删除成功', 800)
    //             })
    //             RNFS.mkdir(path).then((result) => {
    //                 console.log('result', result)
    //             }).catch((err) => {
    //                 console.log('err', err)
    //             })
    //                 .catch((e) => {
    //                     console.log(e)
    //                 })
    //         }
    //     }
    // }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }
    //点击暂存或者提交：
    uploadParamsData(isTemporary) {

        // this.setState({ IndicatorAnimating: true })
        let isDepost = this.state.depostValue == '是' ? 'Y' : 'N'
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.IsTemporary = isTemporary
        param.IsHandInDeposit = isDepost
        param.HandlingId = this.handlingQuoteId
        param.HandlingQuoteList = this.state.data
        let paramStr = JSON.stringify(param)
        console.log(paramStr);

        networkDao.fetchPostNet('UploadHandlingQuoteByEnterYard', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    console.log('验箱估价上传成功')

                    if (isTemporary == 'N') {
                        if (isDepost == 'Y') {
                            this.refs.DepostAlertView.showAlert();
                        } else {
                            NavigationUtil.goBack(this.props.navigation)
                            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                            // this.deleteFile()
                        }
                    } else {
                        NavigationUtil.goBack(this.props.navigation)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                        // this.deleteFile()
                    }
                } else if (data._backcode == '500') {
                    this.refs.TokenMissView.showAlert()
                } else {
                    this.refs.toast.show(data._backmes)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                // console.log('验箱估价上传失败', error)
            })
    }

    //暂存按钮点击事件
    temporaryBtnClick() {
        if (this.state.data && this.state.data.length > 0) {
            this.setState({ IndicatorAnimating: true })
            this.uploadParamsData('Y')
        } else {
            this.refs.toast.show('至少录入一条明细', 800)
        }

    }

    //提交按钮点击事件
    commitBtnClick() {
        for (let i = 0; i < this.state.data.length; i++) {
            let tempObj = this.state.data[i]
            if (tempObj.TempSave == '1' || tempObj.IsTemporary == 'Y') {
                this.refs.toast.show('暂存数据不可操作提交', 800)
                return
            }

        }
        if (this.state.data && this.state.data.length > 0) {
            this.setState({ IndicatorAnimating: true })
            this.uploadParamsData('N')
        } else {
            this.refs.toast.show('至少录入一条明细', 800)
        }

    }

    //分割线
    renderLine() {
        return <View style={{ height: 1, backgroundColor: '#eee' }}></View>
    }

    //箱号,右边点击添加cell
    renderTitleView() {
        return <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 45, paddingLeft: 10, borderBottomColor: '#eee', borderBottomWidth: 1 }}>
            <Text style={styles.textStyle}>箱号</Text>
            <Text style={[styles.textStyle, { color: '#0877DE', marginLeft: 25, fontSize: 16 }]}>{this.state.CntrNo}</Text>
            <TouchableOpacity style={{ position: 'absolute', right: 15, top: 12.5, width: 20, height: 20 }}
                onPress={() => {
                    //右上角添加cell按钮点击事件'
                    let tempJson = Object.assign({}, this.params)    //深拷贝
                    tempJson.HandlingId = this.handlingQuoteId
                    let jsonD = {}
                    jsonD.data = this.state.data
                    jsonD.item = tempJson
                    NavigationUtil.goPage(jsonD, 'FieldBoxStateDetailView')
                    console.log(jsonD);
                }}>
                <Image
                    style={{ width: 20, height: 20 }}
                    source={require('../../../resource/add.png')}
                />
            </TouchableOpacity>
        </View>

    }
    //flatList的cell
    renderItem(data) {
        var item = data.item
        let cellColor = item.IsTemporary == 'Y' ? '#FFF7CF' : 'white'
        let jsonD = {}
        jsonD.data = this.state.data
        jsonD.index = data.index
        jsonD.item = item
        return <TouchableOpacity
            activeOpacity={1}
            style={{ height: 75, backgroundColor: cellColor, paddingLeft: 10 }}
            onPress={() => {
                NavigationUtil.goPage(jsonD, 'FieldBoxStateDetailView')
            }}
        >
            <View style={{ flexDirection: 'row', height: 35, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', height: 35, marginTop: 10 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', height: 15, width: 15, backgroundColor: '#BC1920' }}>
                        <Text style={{ color: 'white', width: 15, height: 15, fontSize: 12, textAlign: 'center' }}>{data.index + 1}</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <Text style={{ marginLeft: 10, height: 15, fontSize: 13, color: '#666666' }}>长-宽-量 :  </Text>
                        <Text style={{ height: 15, fontSize: 13, color: GlobalStyles.gray_text_color }}>{this.handleNumber(item.Length) + '-' + this.handleNumber(item.Width) + '-' + this.handleNumber(item.RepairNum)}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', height: 35, width: 60, marginTop: 10 }}>
                    <TouchableOpacity
                        style={{ height: 32, width: 25, marginLeft: 30 }}
                        onPress={() => {
                            console.log('删除cell按钮点击事件')
                            this.current_delete_index = data.index
                            this.refs.AlertView.showAlert();
                        }}>
                        <Image
                            style={{ height: 18, width: 14 }}
                            source={require('../../../resource/dele.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ flexDirection: 'row', height: 39 }}>
                <Text style={styles.cellInput}>{item.RepairPart}</Text>
                <Text style={styles.cellInput}>{item.RepairMeans}</Text>
                <Text style={styles.cellInput}>{item.DamageLocation}</Text>
                <Text style={styles.cellInput}>{item.DamageType}</Text>
            </View>
            {this.renderLine()}
        </TouchableOpacity >
    }


    //船公司,货主,自然锈蚀老化
    renderBlameView() {
        let om = Number(this.state.ownerTotalMoney).toFixed(2)
        let sm = Number(this.state.shiperTotalMoney).toFixed(2)
        return <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10, height: 95, backgroundColor: "white" }}>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame1Color }]}>
                <Text style={styles.blameTitleText}>船公司</Text>
                <Text style={styles.blameValueText}>{'￥ ' + sm}</Text>
            </View>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame2Color }]}>
                <Text style={styles.blameTitleText}>货主</Text>
                <Text style={styles.blameValueText}>{'￥ ' + om}</Text>
            </View>
            <View
                style={[styles.blameTouchableStyle, { backgroundColor: this.state.blame3Color }]}>
                <Text style={styles.blameTitleText}>自然锈蚀老化</Text>
                <Text style={styles.blameValueText}>￥ 0</Text>
            </View>

        </View>
    }

    //交押金
    renderDepositView() {
        return <View style={{ height: 60, backgroundColor: 'white', marginTop: 10 }}>
            <TouchableOpacity
                onPress={() => {
                    this.refs.selectDeposiModal.show();
                }}
            >
                <View style={{ flexDirection: 'row', height: 50, backgroundColor: 'white' }}>

                    <Text style={[styles.depostText, { fontSize: 17, }]}>交押金</Text>
                    <Text style={[styles.depostText, { fontSize: 16, color: '#666666' }]}>{this.state.depostValue}</Text>
                    <View style={{ position: 'absolute', right: 20, top: 15, width: 20, height: 20 }}>
                        <Ionicons
                            name={'ios-arrow-down'}
                            size={20}
                            style={{
                                alignSelf: 'center',
                                color: '#666666',
                            }} />
                    </View>



                </View>
            </TouchableOpacity>
            <View style={{ height: 10, width: GlobalStyles.screenWidth, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }

    //暂存和提交
    renderBottomView() {
        return this.state.isShowDoneView ? <View style={styles.bottomBackView}>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.bottomTouchable, { backgroundColor: 'white' }]}
                onPress={() => {
                    //保存按钮点击事件
                    console.log('点了暂存按钮')
                    // NavigationUtil.goBackToLoginView(this.props.navigation);
                    this.temporaryBtnClick()
                }}
            >
                <Text style={[styles.bottomText, { color: GlobalStyles.nav_bar_color }]}>暂  存</Text>
            </TouchableOpacity>
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.bottomTouchable, { backgroundColor: GlobalStyles.nav_bar_color }]}
                onPress={() => {
                    //提交按钮点击事件
                    this.commitBtnClick()
                }}
            >
                <Text style={[styles.bottomText, { color: 'white' }]}>提  交</Text>
            </TouchableOpacity>
        </View> : null;
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'估价汇总'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
            // leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                {navigationBar}
                <ScrollView
                    bounces={false}
                    style={{ flex: 1, marginBottom: 50 }}
                >
                    <View style={styles.flatlistBackView}>
                        {this.renderTitleView()}
                        <FlatList
                            data={this.state.data}
                            renderItem={data => this.renderItem(data)}
                            keyExtractor={(item, index) => index.toString()}
                            bounces={false}
                        />
                    </View>
                    {this.renderBlameView()}
                    {this.renderDepositView()}
                    <BottomListModal
                        ref="selectDeposiModal"
                        data={this.state.depostArr}
                        callBack={(selectData) => {
                            this.setState({ depostValue: selectData })
                        }}
                    />
                </ScrollView>
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />)}
                <AlertView
                    ref="AlertView"
                    TitleText="确定删除当前数据?"
                    DesText="删除后将不可恢复"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDown}
                    alertCancelDown={this.alertCancelDown}
                />
                <AlertView
                    ref="DepostAlertView"
                    TitleText="是否去交押金?"
                    // DesText=" "
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.depositAlertSureDown}
                    alertCancelDown={this.depositAlertCancelDown}
                />
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                {this.state.IndicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}>
                            <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                            <View style={{
                                backgroundColor: '#333',
                                borderRadius: 3,
                                marginTop: 10,
                                paddingVertical: 10,
                                width: 130,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                            </View>
                        </View>

                    </Modal>
                )}
                {this.renderBottomView()}

            </SafeAreaViewPlus>

        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    flatlistBackView: {
        backgroundColor: GlobalStyles.nav_bar_color,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    textStyle: {             //文字通用样式
        height: 45,
        lineHeight: 45,
        fontSize: 17,
    },
    blameTouchableStyle: {  //船公司,货主,自然锈蚀老化的touchable
        height: 70,
        width: 105,
        borderWidth: 1,
        borderColor: '#BC1920',
        marginTop: 12.5,
    },
    blameTitleText: {        //船公司,货主,自然锈蚀老化文字
        height: 30,
        lineHeight: 30,
        textAlign: 'center',
        fontSize: 14,
        paddingTop: 5,      //使文字稍微往下一点
    },
    blameValueText: {        //船公司,货主,自然锈蚀老化的value
        height: 40,
        lineHeight: 40,
        textAlign: 'center',
        fontSize: 18,
        color: GlobalStyles.nav_bar_color,
        paddingBottom: 10,   //使文字稍往上一点
    },
    depostText: {        //交押金
        marginLeft: 20,
        height: 50,
        lineHeight: 50,
        width: 70,
    },
    bottomBackView: {
        position: 'absolute',
        bottom: GlobalStyles.is_iphoneX ? 34 : 0,
        height: 50,
        flexDirection: 'row',
        zIndex: 119,
    },
    bottomTouchable: {
        height: 50,
        width: GlobalStyles.screenWidth / 2,
        borderColor: GlobalStyles.nav_bar_color,
        borderWidth: 1,
    },
    bottomText: {
        height: 50,
        lineHeight: 50,
        width: GlobalStyles.screenWidth / 2,
        textAlign: 'center',
        fontSize: 18,
    },
    cellInput: {
        color: '#666666',
        height: 39,
        width: cell_text_width,
        color: 'black',
        fontSize: 17,
    }

});
