/**
 * 修复信息汇总
 */
import React, { Component } from 'react';
import { FlatList, Platform, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Modal, SafeAreaView, Keyboard, ActivityIndicator, DeviceEventEmitter, Alert } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import { Dimensions } from 'react-native'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import Ionicons from 'react-native-vector-icons/Ionicons'
import BottomListModal from '../../common/BottomListModal'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_REPAIRDONEVIEW_UPDATE, EMITTER_REPAIRINFOMAINTAIN_DONE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView'
import RepariInfoMaintainCell from './RepairInfoMaintainCell'
import BottomPeopleModal from './BottomPeopleModal'
import RepairTeamModal from './RepairTeamModal'


let networkDao = new NetworkDao()
var IS_ADNROID = Platform.OS === 'android';



export default class RepairInfoSummery extends Component {
    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params;
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.repairTeamDic = {}
        this.repairPeoPleArray = []
        this.TeamCode = ''
        this.state = {
            data: [],
            totalLabour: 0,         //总维修工时
            repairWorkhours: 0,
            indicatorAnimating: false,
            IndicatorAnimating: false,
            repairTeam: '',             //显示的维修班组
            repairPeople: '',       //显示的修箱人员  
            repairTeamArr: [],                //修箱班组数据
            repairPeopleArr: []
        }
    }

    onBackPress() {
        //this.refs.AlertView.showAlert();
        return true
    }

    alertSureDown = () => {
        NavigationUtil.goBack(this.props.navigation);
    }

    alertSureDone_ = () => {
        this.setState({ IndicatorAnimating: true })
    }

    alertCancelDown = () => {
    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
                this.keyboardDidShowHandler.bind(this));
            //监听键盘隐藏事件
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
                this.keyboardDidHideHandler.bind(this));
        }

        this.getData()

        this.onDoneListener = DeviceEventEmitter.addListener(EMITTER_REPAIRINFOMAINTAIN_DONE, (data) => {
            let tempArr = new Array().concat(this.state.data)
            tempArr.splice(data.index, 1, data)
            this.setState({ data: tempArr })
            console.log(this.state.data);
        })
    }

    handleNumber(num) {
        console.log(num);
        numm = num.replace(/[^\d^\.]+/g, '')
        return numm ? numm : ''
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    //数据获取
    getData() {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetHandlingQuoteList', paramStr)
            .then(data => {
                console.log(data);
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    let tempArr = data.HandlingQuoteLabourList
                    let totalL = 0  //计划工时
                    for (let i = 0; i < tempArr.length; i++) {
                        let tempDic = tempArr[i]
                        if(tempDic.Labour == 0){
                            continue
                        }
                        let labour = parseFloat(tempDic.Labour) ? parseFloat(tempDic.Labour) : ''
                        totalL += labour
                    }
                    console.log(totalL);
                    this.setState({ data: tempArr, totalLabour: Number.parseInt(totalL * 100) / 100, repairWorkhours: Number.parseInt(totalL * 100) / 100 })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取维修明细失败error:', error)
            })
    }

    //上传工时以及修箱人员数据
    uploadLabour() {
        
        let date = {}
        date.HandlingId = this.params.HandlingId       //  HandlingId
        date.CntrNo = this.params.CntrNo               // 箱号
        date.TeamMessage = this.repairTeamDic           // 班组
        date.WorkerMessage = this.repairPeoPleArray    // 修箱人员
        date.Labour = this.state.repairWorkhours  //工时
        // date.ID = this.params.ID                       // ID
        // date.CreateName = this.params.CreateName     //创建人
        // date.CreateTime = this.params.CreateTime   //   创建时间
        let secVal = this.getToken()
        let param = {}
        param.Token = secVal                      // token
        param.LabourRegister = date
        let paramStr = JSON.stringify(param)
        console.log('我是上传的数据', paramStr)
        networkDao.fetchPostNet('UploadHandlingLabour', paramStr)
            .then(data => {
                if (data._backcode == '201') {
                    console.log('数据上传成功')
                } else {
                    this.setState({ IndicatorAnimating: false })
                    console.log('数据上传失败', _backcode)
                    this.refs.toast.show('数据上传失败', 800)
                    return
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                this.refs.toast.show('请输入完整数据', 800)
            })
        
    }

    //确认修复按钮点击事件
    sureBtnClick() {
        let tempArr = new Array().concat(this.state.data)
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = tempArr[0].HandlingId
        let paramStr = JSON.stringify(param)
        console.log('我是上传的数据', paramStr)
        networkDao.fetchPostNet('RepairedConfirmed', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    this.uploadLabour()
                    NavigationUtil.goBack(this.props.navigation)
                    DeviceEventEmitter.emit(EMITTER_REPAIRDONEVIEW_UPDATE)
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    console.log('数据上传失败')
                    this.refs.toast.show('数据上传失败', 800)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                console.log('确认修复失败error:', error)
            })
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        //卸载键盘弹出事件监听
        if (this.keyboardDidShowListener != null) {
            this.keyboardDidShowListener.remove();
        }
        //卸载键盘隐藏事件监听
        if (this.keyboardDidHideListener != null) {
            this.keyboardDidHideListener.remove();
        }
        if (this.onDoneListener) {
            this.onDoneListener.remove()
        }
    }
    //键盘弹出事件响应
    keyboardDidShowHandler(event) {
        this.setState({ isShowDoneView: false });
    }

    //键盘隐藏事件响应
    keyboardDidHideHandler(event) {
        this.setState({ isShowDoneView: true });
    }

    //获取维修班组
    getrepairTeamData() {
        this.setState({ indicatorAnimating: true, })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.TeamCode = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetRepairCntrTeamList', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this.setState({ repairTeamArr: data.TeamList }, () => {
                        this.refs.repairTeamModal.show(this.state.repairTeamArr)
                    })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取维修班组失败error:', error)
            })
    }

    //获取修箱人员数据
    getrepairPeopleData() {
        if ((!this.TeamCode) || (this.TeamCode == '')) {
            this.refs.toast.show('请先选择维修班组', 800)
            return
        }
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.TeamCode = this.TeamCode
        let paramStr = JSON.stringify(param)
        // console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetRepairCntrWorkerList', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    console.log(data);
                    this.setState({ repairPeopleArr: data.WorkerList }, () => {
                        this.refs.bottomPeopleModal.show(this.state.repairPeopleArr)
                    })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取修箱人员失败error:', error)
            })
    }



    leftButtonClick() {
        this.refs.AlertView.showAlert();
    }

    //分割线
    renderLine() {
        return <View style={{ height: 0.5, backgroundColor: '#eee' }}></View>
    }
    renderTableViewCellLine() {
        return <View style={{ backgroundColor: 'white', paddingLeft: 15, paddingRight: 15, height: 0.5 }}>
            <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
        </View>
    }


    //班组,人员,工时
    renderRepairInfoView() {
        return <View style={{ backgroundColor: '#fff', marginBottom: 90 }}>
            {RepariInfoMaintainCell.renderInputTextCell((text) => {
                let l = this.handleNumber(text)
                this.setState({ repairWorkhours: l })
            }, true, '修箱工时', '请输入修箱工时', 45, 'numeric', this.state.repairWorkhours + '', 'none', 10)}
            {this.renderTableViewCellLine()}
            {RepariInfoMaintainCell.renderChooseTableViewCell(() => {
                if (this.state.repairTeamArr && this.state.repairTeamArr.length > 0) {
                    this.refs.repairTeamModal.show(this.state.repairTeamArr)
                } else {
                    this.getrepairTeamData()
                }

            }, true, '维修班组', this.state.repairTeam, 45)}
            {this.renderTableViewCellLine()}

            {RepariInfoMaintainCell.renderPlusTableViewCell(() => {
                if (this.state.repairPeopleArr && this.state.repairPeopleArr.length > 0) {
                    this.refs.bottomPeopleModal.show(this.state.repairPeopleArr)
                } else {
                    this.getrepairPeopleData()
                }

            }, true, '修箱人员', this.state.repairPeople, 60)}
            {this.renderTableViewCellLine()}


        </View>
    }

    //箱号
    renderTitleView() {
        return <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 45, paddingLeft: 10 }}>
            <Text style={styles.textStyle}>箱号</Text>
            <Text style={[styles.textStyle, { color: '#0877DE', marginLeft: 25, fontSize: 16 }]}>{this.params.CntrNo}</Text>
        </View>

    }

    //flatList的cell
    renderItem(data) {
        var item = data.item
        console.log('我是明细状态:', item.IsRepaired);
        return <View style={{ height: 60, backgroundColor: 'white', paddingLeft: 10, borderBottomColor: '#eee', borderBottomWidth: 1, }}>
            <TouchableOpacity onPress={() => {
                //cell点击事件
                // item.IsRepaired = 'N'
                item.HandlingId = this.params.HandlingId
                item.CntrNo = this.params.CntrNo
                item.index = data.index
                NavigationUtil.goPage(item, 'RepairInfoMaintain')
            }}>
                <View style={{ flexDirection: 'row', height: 60, justifyContent: 'space-between', paddingRight: 10, alignItems: 'center' }}>
                    <Text
                        style={{ color: GlobalStyles.nav_bar_color, lineHeight: 25, fontSize: 15, width: (GlobalStyles.screenWidth) / 2 + 30 }}
                        numberOfLines={2}
                    // ellipsizeMode={'middle'}
                    >
                        {item.RepairName + '    ' + item.DamageLocation}
                    </Text>
                    <View style={{ width: GlobalStyles.screenWidth / 2 - 60, justifyContent: 'space-between', flexDirection: 'row', paddingRight: 20 }}>
                        <Text style={styles.cellInput}>{item.Labour}</Text>
                        <Image
                            style={{ width: 16, height: 16, marginTop: 14 }}
                            source={require('../../../resource/ClockTime.png')}
                        />

                    </View>
                </View>
            </TouchableOpacity>
            {
                item.IsRepaired && item.IsRepaired == 'Y' ? <Image
                    style={{ position: 'absolute', right: 0, top: 0, width: 10, height: 10 }}
                    source={require('../../../resource/rightTopGreen.png')}
                /> : null

            }
        </View >
    }

    //总工时
    renderTotalWorkHours() {
        return <View style={{ marginTop: 10, height: 50, backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'row', height: 49, justifyContent: 'space-between' }}>
                <Text style={{ marginLeft: 20, height: 49, lineHeight: 49, color: 'black', fontSize: 17 }}>总修箱工时</Text>
                <Text style={{ position: 'absolute', right: 15, height: 49, lineHeight: 49, color: '#666666', textAlign: 'right', fontSize: 16 }}>{this.state.totalLabour}</Text>
            </View>
            {this.renderLine()}
        </View>
    }

    //确认修复
    renderBottomView() {
        return <View style={styles.bottomBackView}>
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.bottomTouchable}
                onPress={() => {
                    //确认修复按钮点击事件
                    let tempArr = new Array().concat(this.state.data)
                    console.log(this.state);
                    if (this.state.repairTeam && this.state.repairTeam.length > 0 && this.state.repairPeople && this.state.repairPeople.length > 0) {
                        if (tempArr && tempArr.length > 0) {
                            for (let i = 0; i < tempArr.length; i++) {
                                let tempD = tempArr[i]
                                if ((!tempD.IsRepaired) || (tempD.IsRepaired == 'N')) {
                                    this.refs.toast.show('有未修复的信息', 800)
                                    return
                                }
                            }
                            if (this.state.repairWorkhours == this.state.totalLabour) {
                                this.setState({ IndicatorAnimating: true })
                            }
                            if (this.state.repairWorkhours == '') {
                                this.refs.toast.show('请输入工时', 800)
                                return
                            } else if (this.state.repairWorkhours > this.state.totalLabour) {
                                this.refs.toast.show('录入工时超过总工时', 800)
                                return
                            } else if (this.state.repairWorkhours == 0) {
                                this.refs.toast.show('录入工时不能为0', 800)
                                return
                            } else if (this.state.repairWorkhours > 0 == false) {
                                this.refs.toast.show('请输入正确的工时', 800)
                                return
                            } else if (this.state.repairWorkhours < this.state.totalLabour) {
                                this.refs.done_.showAlert()
                                return
                            }
                        } else {
                            return
                        }
                    } else {
                        this.refs.toast.show('请填写完整数据', 800)
                    }

                }}
            >
                <Text style={styles.bottomText}>确认修复</Text>
            </TouchableOpacity>
        </View>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'修复信息汇总'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                {navigationBar}
                <ScrollView
                    bounces={false}
                    style={{ flex: 1 }}
                >
                    <View style={styles.flatlistBackView}>
                        {this.renderTitleView()}
                        {this.renderLine()}
                        <FlatList
                            data={this.state.data}
                            renderItem={data => this.renderItem(data)}
                            keyExtractor={(item, index) => index.toString()}
                            bounces={false}
                        />
                    </View>
                    {this.renderTotalWorkHours()}
                    {this.renderRepairInfoView()}
                </ScrollView>

                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='#c00'
                    />)}

                <RepairTeamModal
                    ref='repairTeamModal'
                    callBack={data => {

                        this.repairTeamDic = data
                        this.setState({
                            repairTeam: data.TeamName,
                            repairPeople: '',
                            repairPeopleArr: []
                        })
                        this.TeamCode = data.TeamCode
                        let TeamMessage = {}
                        this.repairTeamDic = data
                    }}
                />

                <BottomPeopleModal
                    ref='bottomPeopleModal'
                    callBack={(data) => {
                        console.log(data);
                        let date = []
                        for (let i = 0; i < data.length; i++) {
                            let tempd = data[i]
                            let WorkerMessage = {}
                            WorkerMessage.WorkerName = tempd.WorkerName
                            WorkerMessage.WorkerNo = tempd.WorkerNo
                            date.push(WorkerMessage)
                        }
                        this.repairPeoPleArray = date
                        let tempPeople = ''
                        if (data && data.length > 0) {
                            for (let i = 0; i < data.length; i++) {
                                let dic = data[i]
                                tempPeople = tempPeople + '  ' + dic.WorkerName
                            }
                        }
                        this.setState({ repairPeople: tempPeople })

                    }}
                />

                <AlertView
                    ref="AlertView"
                    TitleText="确定放弃修复?"
                    // DesText=" "
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDown}
                    alertCancelDown={this.alertCancelDown}
                />


                <AlertView
                    ref="done_"
                    TitleText="录入工时小于总工时,是否修复?"
                    // DesText=" "
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDone_}
                    alertCancelDown={this.alertCancelDown}
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                <Modal
                    animationType="none"
                    transparent={true}
                    visible={this.state.IndicatorAnimating}
                    onShow={()=>{
                        this.sureBtnClick()
                    }}
                >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0,0,0,0.5)'
                    }}>
                        <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                        <View style={{
                            backgroundColor: '#333',
                            borderRadius: 3,
                            marginTop: 10,
                            paddingVertical: 10,
                            width: 130,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                            <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                        </View>
                    </View>
                </Modal>
                <Toast ref="toast"
                    position='center'
                />
                {this.renderBottomView()}

            </SafeAreaViewPlus>

        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    flatlistBackView: {
        backgroundColor: GlobalStyles.nav_bar_color,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    textStyle: {             //文字通用样式
        height: 45,
        lineHeight: 45,
        fontSize: 17,
    },
    bottomBackView: {
        position: 'absolute',
        bottom: GlobalStyles.is_iphoneX ? 34 : 0,
        height: 90,
        flexDirection: 'row',
        zIndex: 119,
        marginRight: GlobalStyles.screenWidth,
        backgroundColor: GlobalStyles.backgroundColor
        //backgroundColor:'red'
    },
    bottomTouchable: {
        height: 50,
        marginLeft: 15,
        backgroundColor: GlobalStyles.nav_bar_color,
        marginTop: 20,
        width: GlobalStyles.screenWidth - 30,
        borderRadius: 3,
    },
    bottomText: {
        height: 50,
        lineHeight: 50,
        width: GlobalStyles.screenWidth - 30,
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
    },
    cellInput: {
        height: 44,
        width: 80,
        color: 'black',
        fontSize: 16,
        textAlign: 'center',
        lineHeight: 44
    }

});
