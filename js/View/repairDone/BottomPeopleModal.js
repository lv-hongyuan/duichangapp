/**
 * 底部修箱人员选择
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet, SafeAreaView, ImageBackground } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import { tsThisType } from '@babel/types';
import findIndex from 'lodash/findIndex';
import filter from 'lodash/filter';
let cellWidth = (GlobalStyles.screenWidth - 15 * 5) / 4
let cellHeight = 30

export default class BottomPeopleModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            data: [],  //修箱人员数据
            allsclectitem:false,                            //全选按钮
        }
    }

    show(dataArr) {
        this.setState({
            modalVisible: true,
            data: dataArr
        })
    }

    renderTitleView() {
        return <View style={{ marginTop: 15, height: 30, backgroundColor: 'white' }}>
            <Text style={{ marginLeft: 15, height: 30, lineHeight: 30, fontSize: 14, color: 'black' }}>修箱人员</Text>
        </View>
    }

    renderPeopleView() {
        return <View style={styles.peopleBackView}>
            {this.state.data.map((item, index) => (
                <TouchableOpacity
                    key={index + ''}
                    style={{ marginLeft: 15, marginTop: 15, width: cellWidth, height: cellHeight }}
                    onPress={() => {
                        let index1 = findIndex(this.state.data, { WorkerName: item.WorkerName })
                        let tempData = new Array().concat(this.state.data)
                        tempData[index1].isSelect = !tempData[index1].isSelect
                        this.setState({ data: tempData })
                    }}>

                    {
                        item.isSelect ? <View style={{ width: cellWidth, height: cellHeight, backgroundColor: GlobalStyles.separate_line_color }}>
                            <ImageBackground source={require('../../../resource/peopleBack.png')} style={{ width: cellWidth, height: cellHeight }} ></ImageBackground>
                            <Text style={{ marginTop: -cellHeight, width: cellWidth, height: cellHeight, lineHeight: cellHeight, fontSize: 14, color: '#187ADB', textAlign: 'center' }}>{item.WorkerName}</Text>
                        </View> : <View style={{ width: cellWidth, height: cellHeight, backgroundColor: GlobalStyles.separate_line_color }}>
                                <Text style={{ width: cellWidth, height: cellHeight, lineHeight: cellHeight, fontSize: 14, color: 'black', textAlign: 'center' }}>{item.WorkerName}</Text>
                            </View>
                    }
                </TouchableOpacity>

            ))}
        </View>
    }

    renderBottomView() {
        return <View style={{ height: 50, flexDirection: 'row' }}>
            <TouchableOpacity activeOpacity={1} style={{ height: 50, width: GlobalStyles.screenWidth / 2, backgroundColor: 'white', borderWidth: 1, borderColor: GlobalStyles.nav_bar_color }}
                onPress={() => {
                    //重置按钮点击事件
                    let a = !this.state.allsclectitem
                    if(a == true){
                        let data = this.state.data
                        for(var i = 0;i < data.length;i++){
                            data[i].isSelect = true
                        }
                    }else if(a == false){
                        let data = this.state.data
                        for(var i = 0;i < data.length;i++){
                            data[i].isSelect = false
                        }
                    }
                    this.setState({allsclectitem:a})

                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth / 2, height: 50, lineHeight: 50, textAlign: 'center', color: GlobalStyles.nav_bar_color, fontSize: 18 }}>{this.state.allsclectitem ? '取消选中' : '全选'}</Text>
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={1} style={{ height: 50, width: GlobalStyles.screenWidth / 2, backgroundColor: GlobalStyles.nav_bar_color }}
                onPress={() => {
                    //确定按钮点击事件
                    this.setState({ modalVisible: false ,allsclectitem:false})
                    let selectData = filter(this.state.data, (d) => d.isSelect)
                    this.props.callBack(selectData)
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth / 2, height: 50, lineHeight: 50, textAlign: 'center', color: 'white', fontSize: 18 }}>确  定</Text>
            </TouchableOpacity>
        </View>
    }

    render() {
        return (
            <View style={{ backgroundColor: '#F5F5F5', justifyContent: 'flex-end' }}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'rgba(0,0,0,0.5)' }}>
                        {this.renderTitleView()}
                        {this.renderPeopleView()}
                        <View style={{ height: 15, backgroundColor: GlobalStyles.separate_line_color }}></View>
                        {this.renderBottomView()}
                    </View>

                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundView: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.5)'
    },
    peopleBackView: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingBottom: 15,
    }
});