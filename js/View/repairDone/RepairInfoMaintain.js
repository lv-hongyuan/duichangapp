/**
 * 修复信息维护
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Modal, Keyboard, SafeAreaView, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import { Dimensions } from 'react-native'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SelectImageView from '../../common/SelectImageView'
import ImageViewer from 'react-native-image-zoom-viewer';   //类似微信朋友圈浏览图片的效果
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import NetworkDao from '../../dao/NetworkDao'
import DaoUtil from '../../util/DaoUtil'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_REPAIRINFOMAINTAIN_DONE, EMITTER_REPAIRINFOMAINTAIN_SURE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView'
import RNFS from 'react-native-fs';

var IS_ADNROID = Platform.OS === 'android';
let networkDao = new NetworkDao()

export default class RepairInfoMaintain extends Component {
    constructor(props) {
        super(props)
        this.params = Object.assign({}, this.props.navigation.state.params);
        this.CreateName = this.params.CreateName ? this.params.CreateName : '';
        this.CreateTime = this.params.CreateTime ? this.params.CreateTime : '';
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        this.state = {
            isShowDoneView: true,    //是否显示下面的保存和完成的view
            indicatorAnimating: false, //菊花
            IndicatorAnimating: false,
            IndicatorAnimatingText: '',
            badImgArr: [],               //残损照片数组
            repairImgArr: [],             //修复照片数组
            visible: false,             //是否显示大图展示的modal
            currentSelectBigImg: 0,      //选择的是残损照片还是修复照片,0代表残损,1代表修复
            currentBadImgIndex: 0,            //当前显示第几个图片
            currentRepairImgIndex: 0,
            netImgArr: [],                      //下载的修复照片数组
        }
    }

    onBackPress(e) {
        return true;
    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
            //监听键盘弹出事件
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
                this.keyboardDidShowHandler.bind(this));
            //监听键盘隐藏事件
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
                this.keyboardDidHideHandler.bind(this));
        }

        this.downloadImg()
        this.downloadReapirImg()
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        //卸载键盘弹出事件监听
        if (this.keyboardDidShowListener != null) {
            this.keyboardDidShowListener.remove();
        }
        //卸载键盘隐藏事件监听
        if (this.keyboardDidHideListener != null) {
            this.keyboardDidHideListener.remove();
        }

    }

    //键盘弹出事件响应
    keyboardDidShowHandler(event) {
        this.setState({ isShowDoneView: false });
    }

    //键盘隐藏事件响应
    keyboardDidHideHandler(event) {
        this.setState({ isShowDoneView: true });
    }

    leftButtonClick() {
        this.refs.BackAlertView.showAlert();
    }

    //! 点击返回键删除下载的本地照片
    backAlertSureDown = () => {
        this.cleanNetImgArr()
        NavigationUtil.goBack(this.props.navigation)
    }

    //清理本地网络图片缓存
    cleanNetImgArr() {
        if (this.state.netImgArr && this.state.netImgArr.length > 0) {
            for (let i = 0; i < this.state.netImgArr.length; i++) {
                let dic = this.state.netImgArr[i].uri
                networkDao.deleteLocalImage(dic)
            }
        }
    }

    backAlertCancelDown = () => {
        console.log('点击了alert取消按钮')
    }

    handleNumber(num) {
        num = num.replace(/[^\d^\.]+/g, '')
        return num ? num : ''
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }
    
    //下载残损图片
    downloadImg() {
        this.setState({ indicatorAnimating: true, IndicatorAnimatingText: '正在获取数据' })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        param.CntrNo = this.params.CntrNo
        param.ID = this.params.ID
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('DownloadPhotos', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    this.setState({ indicatorAnimating: false })
                    let tempArr = data.PhotoList
                    if (tempArr && tempArr.length > 0) {
                        let tempArray = []
                        for (let i = 0; i < tempArr.length; i++) {
                            let baseImg = `data:image/png;base64,${tempArr[i]}`;
                            let dic1 = {}
                            dic1.uri = baseImg
                            tempArray.push(dic1)
                        }
                        this.setState({ badImgArr: tempArray })
                    }
                } else {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('图片下载失败', 800)
                }

            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('获取图片失败:', error)
                this.refs.toast.show('图片下载失败', 800)
            })
    }

    //异步图片下载
    async saveImgToLocal(dataA) {
        let netArr = []
        for (let i = 0; i < dataA.length; i++) {
            let url = dataA[i]
            await networkDao.downloadImage(url)
                .then(data => {
                    console.log(data);
                    let dic1 = {}
                    dic1.uri = data
                    dic1.localUri = data
                    netArr.push(dic1)
                })
                .catch(error => {
                    console.log(error);
                    // this.refs.permissionsView.showAlert();
                    this.refs.toast.show('图片下载失败', 800)
                })
        }
        this.setState({ repairImgArr: netArr, netImgArr: netArr, IndicatorAnimating: false })

    }
    //修复照片下载
    downloadReapirImg() {
        this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '正在获取数据' })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        param.CntrNo = this.params.CntrNo
        param.ID = this.params.ID
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('DownloadRepairedPhotos', paramStr)
            .then(data => {
                if (data._backcode == '200') {
                    let tempArr = data.PhotoList
                    this.saveImgToLocal(tempArr)
                } else if (data._backcode == '303') {
                    this.setState({ IndicatorAnimating: false })
                    console.log('没有网络图片');
                } else {
                    this.setState({ IndicatorAnimating: false })
                    this.refs.toast.show('图片下载失败', 800)
                }

            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                console.log('获取图片失败:', error)
                this.refs.toast.show('图片下载失败', 800)
            })

    }

    //点击完成-上传图片
    uploadImage() {
        if (this.state.repairImgArr && this.state.repairImgArr.length > 0) {
            this.setState({ IndicatorAnimating: true, IndicatorAnimatingText: '请等待数据提交' })
            let oriImgArr = this.state.repairImgArr.concat()
            console.log('oriImgArr:',oriImgArr);
            
            let formData = new FormData()
            formData.append('CntrNo', this.params.CntrNo)
            formData.append('HandlingId', this.params.HandlingId)
            formData.append('ID', this.params.ID)
            let secVal = this.getToken()
            formData.append('Token', secVal)

            for (let i = 0; i < oriImgArr.length; i++) {
                let tempDic = oriImgArr[i]
                let a = tempDic.uri;
                let arr = a.split('/');
                if (tempDic.localUri) {
                    formData.append('file', { uri: tempDic.localUri, name: arr[arr.length - 1], type: 'image/jpeg' })
                } else {
                    formData.append('file', { uri: tempDic.uri, name: arr[arr.length - 1], type: 'image/jpeg' })
                }
            }
            networkDao.uploadImgArr2('UploadRepairedPhotos', formData)
                .then(data => {
                    this.setState({ IndicatorAnimating: false })
                    if (data._backcode == '201') {
                        console.log('图片上传成功')
                        let tempDic = this.params
                        tempDic.IsRepaired = 'Y'
                        tempDic.CreateName = this.CreateName
                        tempDic.CreateTime = this.CreateTime
                        if (this.state.netImgArr && this.state.netImgArr.length > 0) {
                            this.cleanNetImgArr()
                        }
                        DeviceEventEmitter.emit(EMITTER_REPAIRINFOMAINTAIN_DONE, tempDic)
                        DeviceEventEmitter.emit(EMITTER_REPAIRINFOMAINTAIN_SURE)
                        NavigationUtil.goBack(this.props.navigation)
                    } else {
                        this.refs.toast.show('图片上传失败', 800)
                    }
                })
                .catch(error => {
                    this.setState({ IndicatorAnimating: false })
                    console.log(error);
                    this.refs.toast.show('图片上传失败', 800)
                })
        }
    }

    //分割线
    renderLine() {
        return <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //残损照片显示区域
    renderBadImgView() {
        return <View style={{ marginTop: 10, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}>
            <Text style={styles.title}>残损照片</Text>
            {this.renderLine()}
            <View style={styles.showImgBackView}>
                {this.state.badImgArr.map((item, index) => (
                    <View key={index + ''} style={styles.imgCell}>
                        <SelectImageView
                            source={item.uri ? item : { uri: item }}
                            style={{
                                width: '100%',
                                height: '100%',
                            }}
                            onPress={() => {
                                this.setState({
                                    visible: true,  //显示大图
                                    currentSelectBigImg: 0,
                                    currentBadImgIndex: index,
                                });
                            }}
                        />
                    </View>
                ))}
            </View>
        </View>
    }

    //修复照片显示区域
    renderRepairImgView() {
        return <View style={{ marginTop: 10, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}>
            <Text style={styles.title}>修复照片</Text>
            {this.renderLine()}
            <View style={styles.addImgBackView}>
                {this.state.repairImgArr.map((item, index) => (
                    <View key={index + ''} style={styles.imgCell}>
                        <SelectImageView
                            source={item.uri ? item : { uri: item }}
                            style={{
                                width: '100%',
                                height: '100%',
                            }}
                            onPress={() => {
                                this.setState({
                                    visible: true,  //显示大图
                                    currentSelectBigImg: 1,
                                    currentRepairImgIndex: index,
                                });
                            }}
                            onDelete={() => {
                                const self = this;
                                const data = self.state.repairImgArr.slice(0);
                                data.splice(index, 1);  //删除图片
                                self.setState({
                                    repairImgArr: data
                                });
                            }}
                        />
                    </View>
                ))}
                {this.state.repairImgArr.length < 15 && (
                    <View style={styles.imgCell}>
                        <SelectImageView
                            style={{
                                width: '100%',
                                height: '100%',
                            }}
                            options={{
                                imageCount: 15,
                            }}
                            onPickImages={(images) => {
                                let data = this.state.repairImgArr.slice(0);
                                data = data.concat(images.map(image => ({
                                    // fileName: image.uri.split('/').reverse()[0],
                                    fileSize: image.size,
                                    path: image.path,
                                    uri: image.path,
                                    // base64: image.base64,
                                })));
                                this.setState({ repairImgArr: data });
                            }}
                        />
                    </View>
                )}
            </View>
        </View>
    }

    //查看图片大图的modal
    checkBigImgModal() {
        let index = this.state.currentSelectBigImg == 0 ? this.state.currentBadImgIndex : this.state.currentRepairImgIndex
        let imgArr = this.state.currentSelectBigImg == 0 ? this.state.badImgArr : this.state.repairImgArr
        return <Modal visible={this.state.visible} transparent>
            <ImageViewer
                index={index}
                imageUrls={imgArr.map(item => ({
                    url: item.uri ? item.uri : item,
                }))}
                onClick={() => {
                    this.setState({ visible: false });
                }}
                onSwipeDown={() => {
                    this.setState({ visible: false });
                }}
            />
        </Modal>
    }

    //完成
    renderDoneView() {
        return <View style={{ marginTop: 60, backgroundColor: '#F8F8F8', width: GlobalStyles.screenWidth }}>
            <TouchableOpacity style={{ marginLeft: 15, width: GlobalStyles.screenWidth - 30, height: 50, backgroundColor: GlobalStyles.nav_bar_color, borderRadius: 5 }}
                onPress={() => {
                    //完成按钮点击事件
                    if (this.state.repairImgArr && this.state.repairImgArr.length > 0) {
                        this.uploadImage()
                    } else {
                        this.refs.toast.show('请上传图片', 800)
                    }
                }}
            >
                <Text style={{ width: GlobalStyles.screenWidth - 30, height: 50, lineHeight: 50, fontSize: 18, color: 'white', textAlign: 'center' }}>完  成</Text>
            </TouchableOpacity>
        </View>
    }

    //遮罩层
    markView() {
        return <Modal
            animationType="none"
            transparent={true}
            visible={true}
        >
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'rgba(0,0,0,0.3)'
            }}>
                <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                <View style={{
                    backgroundColor: '#333',
                    borderRadius: 3,
                    marginTop: 10,
                    paddingVertical: 10,
                    width: 130,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Text style={{ color: '#fff', fontSize: 15 }}>{this.state.IndicatorAnimatingText}</Text>
                </View>
            </View>
        </Modal>
    }
    render() {
        let navigationBar =
            <NavigationBar
                title={'修复信息维护'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: '#F8F8F8' }}>
                {navigationBar}
                <ScrollView
                    style={{ backgroundColor: GlobalStyles.backgroundColor }}
                    bounces={false}
                >
                    {this.renderBadImgView()}
                    {this.renderRepairImgView()}
                    {this.renderDoneView()}
                    {this.checkBigImgModal()}
                </ScrollView>
                {this.state.indicatorAnimating && (this.markView())}
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="BackAlertView"
                    TitleText="确定放弃编辑?"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.backAlertSureDown}
                    alertCancelDown={this.backAlertCancelDown}
                />
                {this.state.IndicatorAnimating && (this.markView())}
            </SafeAreaViewPlus>
        )
    }
}


const styles = StyleSheet.create({
    title: {     //残损照片文字
        color: 'black',
        fontSize: 17,
        fontWeight: '500',
        height: 35,
        lineHeight: 35,
    },
    showImgBackView: {    //添加图片的背景view
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    imgCell: {          //显示图片的view
        width: '33%',
        aspectRatio: 1,
        justifyContent: 'center',
        // paddingLeft:5,
        // paddingRight:5,
        maxHeight: 90,
        alignItems: 'center',
    },
    addImgBackView: {    //添加图片的背景view
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'white',
    },
})

