/**
 * 修复确认
 */

import React, { Component } from 'react';
import { Modal, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_REPAIRDONEVIEW_UPDATE, EMITTER_REPAIRINFOMAINTAIN_SURE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
import Entypo from 'react-native-vector-icons/Entypo'
let networkDao = new NetworkDao()


export default class RepairDoneView extends Component {

    constructor(props) {
        super(props)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.oriArr = []
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],                 //数据
            toTopView: false,                        //下拉刷新
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    //数据获取
    getData() {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetCntrListWaitForClaim', paramStr)
            .then(data => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this._largelist.endRefresh();
                    let tempArr = new Array().concat(data.ClaimedList)
                    let userPlan = []
                    for (var i = 0; i < tempArr.length; i++) {
                        if (tempArr[i].UserId == publicDao.CURRENT_USER) {
                            userPlan.push(tempArr[i])
                        }
                    }
                    console.log('展示的数据：', userPlan)
                    let Dataa = this.handleLargeListData(userPlan)
                    this.oriArr = userPlan
                    this.setState({ dataArr: Dataa, value: '' })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                } else if (data._backcode == '401') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                console.log('修复确认error:', error)
            })
    }
    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    //带遮罩层的数据获取
    getData2() {
        this.setState({ IndicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetCntrListWaitForClaim', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '200') {
                    let tempArr = new Array().concat(data.ClaimedList)
                    let userPlan = []
                    for (var i = 0; i < tempArr.length; i++) {
                        if (tempArr[i].UserId == publicDao.CURRENT_USER) {
                            userPlan.push(tempArr[i])
                        }
                    }
                    console.log('展示的数据：', userPlan)
                    let Dataa = this.handleLargeListData(userPlan)
                    this.oriArr = userPlan
                    this.setState({ dataArr: Dataa })

                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                } else if (data._backcode == '401') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false })
                console.log('修复确认error:', error)
            })
    }


    componentDidMount() {
        this.backPress.componentDidMount();
        this.timer = setTimeout(() => {
            this.setState({ indicatorAnimating: false })
            this.refs.toast.show('网络不佳,请重试', 1200)
        }, 10000)
        this.getData()
        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_REPAIRDONEVIEW_UPDATE, () => {
            this.getData2()
            setTimeout(() => {
                this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
            }, 800)

        });
        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_REPAIRINFOMAINTAIN_SURE, () => {
            this.getData()
        });
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.upDataOkListener) {
            this.upDataOkListener.remove();   //移除事件监听
        }

        this.timer && clearTimeout(this.timer);
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //头部搜索
    headerView = () => {
        return <View style={{ height: 90, backgroundColor: GlobalStyles.backgroundColor }}>
            <View style={styles.searchBackView}>
                <Image style={{ marginLeft: 15, marginTop: 10, width: 15, height: 15 }}
                    source={require('../../../resource/search.png')}
                />
                <TextInput
                    style={styles.headerTextInput}
                    placeholder='请输入箱号或场位'
                    clearButtonMode='always'
                    returnKeyType='search'
                    defaultValue={this.state.value}
                    autoCapitalize='characters'
                    onChangeText={text => {
                        text = text.replace(/\s/gi, '').toUpperCase()
                        this.setState({
                            value: text
                        });
                        const newData = this.oriArr.filter(item => {
                            const itemData = `${item.CntrNo.toUpperCase()} ${item.YardLocation.toUpperCase()}`;
                            const textData = text
                            return itemData.indexOf(textData) > -1;
                        });
                        let Dataa = this.handleLargeListData(newData)
                        this.setState({
                            dataArr: Dataa,
                        });
                    }}
                />
            </View>
            <Text style={styles.headerTextView}>已认领计划列表</Text>
        </View>;
    }

    //cell
    renderItem = ({ row: row }) => {
        let item = this.state.dataArr[0].items[row]
        let cellColor = item.IsHadInfo == 'Y' ? '#FFF7CF' : 'white'
        return <TouchableOpacity onPress={() => {
            //cell点击事件
            NavigationUtil.goPage(item, 'RepairInfoSummery')
        }}>
            <View style={{ height: 60, flexDirection: 'row', backgroundColor: cellColor, borderBottomColor: '#eee', borderBottomWidth: 0.5 }}>
                <View style={{ width: GlobalStyles.screenWidth / 2, height: 60 }}>
                    <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                    <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: GlobalStyles.screenWidth / 2, height: 60 }}>
                    <Text style={styles.cellRightText}>{item.PlanTime}</Text>
                    <Ionicons
                        name={'ios-arrow-forward'}
                        size={20}
                        style={{
                            position: 'absolute',
                            right: 15,
                            alignSelf: 'center',
                            color: '#CCCCCC',
                        }} />
                </View>
            </View>
        </TouchableOpacity>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: '#EEEEEE' }}></View>
    }


    render() {

        //title
        let navigationBar =
            <NavigationBar
                title={'修复确认'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <View style={{ flex: 1 }}>
                    <LargeList
                        ref={ref => (this._largelist = ref)}
                        style={styles.container}
                        data={this.state.dataArr}
                        renderHeader={this.headerView}
                        heightForIndexPath={() => 60.5}
                        renderIndexPath={this.renderItem}
                        refreshHeader={ChineseWithLastDateHeader}
                        onRefresh={() => {
                            this.getData()
                        }}
                        onScroll={({ nativeEvent: { contentOffset: { x, y } } }) => {
                            if (y > 600) {
                                this.setState({ toTopView: true })
                            } else {
                                this.setState({ toTopView: false })
                            }
                        }}

                    />
                </View>

                {/* 置顶 */}
                {this.state.toTopView && (
                    <TouchableOpacity onPress={() => {
                        this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
                    }} style={styles.floatView}>
                        <Entypo
                            name={'align-top'}
                            size={30}
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                            }}
                        />
                    </TouchableOpacity>
                )}
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                {/* 菊花 */}
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='#c00'
                    />)}

                {/* 遮罩层 */}
                {this.state.IndicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}>
                            <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                            <View style={{
                                backgroundColor: '#333',
                                borderRadius: 3,
                                marginTop: 10,
                                paddingVertical: 10,
                                width: 130,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{ color: '#fff', fontSize: 15 }}>正在获取数据</Text>
                            </View>
                        </View>

                    </Modal>
                )}
                <Toast ref="toast"
                    position='center'
                />
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBackView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 35,
    },
    headerTextInput: {
        marginLeft: 10,
        marginTop: 2.5,
        width: GlobalStyles.screenWidth - 75,
        height: 30,
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: 15,
    },
    headerTextView: {
        marginTop: 10,
        marginLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'left',
        lineHeight: 30   //为了居中
    },
    cellLeftText: {
        textAlign: 'left',
        fontSize: 16,
        height: 30,
        marginLeft: 15
    },
    cellRightText: {
        height: 60,
        fontSize: 14,
        textAlign: 'center',
        color: '#999999',
        lineHeight: 60,
    },
    floatView: {
        width: 35,
        height: 35,
        backgroundColor: '#bbb',
        position: 'absolute',
        bottom: 100,
        right: 30,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
