/**
 * 修箱班组的modal
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'


export default class RepairTeamModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            dataArr: [],
        }
    }

    show(data) {
        this.setState({
            modalVisible: true,
            dataArr: data
        })
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //cell
    renderItem(data) {
        var showText = this.state.showText
        return <TouchableOpacity style={[styles.FlatListText, { backgroundColor: 'white' }]} onPress={() => {
            this.setState({ modalVisible: false })
            this.props.callBack(data.item)
        }}>
            <Text style={styles.FlatListText}>{data.item.TeamName}</Text>
        </TouchableOpacity>
    }

    //flatlist
    renderFlatList() {
        return (
            <SafeAreaViewPlus style={styles.backgroundView}>
                <View style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => this.separatorView()}
                        data={this.state.dataArr}
                        renderItem={data => this.renderItem(data)}
                        bounces={false}
                    />
                    <TouchableOpacity style={styles.cancelText} onPress={() => {
                        this.setState({
                            modalVisible: false,
                        })
                    }}>
                        <Text style={styles.FlatListText}>取消</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaViewPlus >
        )
    }

    render() {
        return (
            <View style={{ justifyContent: 'flex-end' }}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderFlatList()}
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundView: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.5)',
        marginTop: 60,
        paddingTop: GlobalStyles.is_iphoneX ? 65 : 0,
    },
    FlatListText: {
        height: 50,
        lineHeight: 50,
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
    },
    cancelText: {
        marginTop: 10,
        backgroundColor: 'white',
        height: 50,
    }
});