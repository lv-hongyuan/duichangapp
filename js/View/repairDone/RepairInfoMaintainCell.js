/**
 * 修复信息维护的cell
 */
import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import GlobalStyles from '../../common/GlobalStyles'

export default class RepariInfoMaintainCell {

    /**
     * 有下拉选项的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} chooseText 
     * @param {*} cellHeight 
     */
    static renderChooseTableViewCell(callBack, isStar, title, chooseText, cellHeight) {
        let titleMarginLeft = isStar ? 5 : 8;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: cellHeight, backgroundColor: 'white', paddingLeft: 8, paddingRight: 15 }}
            onPress={callBack}
        >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <Text style={{ color: '#666666', fontSize: 16, lineHeight: cellHeight }}>{chooseText}</Text>
            <Ionicons
                name={'ios-arrow-down'}
                size={18}
                style={{
                    position: 'absolute',
                    right: 15,
                    alignSelf: 'center',
                    color: '#666666',
                }} />
        </TouchableOpacity>
    }

    /**
     * 有textInput的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} placeHolder 
     * @param {*} cellHeight 
     * @param {*} defaultText
     * @param {*}Capitalize 是否大写
     * @param {*}maxLength 最多字符字数
     * @param {*}endCallBack 输入完成的回调
     */
    static renderInputTextCell(callBack, isStar, title, placeHolder, cellHeight, inputType, defaultText, capitalize, maxLength, endCallBack) {
        let titleMarginLeft = isStar ? 5 : 9;
        return <View style={{ height: cellHeight, flexDirection: 'row', backgroundColor: 'white', paddingLeft: 8, paddingRight: 15 }}>
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <TextInput
                style={{ color: '#666666', paddingTop: 0, paddingBottom: 0, height: cellHeight, width: GlobalStyles.screenWidth - 120 }}
                placeholder={placeHolder}
                value={defaultText}
                fontSize={16}
                clearButtonMode='always'
                keyboardType={inputType}
                autoCapitalize={capitalize}
                maxLength={maxLength}
                onChangeText={(text) => {
                    callBack(text)
                }}
            />
        </View>
    }

    /**
     * 右边加号的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} chooseText 
     * @param {*} cellHeight 
     */
    static renderPlusTableViewCell(callBack, isStar, title, chooseText, cellHeight) {
        let titleMarginLeft = isStar ? 5 : 8;
        let chooseText2 = chooseText.trim()
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: cellHeight, backgroundColor: 'white', paddingLeft: 8, paddingRight: 15 ,alignItems:'center'}}
            onPress={callBack}
        >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <Text numberOfLines={2} style={{ color: '#666666', fontSize: 16, lineHeight: cellHeight / 2,width:GlobalStyles.screenWidth / 2 +15, textAlign:'left'}}>{chooseText2}</Text>
            <Ionicons
                name={'ios-add-circle-outline'}
                size={18}
                style={{
                    position: 'absolute',
                    right: 15,
                    alignSelf: 'center',
                    color: '#666666',
                }} />
        </TouchableOpacity>
    }


}