//修箱业务

import React, { Component } from 'react';
import { StatusBar, StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { Dimensions } from 'react-native'
import NavigationUtil from '../Navigator/NavigationUtils'
import BackPressComponent from "../common/BackPressComponent";
import GlobalStyles from '../common/GlobalStyles';
import publicDao from '../dao/publicDao';
import SafeAreaViewPlus from '../tools/SafeAreaViewPlus';
import Toast from 'react-native-easy-toast';
import NavigationBar from '../tools/NavigationBar';
import ViewUtil from '../util/ViewUtil';
var screenWidth = Dimensions.get('window').width;
var contentItemWH = 60; //三张功能图片大小
var itemMargin = (screenWidth - 3 * 70) / 4; //三张功能图之间的间距


export default class MainView extends Component {

  constructor(props) {
    super(props);
    this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
    this.state = {
      data: ['验箱估价', '押金登记', '估价维护', '场内验残', '计划认领', '修复确认', '已验列表'],     //要显示的功能
    };
  }

  onBackPress() {
    NavigationUtil.goBack(this.props.navigation);
    return true;

  }
  componentDidMount() {
    NavigationUtil.navigation = this.props.navigation;
    this.backPress.componentDidMount();
    if (publicDao.CURRENT_PRIVILEGES && publicDao.CURRENT_PRIVILEGES.length > 0) {
      let privileges = publicDao.CURRENT_PRIVILEGES
      let newArr = []
      for (let i = 0; i < privileges.length; i++) {
        let index = parseInt(privileges[i]) - 1
        newArr.push(this.state.data[index])
      }
      this.setState({ data: newArr })
    } else {
      this.setState({ data: [] })
    }
  }

  componentWillUnmount() {
    this.backPress.componentWillUnmount();
  }

  //要展示的功能
  renderShowView() {
    let itemStyle = [styles.contentItem, { marginLeft: itemMargin }]
    let imageStyle = { width: contentItemWH, height: contentItemWH, borderRadius: 5 }
    return this.state.data.map((item, index) => {
      switch (item) {
        case '验箱估价':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'BoxEvaluateView')
            }
            }
          >
            <Image style={imageStyle} source={require('../../resource/ico1.png')} />
            <Text style={styles.itemTitle}>验箱估价</Text>
          </TouchableOpacity >

          break;
        case '押金登记':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'DepositRegisteView')
            }}
          >
            <Image style={imageStyle} source={require('../../resource/yajindengji.png')} />
            <Text style={styles.itemTitle}>押金登记</Text>
          </TouchableOpacity>

          break;
        case '场内验残':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'FieldBoxEvaluateView')
            }}
          >
            <Image style={imageStyle} source={require('../../resource/WechatIMG22.png')} />
            <Text style={styles.itemTitle}>场内验残</Text>
          </TouchableOpacity>
          break;
        case '估价维护':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
            }}
          >
            <Image style={imageStyle} source={require('../../resource/gujiaweihu.png')} />
            <Text style={styles.itemTitle}>估价维护</Text>
          </TouchableOpacity>

          break;
        case '计划认领':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'PlaneGetView')
            }}
          >
            <Image style={imageStyle} source={require('../../resource/ico2.png')} />
            <Text style={styles.itemTitle}>计划认领</Text>
          </TouchableOpacity>

          break;
        case '修复确认':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'RepairDoneView')
            }}
          >
            <Image style={imageStyle} source={require('../../resource/xiufuqueren.png')} />
            <Text style={styles.itemTitle}>修复确认</Text>
          </TouchableOpacity>

          break;
        case '已验列表':
          return <TouchableOpacity
            key={`${index}`}
            style={itemStyle}
            onPress={() => {
              NavigationUtil.goPage({}, 'HadEvaluateListView')
            }}
          >
            <Image style={imageStyle} source={require('../../resource/ico7.png')} />
            <Text style={styles.itemTitle}>已验列表</Text>
          </TouchableOpacity>

          break;

      }
    })
  }

  leftButtonClick() {
    NavigationUtil.goBack(this.props.navigation)
  }

  render() {
    let navigationBar =
      <NavigationBar
        title={'修箱业务'}
        style={{ backgroundColor: GlobalStyles.nav_bar_color }}
        leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
      />;
    return (
      <SafeAreaViewPlus bottomInset={false} topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
        <View style={styles.container}>
          <Toast ref="toast"
            position='center'
          />
          {navigationBar}
          <View style={styles.showItemBackView}>
            {this.renderShowView()}
          </View>
        </View>
      </SafeAreaViewPlus>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255,249,249,1)',
  },
  navigationBar: {
    height: GlobalStyles.is_iphoneX ? 44 : 64,
    backgroundColor: 'rgba(186,28,38,1)',
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 20,
  },
  contentItem: {
    marginTop: 30,
    width: 70,
    height: 90,
    alignItems: 'center',
  },
  itemTitle: {
    marginTop: 10,
    width: 70,
    textAlign: 'center',
    fontSize: 15,
  },
  showItemBackView: {  //展示功能的背景
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});
