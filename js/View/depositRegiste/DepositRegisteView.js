/**
 * 押金登记
 */

import React, { Component } from 'react';
import { Modal,Platform, StyleSheet, Text, View, Button, ImageBackground, TextInput, TouchableOpacity, FlatList, RefreshControl, Image, SafeAreaView, DeviceEventEmitter, ActivityIndicator } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_DEPOSTIREGISTERVIEW_UPDATE } from '../../common/EmitterTypes'
import AlertView from '../../tools/alertView';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
let networkDao = new NetworkDao()


export default class DepositRegisteView extends Component {

    constructor(props) {
        super(props)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.InDataiArr = []
        this.EnterDataArr = []
        this.selectedItem = 0   //选中了进场还是在场
        this.state = {
            indicatorAnimating: false, //菊花
            enterArr: [],                 //进场箱数据
            inArr: [],                  //在场箱数据
            refreshing: false,
            item0LineColor: GlobalStyles.nav_bar_color,         //进场箱下划线颜色
            item1LineColor: GlobalStyles.backgroundColor,       //在场箱下划线颜色
            item0BgColor: GlobalStyles.nav_bar_color,           //进场箱按钮颜色
            item1BgColor: 'black',                              //在场箱按钮颜色
            Placeholder:'请输入箱号或车牌号'
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }
    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    //token获取
    getToken(){
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length-2)
        if(keystr == '=='){
            this.getToken()
        }
        return secVal
    }

    //数据获取
    getData() {
        this.setState({ indicatorAnimating: true})
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        // console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetCntrListWaitForHandINDeposit', paramStr)
            .then(data => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false})
                if (data._backcode == '200') {
                    this._largelist.endRefresh();
                    let tempArr = new Array().concat(data.DepositList)
                    let inDataArr = []
                    let enterDataArr = []
                    for(var i = 0;i < tempArr.length;i++){
                        if(tempArr[i].DepositType == 'ENTER'){
                            enterDataArr.push(tempArr[i])
                        }else{
                            inDataArr.push(tempArr[i])
                        }
                    }
                    this.EnterDataArr = enterDataArr
                    this.InDataiArr = inDataArr
                    let DataIn = this.handleLargeListData(inDataArr)
                    let DataEnter = this.handleLargeListData(enterDataArr)
                    this.setState({ enterArr: DataEnter ,inArr:DataIn ,value:''})
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }else if(data._backcode == '401'){
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: true})
                // console.log('验箱估价error:', error)
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    componentDidMount() {
        this.backPress.componentDidMount();
        this.timer = setTimeout(() => {
            this.setState({ indicatorAnimating: false })
            this.refs.toast.show('网络不佳,请重试', 1200)
        }, 10000)
        this.getData()

        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_DEPOSTIREGISTERVIEW_UPDATE, () => {
            this.getData()
        });
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.upDataOkListener) {
            this.upDataOkListener.remove();   //移除事件监听
        }

        this.timer && clearTimeout(this.timer);
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    
    headerView = () => {
        return <View style={{ height: 90, backgroundColor: GlobalStyles.backgroundColor }}>
            <View style={styles.searchBackView}>
                <Image style={{ marginLeft: 15, marginTop: 10, width: 15, height: 15 }}
                    source={require('../../../resource/search.png')}
                />
                <TextInput
                    style={styles.headerTextInput}
                    placeholder= {this.state.Placeholder}
                    clearButtonMode='always'
                    returnKeyType='search'
                    defaultValue={this.state.value}
                    autoCapitalize='characters'
                    onChangeText={text=>{
                        text = text.replace(/\s/gi, '').toUpperCase()
                        this.setState({
                            value: text
                        });
                        if (text && text.length > 0) {
                            if (this.selectedItem == 0) {
                                let tempArr = []
                                for (let i = 0; i < this.EnterDataArr.length; i++) {
                                    let tempDic = this.EnterDataArr[i]
                                    let tempStr = `${tempDic.CntrNo.toUpperCase()} ${tempDic.CarrierCode.toUpperCase()}`
                                    console.log(tempStr);
                                    if (tempStr.indexOf(text) > -1) {
                                        tempArr.push(tempDic)
                                    }
                                }
                                let DataArr = this.handleLargeListData(tempArr)
                                this.setState({ enterArr: DataArr })
                            } else {
                                let tempArr1 = []
                                for (let i = 0; i < this.InDataiArr.length; i++) {
                                    let tempDic = this.InDataiArr[i]
                                    let tempStr = `${tempDic.CntrNo.toUpperCase()} ${tempDic.YardLocation.toUpperCase()}`
                                    console.log(tempStr);
                                    if (tempStr.indexOf(text) > -1) {
                                        tempArr1.push(tempDic)
                                    }
                                }
                                let GetArr = this.handleLargeListData(tempArr1)
                                console.log(GetArr);
                                this.setState({ inArr: GetArr })
                                
                            }
                        } else {
                            let DataArr = this.handleLargeListData(this.EnterDataArr)
                            let GetArr = this.handleLargeListData(this.InDataiArr)
                            this.setState({ enterArr: DataArr, inArr: GetArr })
                            
                        }
                    }}
                />
            </View>
            <View style={{ flexDirection: 'row' }}>
                <TouchableOpacity
                    style={{ marginTop: 10, marginLeft: 15, height: 40 }}
                    onPress={() => {
                        let DataArr = this.handleLargeListData(this.EnterDataArr)
                        console.log('我是进场箱子：',DataArr);
                        this.currentSearchText = ''
                        this.selectedItem = 0
                        this.setState({
                            item0BgColor: GlobalStyles.nav_bar_color,
                            item1BgColor: 'black',
                            item0LineColor: GlobalStyles.nav_bar_color,
                            item1LineColor: GlobalStyles.backgroundColor,
                            enterArr: DataArr,
                            value:'',
                            Placeholder:'请输入箱号或车牌号'
                        })
                    }}
                >
                    <Text style={[styles.headerTextView, { color: this.state.item0BgColor }]}>进场箱</Text>
                    <View style={{ width: 50, height: 2, backgroundColor: this.state.item0LineColor }}></View>
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ marginTop: 10, marginLeft: 35, height: 40 }}
                    onPress={() => {
                        let GetArr = this.handleLargeListData(this.InDataiArr)
                        this.currentSearchText = ''
                        this.selectedItem = 1
                        this.setState({
                            item0BgColor: 'black',
                            item1BgColor: GlobalStyles.nav_bar_color,
                            item0LineColor: GlobalStyles.backgroundColor,
                            item1LineColor: GlobalStyles.nav_bar_color,
                            inArr: GetArr,
                            value:'',
                            Placeholder:'请输入箱号或场位'
                        })
                    }}
                >
                    <Text style={[styles.headerTextView, { color: this.state.item1BgColor }]}>在场箱</Text>
                    <View style={{ width: 50, height: 2, backgroundColor: this.state.item1LineColor }}></View>
                </TouchableOpacity>
            </View >
        </View>;
    }

    //cell
    renderItem = ({row : row}) => {
        let item =this.selectedItem == 0 ? this.state.enterArr[0].items[row] : this.state.inArr[0].items[row]
        //console.log(item);
        return <View>
            {/* 进场 */}
            {this.selectedItem == 0 && (
                <TouchableOpacity onPress={() => {
                    //点击事件
                    let dic = {}
                    dic.item = item
                    dic.viewKey = 'DepositRegisteView'
                    NavigationUtil.goPage(dic, 'RegisteFormView')
                }}>
                    <View style={{ height: 59.5, flexDirection: 'row', backgroundColor: 'white',borderBottomColor:'#eee',borderBottomWidth:0.5 }}>
                        <View style={{ width: GlobalStyles.screenWidth / 2, height: 60 }}>
                            <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.CarrierCode}</Text>
                            <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                            <Text style={styles.cellRightText}>{item.ActualArrivalTime}</Text>
                            <Ionicons
                                name={'ios-arrow-forward'}
                                size={20}
                                style={{
                                    position: 'absolute',
                                    right: 15,
                                    alignSelf: 'center',
                                    color: '#CCCCCC',
                                }} />
                        </View>
                    </View>
                </TouchableOpacity>
            )}

            {/* 在场 */}
            {this.selectedItem == 1 && (
                <TouchableOpacity onPress={() => {
                    //点击事件
                    let dic = {}
                    dic.item = item
                    dic.viewKey = 'DepositRegisteView'
                    NavigationUtil.goPage(dic, 'RegisteFormView')
                }}>
                    <View style={{ height: 59.5, flexDirection: 'row', backgroundColor: 'white',borderBottomColor:'#eee',borderBottomWidth:0.5 }}>
                        <View style={{ width: GlobalStyles.screenWidth / 2, height: 60 }}>
                            <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                            <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                            <Text style={styles.cellRightText}>{item.BeginTime}</Text>
                            <Ionicons
                                name={'ios-arrow-forward'}
                                size={20}
                                style={{
                                    position: 'absolute',
                                    right: 15,
                                    alignSelf: 'center',
                                    color: '#CCCCCC',
                                }} />
                        </View>
                    </View>
                </TouchableOpacity>
            )}
        </View>
        
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: '#EEEEEE' }}></View>
    }


    render() {

        let navigationBar =
            <NavigationBar
                title={'押金登记'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {this.state.indicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='#c00'
                        />
                    </Modal>
                )}
                {navigationBar}
                <View style={{ flex: 1 }}>
                    <LargeList
                        ref={ref => (this._largelist = ref)}
                        style={styles.container}
                        data={this.selectedItem == 0 ? this.state.enterArr : this.state.inArr}
                        renderHeader = {this.headerView}
                        heightForIndexPath={() => 60}
                        renderIndexPath={this.renderItem}
                        refreshHeader={ChineseWithLastDateHeader}
                        onRefresh={()=>{
                            this.getData()
                        }}
                        
                    />
                </View>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBackView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 35,
    },
    headerTextInput: {
        marginLeft: 10,
        marginTop: 2.5,
        width: GlobalStyles.screenWidth - 75,
        height: 30,
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: 15,
    },
    headerTextView: {
        // marginTop: 10,
        // marginLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'left',
        lineHeight: 30   //为了居中
    },
    cellLeftText: {
        textAlign: 'left',
        fontSize: 16,
        height: 30,
        marginLeft: 15
    },
    cellRightText: {
        height: 60,
        fontSize: 14,
        textAlign: 'center',
        color: '#999999',
        lineHeight: 60,
    }
});
