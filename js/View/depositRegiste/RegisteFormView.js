/**
 * 登记表单
 */


import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, Button, TextInput, TouchableOpacity, Image, Modal, SafeAreaView, DeviceEventEmitter, ActivityIndicator } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import { Dimensions } from 'react-native'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_DEPOSTIREGISTERVIEW_UPDATE, EMITTER_BOX_UPDATE_OK_TYPE} from '../../common/EmitterTypes'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AlertView from '../../tools/alertView';

let networkDao = new NetworkDao()


export default class RegisteFormView extends Component {

    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        console.log('params:', this.params)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.depositAmount = ''     //押金金额
        this.depositRemark = ''     //押金备注
        this.state = {
            indicatorAnimating: false, //菊花
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    componentDidMount() {
        this.backPress.componentDidMount();
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
    }

    leftButtonClick() {
        // NavigationUtil.goBack(this.props.navigation)
        if (this.params.viewKey == 'DepositRegisteView') {
            NavigationUtil.goBack(this.props.navigation)
            DeviceEventEmitter.emit(EMITTER_DEPOSTIREGISTERVIEW_UPDATE)
        } else if (this.params.viewKey == 'EvaluateSummery') {
            this.props.navigation.goBack(publicDao.BoxStateView_NavigationKey)
            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
        } else if (this.params.viewKey == 'EvaluateSummeryDetail') {
            this.props.navigation.goBack(publicDao.EvaluateSummeryDetail_NavigationKey)
            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
        }else if(this.params.viewKey == 'FieldEvaluateSummery'){
            this.props.navigation.goBack(publicDao.FieldBoxStateView_NavigationKey)
            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
        }else if(this.params.viewKey == 'FieldEvaluateSummeryDetail'){
            this.props.navigation.goBack(publicDao.FieldEvaluateSummeryDetail_NavigationKey)
            DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
        }
    }

    //token获取
    getToken(){
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length-2)
        if(keystr == '=='){
            this.getToken()
        }
        return secVal
    }

    //提交按钮点击事件
    commitDeposit() {
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let hId = ''
        if (this.params.viewKey == 'DepositRegisteView') {
            let dic = this.params.item
            hId = dic.HandlingId
            dType = dic.DepositType
        } else {
            hId = this.params.handlingId
            dType = this.params.depositType
        }
        param.HandlingId = hId
        param.DepositAmount = this.depositAmount
        param.DepositRemark = this.depositRemark
        param.DepositType = dType
        let paramStr = JSON.stringify(param)
        console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('UploadDeposit', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '201') {
                    if (this.params.viewKey == 'DepositRegisteView') {
                        // console.log(data._backcode)
                        NavigationUtil.goBack(this.props.navigation)
                        DeviceEventEmitter.emit(EMITTER_DEPOSTIREGISTERVIEW_UPDATE)
                    } else if (this.params.viewKey == 'EvaluateSummery') {
                        // console.log(data._backcode)
                        this.props.navigation.goBack(publicDao.BoxStateView_NavigationKey)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                    } else if (this.params.viewKey == 'EvaluateSummeryDetail') {
                        // console.log(data._backcode)
                        this.props.navigation.goBack(publicDao.EvaluateSummeryDetail_NavigationKey)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                    }else if(this.params.viewKey == 'FieldEvaluateSummery'){
                        this.props.navigation.goBack(publicDao.FieldBoxStateView_NavigationKey)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                    }else if(this.params.viewKey == 'FieldEvaluateSummeryDetail'){
                        this.props.navigation.goBack(publicDao.FieldEvaluateSummeryDetail_NavigationKey)
                        DeviceEventEmitter.emit(EMITTER_BOX_UPDATE_OK_TYPE);
                    }

                } else if (data._backcode == '500') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.TokenMissView.showAlert()
                }
            })
            .catch(error => {
                console.log('提交押金失败:', error)
            })
    }

    //分割线
    renderLine() {
        return <View style={{ height: 0.5, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }


    render() {
        let navigationBar =
            <NavigationBar
                title={'登记表单'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        let cntrNo = ''
        let amount = ''
        if (this.params.viewKey == 'DepositRegisteView') {
            let dic = this.params.item
            cntrNo = dic.CntrNo
            amount = Number(dic.Amount).toFixed(2)
        } else {
            cntrNo = this.params.cntrNo
            let ownerTotalMoney = this.params.ownerTotalMoney
            let shiperTotalMoney = this.params.shiperTotalMoney
            amount = Number(shiperTotalMoney + ownerTotalMoney).toFixed(2)
            console.log(amount,"估价费用")
        }

        let currentTime = DaoUtil.getCurrentTime()

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <KeyboardAwareScrollView>
                    <View style={styles.container}>
                        <View style={[styles.boxNumBackView, { marginTop: 10}]}>
                            <Text style={styles.leftText}>箱号</Text>
                            <Text style={[styles.rightText, { color: '#0877DE' }]}>{cntrNo}</Text>
                        </View>
                        <View style={[styles.boxNumBackView,{marginTop:1}]}>
                            <Text style={styles.leftText}>估价费用</Text>
                            <Text style={[styles.rightText, { color: '#0877DE' }]}>¥ {amount}</Text>
                        </View>
                        {this.renderLine()}
                        <View style={styles.boxNumBackView}>
                            <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: 45,position: 'absolute',left:7 }}>*</Text>
                            <Text style={styles.leftText}>押金金额</Text>
                            <TextInput
                                style={styles.inputStyle}
                                placeholder='请输入金额'
                                fontSize={16}
                                clearButtonMode='always'
                                keyboardType='numeric'
                                onChangeText={(text) => {
                                    this.depositAmount = text
                                }}
                            />
                        </View>
                        {this.renderLine()}
                        <View style={styles.boxNumBackView}>
                            <Text style={styles.leftText}>收款原因</Text>
                            <TextInput
                                style={styles.inputStyle}
                                placeholder='请输入原因'
                                fontSize={16}
                                clearButtonMode='always'
                                onChangeText={(text) => {
                                    this.depositRemark = text
                                }}
                            />
                        </View>
                        {this.renderLine()}
                        <View style={styles.boxNumBackView}>
                            <Text style={styles.leftText}>收款时间</Text>
                            <Text style={[styles.rightText, { color: '#666666' }]}>{currentTime}</Text>
                        </View>
                        <TouchableOpacity style={styles.confirmBackView} onPress={() => {
                            if (this.depositAmount && this.depositAmount.length > 0) {
                                if(/^0$/.test(this.depositAmount)){
                                    this.refs.toast.show('押金不能为0', 800)
                                }else if(/^[0-9]+\.?[0-9]+?$/.test(this.depositAmount)){
                                    this.setState({ indicatorAnimating: true })
                                }else{
                                    this.refs.toast.show('请填写纯数字', 800)
                                }
                            } else {
                                this.refs.toast.show('请填写完整数据', 800)
                            }
                        }}>
                            <View style={{ height: 50 }}>
                                <Text style={styles.modalConfirm}>提  交</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                <Modal
                    animationType="none"
                    transparent={true}
                    visible={this.state.indicatorAnimating}
                    onShow={()=>{
                        this.commitDeposit()
                    }}
                >
                    <View style={{
                        flex:1,
                        justifyContent:'center',
                        alignItems:'center',
                        backgroundColor:'rgba(0,0,0,0.5)'
                    }}>
                        <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00'/>
                        <View style={{
                            backgroundColor:'#333',
                            borderRadius:3,
                            marginTop:10,
                            paddingVertical:10,
                            width:130,
                            justifyContent:'center',
                            alignItems:'center'
                            }}>
                            <Text style={{color:'#fff',fontSize:15}}>请等待数据提交</Text>
                        </View>
                    </View>
                </Modal>
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    boxNumBackView: {
        flexDirection: 'row',
        paddingLeft: 20,
        height: 45,
        backgroundColor: 'white'
    },
    leftText: {
        width: 90,
        textAlign: 'left',
        fontSize: 17,
        lineHeight: 45,
    },
    rightText: {
        height: 45,
        lineHeight: 45,
        fontSize: 16,
        textAlign: 'left',
    },
    inputStyle: {
        color: '#666666',
        paddingTop: 0,
        paddingBottom: 0,
        height: 45,
        width: GlobalStyles.screenWidth - 120,
    },
    confirmBackView: {
        height: 50,
        marginTop: 100,
        backgroundColor: GlobalStyles.nav_bar_color,
        marginLeft: 15,
        width: GlobalStyles.screenWidth - 30,
        borderRadius: 5,
    },
    modalConfirm: {      //提交
        height: 50,
        lineHeight: 50,
        fontSize: 10,
        textAlign: 'center',
        color: 'white',
        fontSize: 18,
    },

});



