/**
 * 验箱估价
 */
import React, { Component } from 'react';
import { Modal,Platform, StyleSheet, Text, View, Button,  TextInput, TouchableOpacity,  RefreshControl, Image, SafeAreaView, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_BOX_UPDATE_OK_TYPE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
let networkDao = new NetworkDao()


export default class BoxEvaluateView extends Component {

    constructor(props) {
        super(props)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.oriArr = []
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],                 //数据
            refreshing: false,
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }
    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    //token获取
    getToken(){
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length-2)
        if(keystr == '=='){
            this.getToken()
        }
        return secVal
    }

    getData() {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        //console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetCntrListWaitForInspect', paramStr)
            .then(data => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                // console.log('验箱估价:', data)
                if (data._backcode == '200') {
                    this._largelist.endRefresh();
                    let tempArr = new Array().concat(data.InspectList)
                    let Dataa = this.handleLargeListData(tempArr)
                    this.oriArr = tempArr
                    this.setState({ dataArr: Dataa ,value:''})
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }else if(data._backcode == '401'){
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false, })
                // console.log('验箱估价error:', error)
                this.refs.toast.show('获取数据失败', 800)
            })
    }

    componentDidMount() {
        this.backPress.componentDidMount();
        this.timer = setTimeout(() => {
            this.setState({ indicatorAnimating: false })
            this.refs.toast.show('网络不佳,请重试', 1200)
        }, 10000)
        this.getData()

        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_BOX_UPDATE_OK_TYPE, () => {
            this.getData()
        });
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.upDataOkListener) {
            this.upDataOkListener.remove();   //移除事件监听
        }

        this.timer && clearTimeout(this.timer);
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //tableViewHeaderView
		headerView = () => {
        return <View style={{ height: 90, backgroundColor: GlobalStyles.backgroundColor }}>
            <View style={styles.searchBackView}>
                <Image style={{ marginLeft: 15, marginTop: 10, width: 15, height: 15 }}
                    source={require('../../../resource/search.png')}
                />
                <TextInput
                    style={styles.headerTextInput}
                    placeholder='请输入车牌号或箱号'
                    clearButtonMode='always'
                    returnKeyType='search'
                    defaultValue={this.state.value}
                    autoCapitalize='characters'
                    onChangeText={text=>{
                        text = text.replace(/\s/gi, '').toUpperCase()
                        this.setState({
                            value: text
                        });
                        const newData = this.oriArr.filter(item => {
                            const itemData = `${item.CntrNo.toUpperCase()} ${item.CarrierCode.toUpperCase()}`;
                            const textData = text
                            return itemData.indexOf(textData) > -1;
                        });
                        let Dataa = this.handleLargeListData(newData)
                        this.setState({
                            dataArr: Dataa,
                        });
                    }}
                />
            </View>
            <Text style={styles.headerTextView}>待验箱列表</Text>
        </View>;
	}

    //cell
    renderItem = ({row : row}) => {
        let item = this.state.dataArr[0].items[row]
        let isTemp = item.IsExistTemporary  //是否是暂存
        let bgColor = isTemp == 'Y' ? '#FFF7CF' : 'white'
        return <TouchableOpacity onPress={() => {
            //cell点击事件
            if (isTemp == 'Y') {
                NavigationUtil.goPage(item, 'EvaluateSummeryDetail')
                // NavigationUtil.goPage(item, 'BoxStateView')
            } else {
                NavigationUtil.goPage(item, 'BoxStateView')
            }

        }}>
            <View style={{ height: 59.5, flexDirection: 'row', backgroundColor: bgColor,borderBottomColor:'#ccc',borderBottomWidth: 0.5, }}>
                <View style={{ width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                    <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.CarrierCode}</Text>
                    <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', width: GlobalStyles.screenWidth / 2, height: 59.5 }}>
                    <Text style={styles.cellRightText}>{item.ActualArrivalTime}</Text>
                    <Ionicons
                        name={'ios-arrow-forward'}
                        size={20}
                        style={{
                            position: 'absolute',
                            right: 15,
                            alignSelf: 'center',
                            color: '#CCCCCC',
                        }} />
                </View>
            </View>
        </TouchableOpacity>
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: '#EEEEEE' }}></View>
    }


    render() {

        let navigationBar =
            <NavigationBar
                title={'验箱估价'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {this.state.indicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='#c00'
                        />
                    </Modal>
                )}
                {navigationBar}
                <View style={{ flex: 1 }}>
                    {/* <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => this.separatorView()}
						ListHeaderComponent = {this.headerView()}
                        refreshControl={
                            <RefreshControl
                                title={'loading'}
                                colors={['#666666']}
                                refreshing={this.state.refreshing}
                                onRefresh={() =>
                                    this.getData()
                                }
                                tintColor={'#666666'}
                            />
                        }
                        data={this.state.dataArr}
                        renderItem={data => this.renderItem(data)}
                    /> */}
                    <LargeList
                        ref={ref => (this._largelist = ref)}
                        style={styles.container}
                        data={this.state.dataArr}
                        renderHeader = {this.headerView}
                        heightForIndexPath={() => 60}
                        renderIndexPath={this.renderItem}
                        refreshHeader={ChineseWithLastDateHeader}
                        onRefresh={()=>{
                            this.getData()
                        }}
                        
                    />
                </View>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    searchBackView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 35,
    },
    headerTextInput: {
        marginLeft: 10,
        marginTop: 2.5,
        width: GlobalStyles.screenWidth - 75,
        height: 30,
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: 15,
    },
    headerTextView: {
        marginTop: 10,
        marginLeft: 15,
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'left',
        lineHeight: 30   //为了居中
    },
    cellLeftText: {
        textAlign: 'left',
        fontSize: 16,
        height: 30,
        marginLeft: 15
    },
    cellRightText: {
        height: 60,
        fontSize: 14,
        textAlign: 'center',
        color: '#999999',
        lineHeight: 60,
    }
});
