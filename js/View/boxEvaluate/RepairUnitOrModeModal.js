/**
 * 维修组件或维修方式
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View, Modal, Dimensions, Platform, Keyboard, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import GlobalStyles from '../../common/GlobalStyles';
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'


export default class RepairUnitOrModeModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            showText: '',    
            dataArr: [],
        }
    }

    show(text, data) {
        this.setState({
            modalVisible: true,
            showText: text,
            dataArr: data
        })
    }

    //cell之间的分割线
    separatorView() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //cell
    renderItem(data) {
        var showText = this.state.showText
        return <TouchableOpacity style={[styles.FlatListText, { backgroundColor: 'white' }]} onPress={() => {
            this.setState({ modalVisible: false })
            data.item.showText = showText
            this.props.callBack(data.item)
        }}>
            <Text style={styles.FlatListText}>{showText == 'RepairUnit' ? data.item.PartCode : data.item.RepairMeans}</Text>
        </TouchableOpacity>
    }

    //flatlist
    renderFlatList() {
        // console.log('传过来的数据:', this.props.data)
        return (
            <SafeAreaViewPlus style={styles.backgroundView}>
                <View style={{ backgroundColor: GlobalStyles.backgroundColor ,paddingTop: 60}}>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        ItemSeparatorComponent={() => this.separatorView()}
                        data={this.state.dataArr}
                        renderItem={data => this.renderItem(data)}
                        bounces={false}
                        getItemLayout={(data, index) => (
                            {length: 50, offset: 50 * index, index}
                        )}
                        initialNumToRender={20}
                    />
                    <TouchableOpacity style={styles.cancelText} onPress={() => {
                        this.setState({
                            modalVisible: false,
                        })
                    }}>
                        <Text style={styles.FlatListText}>取消</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaViewPlus >
        )
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                <Modal
                    animationType={"none"}
                    visible={this.state.modalVisible}
                    transparent={true}
                >
                    {this.renderFlatList()}
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    backgroundView: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(0,0,0,0.5)',
        // marginTop: 60,
        paddingTop: GlobalStyles.is_iphoneX ? 65 : 0,
    },
    FlatListText: {
        height: 50,
        lineHeight: 50,
        textAlign: 'center',
        fontSize: 18,
        color: 'black',
    },
    cancelText: {
        marginTop: 10,
        backgroundColor: 'white',
        height: 50,
    }
});