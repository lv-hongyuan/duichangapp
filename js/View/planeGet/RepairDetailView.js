/**
 * 维修明细
 */
import React, { Component } from 'react';
import { FlatList, Platform, StyleSheet, Text, View, ScrollView, TextInput, TouchableOpacity, Image, Modal, SafeAreaView, Keyboard, ActivityIndicator, DeviceEventEmitter, Alert } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import { Dimensions } from 'react-native'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import Ionicons from 'react-native-vector-icons/Ionicons'
import BottomListModal from '../../common/BottomListModal'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_PLANEGETVIEW_UPDATE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView'
let networkDao = new NetworkDao()
var IS_ADNROID = Platform.OS === 'android';
var cell_text_width = (GlobalStyles.screenWidth - 30) / 4



export default class RepairDetailView extends Component {
    constructor(props) {
        super(props)
        this.params = this.props.navigation.state.params;
        this.backPress = new BackPressComponent({ backPress: (e) => this.onBackPress(e) });
        this.state = {
            data: [],
            indicatorAnimating: false,
            IndicatorAnimating: false
        }
    }

    onBackPress(e) {
        //NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    componentDidMount() {
        if (IS_ADNROID) {
            this.backPress.componentDidMount();
        }
        this.getData()
        console.log(this.params);
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    //数据获取
    getData() {
        this.setState({ indicatorAnimating: true, refreshing: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        // console.log('paramStr is :', paramStr)
        networkDao.fetchPostNet('GetHandlingQuoteList', paramStr)
            .then(data => {
                this.setState({ indicatorAnimating: false, refreshing: false })
                if (data._backcode == '200') {
                    this.setState({ data: data.HandlingQuoteLabourList })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false, refreshing: false })
                console.log('获取维修明细失败error:', error)
            })
    }

    //认领点击事件
    getRepairClick() {
        this.setState({ IndicatorAnimating: true, refreshing: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('ClaimRepairPlan', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false, refreshing: false })
                if (data._backcode == '201') {
                    NavigationUtil.goBack(this.props.navigation)
                    DeviceEventEmitter.emit(EMITTER_PLANEGETVIEW_UPDATE);
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false, refreshing: false })
                console.log('认领失败error:', error)
            })

    }

    //确定放弃认领
    abandonAlertSureDown = () => {
        this.setState({ IndicatorAnimating: true, refreshing: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingId = this.params.HandlingId
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('CancelClaim', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false, refreshing: false })
                if (data._backcode == '201') {
                    NavigationUtil.goBack(this.props.navigation)
                    DeviceEventEmitter.emit(EMITTER_PLANEGETVIEW_UPDATE);
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.setState({ IndicatorAnimating: false, refreshing: false })
                console.log('放弃失败error:', error)
            })
    }
    abandonAlertCancelDown = () => {

    }

    //分割线
    renderLine() {
        return <View style={{ height: 1, backgroundColor: GlobalStyles.separate_line_color }}></View>
    }

    //箱号
    renderTitleView() {
        return <View>
            <View style={{ flexDirection: 'row', backgroundColor: 'white', height: 45, paddingLeft: 10 }}>
                <Text style={styles.textStyle}>箱号</Text>
                <Text style={[styles.textStyle, { color: '#0877DE', marginLeft: 25, fontSize: 16 }]}>{this.params.CntrNo}</Text>
                <Text style={[styles.textStyle, { color: '#0877DE', fontSize: 16, position: 'absolute', right: 15, }]}>{this.params.UserName}</Text>
                {/* <View style={{backgroundColor:'rgb(150,200,100)',position: 'absolute',top:0,right:0, width:65,height:25,alignItems:'center',justifyContent:'center'}}>
                    <Text style={{color:'#fff'}}>李宁</Text>
                </View> */}
            </View>
        </View>

    }

    //flatList的cell
    renderItem(data) {
        var item = data.item
        let fontS = 16
        return <View style={{ height: 81, backgroundColor: 'white', paddingLeft: 10, justifyContent: 'space-around', paddingRight: 10 }}>
            <View style={{ flexDirection: 'row', height: 30, justifyContent: 'space-between' }}>
                <View style={{ flexDirection: 'row', height: 35, marginTop: 5 }}>
                    <Text style={{ height: 18, fontSize: 15, color: '#666666' }}>长-宽-数量-工时 :  </Text>
                    <Text style={{ height: 18, lineHeight: 18, fontSize: 13, color: GlobalStyles.gray_text_color }}>{item.Length + '-' + item.Width + '-' + item.RepairNum + '-' + item.Labour}</Text>
                </View>

            </View>
            <View style={{ flexDirection: 'row', height: 50, justifyContent: 'space-between' }}>
                <Text style={{ color: GlobalStyles.nav_bar_color, height: 55, fontSize: fontS, width: GlobalStyles.screenWidth / 2 }}>{item.RepairName}</Text>
                <Text style={[styles.cellInput, { fontSize: fontS }]}>{item.DamageLocation}</Text>
                <Text style={[styles.cellInput, { fontSize: fontS }]}>{item.RepairMeans}</Text>
            </View>
            {this.renderLine()}
        </View >
    }

    //认领
    renderBottomView() {
        return <View style={styles.bottomBackView}>
            <TouchableOpacity
                activeOpacity={0.7}
                style={styles.bottomTouchable}
                onPress={() => {
                    if (this.params.selectedItem == 0) {
                        //认领按钮点击事件
                        this.getRepairClick()
                    } else {
                        //放弃按钮点击事件
                        if (this.params.IsHadInfo == 'Y') {
                            this.refs.IsHadInfoItems.showAlert();
                        } else {
                            this.refs.AbandonAlertView.showAlert();
                        }

                    }
                }}
            >
                <Text style={styles.bottomText}>{this.params.selectedItem == 0 ? '认  领' : '放  弃'}</Text>
            </TouchableOpacity>
        </View>
    }

    render() {
        let navigationBar =
            <NavigationBar
                title={'维修明细'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color} style={{ backgroundColor: GlobalStyles.backgroundColor }}>
                {navigationBar}
                <ScrollView
                    bounces={false}
                    style={{ flex: 1 }}
                >
                    <View style={styles.flatlistBackView}>
                        {this.renderTitleView()}
                        {this.renderLine()}
                        <FlatList
                            data={this.state.data}
                            renderItem={data => this.renderItem(data)}
                            keyExtractor={(item, index) => index.toString()}
                            bounces={false}
                        />
                    </View>


                    {this.state.indicatorAnimating && (
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='gray'
                        />)}
                    <Toast ref="toast"
                        position='center'
                    />
                    <AlertView
                        ref="AbandonAlertView"
                        TitleText="确定放弃该计划?"
                        CancelText='取消'
                        OkText='确定'
                        BottomRightFontColor='#BA1B25'
                        alertSureDown={this.abandonAlertSureDown}
                        alertCancelDown={this.abandonAlertCancelDown}
                    />
                    <AlertView
                        ref="IsHadInfoItems"
                        TitleText="该计划已上传修复照片"
                        DesText='确定放弃该计划?'
                        DesTextFontSize={17}
                        CancelText='取消'
                        OkText='确定'
                        BottomRightFontColor='#BA1B25'
                        alertSureDown={this.abandonAlertSureDown}
                        alertCancelDown={this.abandonAlertCancelDown}
                    />
                    <AlertView
                        ref="TokenMissView"
                        TitleText="登陆失效，请重新登陆。"
                        CancelText=''
                        OkText='确定'
                        BottomRightFontColor='#BA1B25'
                        alertSureDown={this.TokenMissDown}
                    />
                    {this.state.IndicatorAnimating && (
                        <Modal
                            animationType="none"
                            transparent={true}
                            visible={true}
                        >
                            <View style={{
                                flex: 1,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: 'rgba(0,0,0,0.5)'
                            }}>
                                <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                                <View style={{
                                    backgroundColor: '#333',
                                    borderRadius: 3,
                                    marginTop: 10,
                                    paddingVertical: 10,
                                    width: 130,
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                    <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                                </View>
                            </View>

                        </Modal>
                    )}
                </ScrollView>
                {(this.params.UserId == publicDao.CURRENT_USER || this.params.selectedItem == 0) && this.renderBottomView()}

            </SafeAreaViewPlus>

        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    flatlistBackView: {
        backgroundColor: GlobalStyles.nav_bar_color,
        paddingLeft: 5,
        paddingRight: 5,
        paddingBottom: 5,
    },
    textStyle: {             //文字通用样式
        height: 45,
        lineHeight: 45,
        fontSize: 17,
    },
    bottomBackView: {
        bottom: GlobalStyles.is_iphoneX ? 34 : 0,
        height: 90,
        flexDirection: 'row',
        backgroundColor: GlobalStyles.backgroundColor
    },
    bottomTouchable: {
        height: 50,
        marginLeft: 15,
        backgroundColor: GlobalStyles.nav_bar_color,
        marginTop: 20,
        width: GlobalStyles.screenWidth - 30,
        borderRadius: 3,
    },
    bottomText: {
        height: 50,
        lineHeight: 50,
        width: GlobalStyles.screenWidth - 30,
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
    },
    cellInput: {
        height: 39,
        width: cell_text_width,
        color: 'black',
        fontSize: 17,
        textAlign: 'center'
    }

});
