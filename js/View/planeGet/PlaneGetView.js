/**
 * 计划认领
 */

import React, { Component } from 'react';
import { Modal, StyleSheet, Text, View, TextInput, TouchableOpacity, Image, ActivityIndicator, DeviceEventEmitter } from 'react-native';
import NavigationUtil from '../../Navigator/NavigationUtils'
import NavigationBar from '../../tools/NavigationBar';
import GlobalStyles from '../../common/GlobalStyles';
import ViewUtil from '../../util/ViewUtil'
import Ionicons from 'react-native-vector-icons/Ionicons'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import SafeAreaViewPlus from '../../tools/SafeAreaViewPlus'
import BackPressComponent from '../../common/BackPressComponent'
import DaoUtil from '../../util/DaoUtil'
import NetworkDao from '../../dao/NetworkDao'
import publicDao from '../../dao/publicDao';
import Toast, { DURATION } from 'react-native-easy-toast'
import { EMITTER_PLANEGETVIEW_UPDATE } from '../../common/EmitterTypes';
import AlertView from '../../tools/alertView';
import { LargeList } from "react-native-largelist-v3";
import { ChineseWithLastDateHeader } from "react-native-spring-scrollview/Customize";
import Entypo from 'react-native-vector-icons/Entypo'
let networkDao = new NetworkDao()


export default class PlaneGetView extends Component {

    constructor(props) {
        super(props)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        this.oriArr = []
        this.oriGetArr = []
        this.selectedItem = 0   //选中了全部还是认领
        this.state = {
            indicatorAnimating: false, //菊花
            dataArr: [],                 //全部数据
            getArr: [],                  //认领数据
            item0BgColor: GlobalStyles.nav_bar_color,    //全部按钮的颜色
            item1BgColor: 'black',                       //认领按钮颜色
            item0LineColor: GlobalStyles.nav_bar_color,  //全部下划线的颜色
            item1LineColor: GlobalStyles.backgroundColor,    //提交下划线的颜色
            refreshing: false,                          //正在请求数据
            currentData: '全部',                         //显示哪个数据
            selectItem: [],                              //多选数据
            Rightcell: false,                                  //右侧编辑按钮
            Rightfont: '多选',                             //全选
            allsclectitem: false,                            //全选按钮
            IndicatorAnimating: false,                    //遮罩层
            planGetButtom: true,                     //认领按钮
            toTopView: false,                        //置顶
        }
    }

    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }
    //符合大列表的数据结构
    handleLargeListData(dataArray) {
        let rData = [{ items: [] }];
        dataArray.forEach(element => {
            rData[0].items.push(element);
        });
        return rData
    }

    //token获取
    getToken() {
        let timeStamp = DaoUtil.getCurrentTimestamp()
        val = publicDao.CURRENT_TOKEN + timeStamp
        secVal = DaoUtil.encryption(val)
        let keystr = secVal.substring(secVal.length - 2)
        if (keystr == '==') {
            this.getToken()
        }
        return secVal
    }

    //数据获取
    getData() {
        this.setState({ indicatorAnimating: true })
        let param = {}
        let secVal = this.getToken()
        param.Token = secVal
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetCntrListWaitForClaim', paramStr)
            .then(data => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                if (data._backcode == '200') {
                    this._largelist.endRefresh();
                    let tempArr = new Array().concat(data.UnClaimList)
                    let tempArr1 = new Array().concat(data.ClaimedList)
                    for (var i = 0; i < tempArr.length; i++) {
                        tempArr[i].select = false
                    }
                    for (var i = 0; i < tempArr1.length; i++) {
                        tempArr1[i].select = false
                    }
                    let DataAll = this.handleLargeListData(tempArr)
                    let GetALl = this.handleLargeListData(tempArr1)
                    this.oriArr = tempArr
                    this.oriGetArr = tempArr1
                    this.setState({ dataArr: DataAll, getArr: GetALl, value: '' })
                } else if (data._backcode == '500') {
                    console.log('toeken失效')

                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                } else if (data._backcode == '401') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                this.timer && clearTimeout(this.timer);
                this.setState({ indicatorAnimating: false })
                console.log('计划认领error:', error)
            })
    }

    componentDidMount() {
        console.log(publicDao.CURRENT_USER);

        this.backPress.componentDidMount();
        this.timer = setTimeout(() => {
            this.setState({ indicatorAnimating: false })
            this.refs.toast.show('网络不佳,请重试', 1200)
        }, 10000)
        this.getData()

        this.upDataOkListener = DeviceEventEmitter.addListener(EMITTER_PLANEGETVIEW_UPDATE, () => {
            // this._flatList.scrollToOffset({animated: true, viewPosition: 0, index: 0});
            this.getData()
            setTimeout(() => {
                this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
            }, 300)
        });
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        if (this.upDataOkListener) {
            this.upDataOkListener.remove();   //移除事件监听
        }
        this.timer && clearTimeout(this.timer);
    }

    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }

    //头部搜索框及页面切换
    headerView = () => {
        return <View style={{ height: 100, backgroundColor: GlobalStyles.backgroundColor }}>
            <View style={styles.searchBackView}>
                <Image style={{ marginLeft: 15, marginTop: 10, width: 15, height: 15 }}
                    source={require('../../../resource/search.png')}
                />
                <TextInput
                    style={styles.headerTextInput}
                    placeholder='请输入箱号或场位'
                    clearButtonMode='always'
                    returnKeyType='search'
                    autoCapitalize='characters'
                    defaultValue={this.state.value}
                    onChangeText={(text) => {
                        text = text.replace(/\s/gi, '').toUpperCase()
                        this.setState({
                            value: text
                        });
                        if (text && text.length > 0) {
                            if (this.selectedItem == 0) {
                                let tempArr = []
                                for (let i = 0; i < this.oriArr.length; i++) {
                                    let tempDic = this.oriArr[i]
                                    let tempStr = `${tempDic.CntrNo.toUpperCase()} ${tempDic.YardLocation.toUpperCase()}`
                                    if (tempStr.indexOf(text) > -1) {
                                        tempArr.push(tempDic)
                                    }
                                }
                                let DataArr = this.handleLargeListData(tempArr)
                                this.setState({ dataArr: DataArr })
                            } else {
                                let tempArr1 = []
                                for (let i = 0; i < this.oriGetArr.length; i++) {
                                    let tempDic = this.oriGetArr[i]
                                    let tempStr = `${tempDic.CntrNo.toUpperCase()} ${tempDic.YardLocation.toUpperCase()}`
                                    if (tempStr.indexOf(text) > -1) {
                                        tempArr1.push(tempDic)
                                    }
                                }
                                let GetArr = this.handleLargeListData(tempArr1)
                                this.setState({ getArr: GetArr })
                            }

                        } else {
                            let DataArr = this.handleLargeListData(this.oriArr)
                            let GetArr = this.handleLargeListData(this.oriGetArr)
                            this.setState({ dataArr: DataArr, getArr: GetArr })
                            // this.setState({ dataArr: this.oriArr, getArr: this.oriGetArr })
                        }

                    }}
                />
            </View>
            <View style={{ flexDirection: 'row' }}>

                {/* 可认领 */}
                <TouchableOpacity
                    style={{ marginTop: 10, marginLeft: 15, height: 40 }}
                    onPress={() => {
                        // this.oriArr = this.state.dataArr
                        let DataArr = this.handleLargeListData(this.oriArr)
                        // this.currentSearchText = ''
                        this.selectedItem = 0
                        this.setState({
                            currentData: '全部',
                            item0BgColor: GlobalStyles.nav_bar_color,
                            item1BgColor: 'black',
                            item0LineColor: GlobalStyles.nav_bar_color,
                            item1LineColor: GlobalStyles.backgroundColor,
                            dataArr: DataArr,
                            planGetButtom: true,
                            value: ''
                        })
                        if (this.state.dataArr && this.state.dataArr.length > 0) {
                            this.cleanselectAllItem()
                        }
                    }}
                >
                    <Text style={[styles.headerTextView, { color: this.state.item0BgColor }]}>可认领</Text>
                    <View style={{ width: 50, height: 2, backgroundColor: this.state.item0LineColor }}></View>
                </TouchableOpacity>

                {/* 已认领 */}
                <TouchableOpacity
                    style={{ marginTop: 10, marginLeft: 35, height: 40 }}
                    onPress={() => {
                        // this.oriArr = this.state.getArr
                        let GetArr = this.handleLargeListData(this.oriGetArr)
                        // this.currentSearchText = ''
                        this.selectedItem = 1
                        this.setState({
                            currentData: '认领',
                            item0BgColor: 'black',
                            item1BgColor: GlobalStyles.nav_bar_color,
                            item0LineColor: GlobalStyles.backgroundColor,
                            item1LineColor: GlobalStyles.nav_bar_color,
                            getArr: GetArr,
                            planGetButtom: false,
                            value: ''
                        })
                        if (this.state.getArr && this.state.getArr.length > 0) {
                            this.cleanselectAllItem()
                        }
                    }}
                >
                    <Text style={[styles.headerTextView, { color: this.state.item1BgColor }]}>已认领</Text>
                    <View style={{ width: 50, height: 2, backgroundColor: this.state.item1LineColor }}></View>
                </TouchableOpacity>

                {/* 多选 */}
                <TouchableOpacity
                    style={{ marginTop: 10, height: 40, position: 'absolute', right: 15 }}
                    onPress={() => {
                        let cell = this.state.Rightcell
                        if (cell == false) {
                            let FistList = this.state.currentData == '全部' ? this.state.dataArr : this.state.getArr
                            for (var i = 0; i < FistList.length; i++) {
                                FistList[i].select = false
                            }
                            this.setState({
                                Rightcell: true,
                                Rightfont: '取消',
                                selectItem: [],
                                allsclectitem: false
                            })
                        } else {
                            this.setState({
                                Rightcell: false,
                                Rightfont: '多选'
                            })
                        }
                    }}
                >
                    <Text style={[styles.headerTextView, { color: this.state.Rightfont == '多选' ? GlobalStyles.nav_bar_color : '#aaa' }]}>{this.state.Rightfont}</Text>
                </TouchableOpacity>
            </View >

        </View >;
    }

    //左侧选择框
    _selectItemPress(item) {
        let USERID = publicDao.CURRENT_USER
        if (USERID == item.UserId || this.state.currentData == '全部') {
            let IDList = []
            let FistList = this.state.currentData == '全部' ? this.state.dataArr[0].items : this.state.getArr[0].items
            for (var i = 0; i < FistList.length; i++) {
                IDList.push(FistList[i].HandlingId)
            }
            let cellindex = IDList.indexOf(item.HandlingId)
            let data = this.state.selectItem
            if (!item.select) {
                data.push(item.HandlingId)
            } else {
                let aa = data.indexOf(item.HandlingId)
                this.state.selectItem.splice(aa, 1)
            }
            FistList[cellindex].select = !item.select
            console.log(item, this.state.selectItem);
            let DataArr = this.handleLargeListData(FistList)
            this.setState({ dataArr: DataArr, selectItem: data })
        }

    }

    //认领
    planeGetSure(data) {
        // this._flatList.scrollToOffset({animated: true, viewPosition: 0, index: 0}); 
        this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
        let param = {}
        this.setState({ IndicatorAnimating: true })
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingIdArray = data
        let paramStr = JSON.stringify(param)
        console.log('data:', paramStr);
        networkDao.fetchPostNet('BatchClaimRepairPlan', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    console.log('认领成功')
                    this.setState({
                        selectItem: [],
                        Rightcell: false,
                        Rightfont: '多选'
                    })
                    this.getData()
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.setState({ IndicatorAnimating: false })
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                console.log('error:', error)
                this.setState({ IndicatorAnimating: false })
            })

    }

    abandonAlertCancelDown = () => {

    }

    //放弃认领
    abandonAlertSureDown = () => {
        // this._flatList.scrollToOffset({animated: true, viewPosition: 0, index: 0}); 
        this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
        let param = {}
        this.setState({ IndicatorAnimating: true })
        let secVal = this.getToken()
        param.Token = secVal
        param.HandlingIdArray = this.state.selectItem
        let paramStr = JSON.stringify(param)
        console.log('data:', paramStr);
        networkDao.fetchPostNet('BatchCancelClaim', paramStr)
            .then(data => {
                this.setState({ IndicatorAnimating: false })
                if (data._backcode == '201') {
                    console.log('放弃成功')
                    this.setState({
                        selectItem: [],
                        Rightcell: false,
                        Rightfont: '多选'
                    })
                    this.getData()
                } else if (data._backcode == '500') {
                    console.log('toeken失效')
                    this.refs.TokenMissView.showAlert()
                } else if (data._backcode == '400') {
                    this.refs.toast.show(data._backmes, 800)
                }
            })
            .catch(error => {
                console.log('error:', error)
                this.setState({ IndicatorAnimating: false })
            })

    }

    //清除全选
    cleanselectAllItem() {
        let FistList = this.state.currentData == '全部' ? this.state.dataArr[0].items : this.state.getArr[0].items
        for (var i = 0; i < FistList.length; i++) {
            FistList[i].select = false
        }
        this.setState({ selectItem: [], allsclectitem: false })
    }

    //列表全选
    selectAllItem() {
        let a = !this.state.allsclectitem
        let IDList = []
        let FistList = this.state.currentData == '全部' ? this.state.dataArr[0].items : this.state.getArr[0].items
        if (a == true) {
            for (var i = 0; i < FistList.length; i++) {
                if (FistList[i].UserId == publicDao.CURRENT_USER || this.state.currentData == '全部') {
                    FistList[i].select = true
                    IDList.push(FistList[i].HandlingId)
                }

            }
        } else if (a == false) {
            for (var i = 0; i < FistList.length; i++) {
                FistList[i].select = false
            }
            IDList = []
        }
        this.setState({ selectItem: IDList, allsclectitem: a })
        console.log('全选数据:', IDList);
    }
    stringSlice(text) {
        let newText = text.substring(0, 4)
        return newText
    }

    //判断是否选择了有图片的箱子
    IsHadInfoView(data) {
        let AllArr = this.state.getArr[0].items
        let ALlItem = []
        let IsHadInfoItem = false
        for (var i = 0; i < AllArr.length; i++) {
            if (AllArr[i].IsHadInfo == 'Y') {
                ALlItem.push(AllArr[i].HandlingId)
            }
        }
        for (var j = 0; j < data.length; j++) {
            let INDEX = ALlItem.indexOf(data[j])
            if (INDEX > -1) {
                IsHadInfoItem = true
                break
            }
        }
        if (IsHadInfoItem == false) {
            this.refs.AbandonAlertView.showAlert();
        } else {
            this.refs.IsHadInfoItems.showAlert();
        }

    }

    //cell
    renderItem = ({ row: row }) => {
        let USERID = publicDao.CURRENT_USER
        let item = this.state.currentData == '全部' ? this.state.dataArr[0].items[row] : this.state.getArr[0].items[row]
        let cellColor = item.IsHadInfo == 'Y' ? '#fffaf8' : 'white'
        // let item = data.item
        return <View>
            {/* 开启多选 已认领*/}
            {this.state.Rightcell && this.state.currentData == '认领' && (
                <TouchableOpacity activeOpacity={1} onPress={() => { this._selectItemPress(item) }}>
                    <View style={{ flexDirection: 'row', backgroundColor: cellColor, borderBottomColor: '#eee', borderBottomWidth: 1 }}>
                        {USERID == item.UserId ?
                            <View style={styles.Ionicons}>
                                <Ionicons
                                    name={item.select ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                                    size={24}
                                    color={GlobalStyles.nav_bar_color} >
                                </Ionicons>
                            </View>
                            : <View style={styles.Ionicons}>
                                <SimpleLineIcons
                                    name={'ban'}
                                    size={19}
                                    color={'#ccc'} >
                                </SimpleLineIcons>
                            </View>}
                        <View style={{ width: '35%', height: 60 }}>
                            <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                            <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1, height: 60, justifyContent: 'space-between' }}>
                            <Text style={styles.cellRightText}>{item.PlanTime}</Text>
                            <View style={{ backgroundColor: '#c33', position: 'absolute', top: 0, right: 0, width: 65, height: 20, alignItems: 'center', lineHeight: 20 }}>
                                <Text style={{ color: '#fff', fontSize: 13 }}>{this.stringSlice(item.UserName)}</Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )}

            {/* 开启多选 可认领*/}
            {this.state.Rightcell && this.state.currentData == '全部' && (
                <TouchableOpacity activeOpacity={1} onPress={() => { this._selectItemPress(item) }}>
                    <View style={{ flexDirection: 'row', backgroundColor: 'white', borderBottomColor: '#eee', borderBottomWidth: 1 }}>
                        <View style={styles.Ionicons}>
                            <Ionicons
                                name={item.select ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                                size={24}
                                color={GlobalStyles.nav_bar_color} >
                            </Ionicons>
                        </View>
                        <View style={{ width: '53%', height: 60 }}>
                            <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                            <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1, height: 60, justifyContent: 'space-between' }}>
                            <Text style={styles.cellRightText}>{item.PlanTime}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )}

            {/* 点击查看详情，未开启多选 */}
            {!this.state.Rightcell && (
                <TouchableOpacity activeOpacity={1} onPress={() => {
                    item.selectedItem = this.selectedItem
                    NavigationUtil.goPage(item, 'RepairDetailView')
                }}>
                    <View style={{ flexDirection: 'row', backgroundColor: cellColor, borderBottomColor: '#eee', borderBottomWidth: 1 }}>
                        <View style={{ width: this.state.currentData == '全部' ? "60%" : '40%', height: 60 }}>
                            <Text style={[styles.cellLeftText, { color: 'black', fontSize: 15, lineHeight: 30 }]}>{item.YardLocation}</Text>
                            <Text style={[styles.cellLeftText, { color: '#187ADB' }]}>{item.CntrNo}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', width: this.state.currentData == '全部' ? "40%" : '60%', height: 60 }}>
                            <Text style={styles.cellRightText}>{item.PlanTime}</Text>
                            {this.state.currentData == '认领' && (
                                <View style={{ backgroundColor: '#c33', position: 'absolute', top: 0, right: 0, width: 65, height: 20, alignItems: 'center', lineHeight: 20 }}>
                                    <Text style={{ color: '#fff', fontSize: 13 }}>{this.stringSlice(item.UserName)}</Text>
                                </View>
                            )}

                        </View>
                    </View>
                </TouchableOpacity>
            )}
        </View>
    }

    render() {

        let navigationBar =
            <NavigationBar
                title={'计划认领'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;

        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <View style={{ flex: 1 }}>
                    <LargeList
                        ref={ref => (this._largelist = ref)}
                        style={styles.container}
                        data={this.state.currentData == '全部' ? this.state.dataArr : this.state.getArr}
                        renderHeader={this.headerView}
                        heightForIndexPath={() => 61}
                        renderIndexPath={this.renderItem}
                        refreshHeader={ChineseWithLastDateHeader}
                        showsVerticalScrollIndicator={false}
                        onRefresh={() => {
                            this.getData()
                        }}
                        onScroll={({ nativeEvent: { contentOffset: { x, y } } }) => {
                            if (y > 800) {
                                this.setState({ toTopView: true })
                            } else {
                                this.setState({ toTopView: false })
                            }
                        }}
                    />

                    {/* 置顶 */}
                    {this.state.toTopView && (
                        <TouchableOpacity onPress={() => {
                            this._largelist && this._largelist.scrollTo({ x: 0, y: 0 })
                        }} style={styles.floatView}>
                            <Entypo
                                name={'align-top'}
                                size={30}
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                }}
                            />
                        </TouchableOpacity>
                    )}

                    {/* 开启全选 */}
                    {this.state.Rightcell && (
                        <View style={{
                            height: 55,
                            backgroundColor: GlobalStyles.backgroundColor,
                            //backgroundColor:'red',
                            flexDirection: 'row',
                            justifyContent: 'space-between'

                        }}>

                            {/* 全选 */}
                            <TouchableOpacity
                                style={{
                                    paddingTop: 16,
                                    paddingLeft: 13,
                                    height: 40,
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    // backgroundColor:'red',
                                    width: 110
                                }}
                                onPress={() => {
                                    this.selectAllItem()
                                }}
                            >
                                <Ionicons
                                    name={this.state.allsclectitem ? 'ios-radio-button-on' : 'ios-radio-button-off'}
                                    size={24}
                                    color={GlobalStyles.nav_bar_color} >
                                </Ionicons>
                                <Text style={{ marginLeft: 8, fontWeight: '700', fontSize: 15 }}>{this.state.allsclectitem ? '取消选中' : '全选'}</Text>
                            </TouchableOpacity>

                            {/* 认领/放弃认领 */}
                            <TouchableOpacity
                                onPress={() => {
                                    let data = this.state.selectItem
                                    if (this.state.planGetButtom) {
                                        if (data && data.length > 0) {
                                            this.planeGetSure(data)
                                        } else {
                                            this.refs.toast.show('请至少选中一条记录', 800)
                                        }
                                    } else {
                                        if (data && data.length > 0) {
                                            this.IsHadInfoView(data)
                                        } else {
                                            this.refs.toast.show('请至少选中一条记录', 800)
                                        }
                                    }
                                }}
                                style={{
                                    height: 55,
                                    backgroundColor: GlobalStyles.nav_bar_color,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: 140,
                                }}
                            >
                                <Text style={{ fontSize: 20, color: '#fff' }}>{this.state.planGetButtom ? '认领' : '放弃认领'}</Text>
                            </TouchableOpacity>

                        </View>
                    )}
                </View>
                <Toast ref="toast"
                    position='center'
                />
                <AlertView
                    ref="TokenMissView"
                    TitleText="登陆失效，请重新登陆。"
                    CancelText=''
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.TokenMissDown}
                />
                <AlertView
                    ref="AbandonAlertView"
                    TitleText="确定放弃所选计划?"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.abandonAlertSureDown}
                    alertCancelDown={this.abandonAlertCancelDown}
                />
                <AlertView
                    ref="IsHadInfoItems"
                    TitleText="所选计划中包含已上传修复照片的计划"
                    DesText='确定放弃所选计划?'
                    // TitleFontSize = {18}
                    DesTextFontSize={17}
                    // DesTextFontColor = '#666'
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.abandonAlertSureDown}
                    alertCancelDown={this.abandonAlertCancelDown}
                />
                {this.state.indicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <ActivityIndicator
                            style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                            size={GlobalStyles.isIos ? 'large' : 40}
                            color='#c00'
                        />
                    </Modal>
                )}
                {this.state.IndicatorAnimating && (
                    <Modal
                        animationType="none"
                        transparent={true}
                        visible={true}
                    >
                        <View style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}>
                            <ActivityIndicator style={styles.indicatorStyle} size={GlobalStyles.isIos ? 'large' : 40} color='#c00' />
                            <View style={{
                                backgroundColor: '#333',
                                borderRadius: 3,
                                marginTop: 10,
                                paddingVertical: 10,
                                width: 130,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Text style={{ color: '#fff', fontSize: 15 }}>请等待数据提交</Text>
                            </View>
                        </View>

                    </Modal>
                )}
            </SafeAreaViewPlus>
        )
    }
    TokenMissDown = () => {
        NavigationUtil.resetToLoginView()
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: GlobalStyles.backgroundColor,
    },
    Ionicons: {
        paddingLeft: 12,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,
    },
    searchBackView: {
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
        height: 35,
    },
    headerTextInput: {
        marginLeft: 10,
        marginTop: 2.5,
        width: GlobalStyles.screenWidth - 75,
        height: 30,
        paddingTop: 0,
        paddingBottom: 0,
        fontSize: 15,
    },
    headerTextView: {
        fontSize: 16,
        fontWeight: '500',
        textAlign: 'left',
        lineHeight: 30   //为了居中
    },
    cellLeftText: {
        textAlign: 'left',
        fontSize: 16,
        height: 30,
        marginLeft: 12
    },
    cellRightText: {
        height: 60,
        fontSize: 15,
        textAlign: 'center',
        color: '#999999',
        lineHeight: 60,
    },
    floatView: {
        width: 35,
        height: 35,
        backgroundColor: '#bbb',
        position: 'absolute',
        bottom: 100,
        right: 10,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
