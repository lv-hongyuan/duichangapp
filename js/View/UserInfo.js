import React, { Component } from 'react';
import { ScrollView, Linking, FlatList, Platform, Dimensions, Image, StyleSheet, Text, View, TouchableOpacity, Modal, ActivityIndicator } from 'react-native';
import NavigationUtil from '../Navigator/NavigationUtils';
import AlertView from '../tools/alertView';
import BackPressComponent from "../common/BackPressComponent";
import GlobalStyles from '../common/GlobalStyles';
import publicDao from '../dao/publicDao';
import SafeAreaViewPlus from '../tools/SafeAreaViewPlus';
import NavigationBar from '../tools/NavigationBar';
import ViewUtil from '../util/ViewUtil'
import Toast from 'react-native-easy-toast';
import DeviceInfo from 'react-native-device-info'
import NetworkDao from '../dao/NetworkDao'
import AsyncStorage from '@react-native-community/async-storage';
const USERINFO_KEY = 'userInfoKey'
const screenHeight = Platform.OS === "ios" ? Dimensions.get("window").height : require("react-native-extra-dimensions-android").get("REAL_WINDOW_HEIGHT");
var screenWidth = Dimensions.get('window').width;
let networkDao = new NetworkDao()
export default class UserInfo extends Component {

    constructor(props) {
        super(props)
        this.backPress = new BackPressComponent({ backPress: () => this.onBackPress() });
        const version = DeviceInfo.getVersion()
        const systemName = DeviceInfo.getSystemName()
        this.state = {
            showModal: false,
            indicatorAnimating: false, //菊花
            Appversion: version,           //版本信息
            SystemName: systemName,          //设备系统
            newestVersion: '',               //最新版本
            newestVersionURL: '',            //最新版本下载地址
            newestVersionMessage: '',           //最新版本更新信息
            showVersionMessageModle: false
        }
    }
    alertCancelDown = () => {

    }

    //自动下载
    _version() {
        AsyncStorage.removeItem(USERINFO_KEY, (error) => {
            if (error) {
                console.log('删除失败:', error)
            } else {
                console.log('删除成功')
            }
        })
        let url = this.state.newestVersionURL
        this.refs.toasts.show('即将前往浏览器下载最新版本', 800)
        this.timer = setTimeout(() => {
            Linking.canOpenURL(url).then(supported => {
                if (!supported) {
                    this.refs.toast.show('请先下载浏览器', 800)
                } else {
                    return Linking.openURL(url);
                }
            }).catch(err => console.error('An error occurred', err));
        }, 2000);
    }

    //获取最新版本
    getVersionMessage(key) {
        this.setState({ indicatorAnimating: true })
        let param = {}
        param.AppKey = this.state.SystemName.toLocaleLowerCase()
        let paramStr = JSON.stringify(param)
        networkDao.fetchPostNet('GetAPPNewestVersion', paramStr)
            .then(data => {
                let num = data.VersionCode.replace(/[^\d^\.]+/g, '')
                if (data._backcode == '200') {
                    console.log('当前版本:', this.state.Appversion, '      最新版本:', num);
                    if (key == 'showmessage') {
                        this.setState({
                            showModal: true,
                            indicatorAnimating: false,
                            newestVersion: data.VersionCode,
                            newestVersionMessage: data.UpgradeContent,
                            showVersionMessageModle: true
                        })
                    } else if(key == 'updata'){
                        if (this.state.Appversion >= num) {
                            this.refs.toast.show('当前已是最新版本', 800)
                            this.setState({ indicatorAnimating: false })
                        } else {
                            this.setState({
                                showModal: true,
                                indicatorAnimating: false,
                                newestVersion: data.VersionCode,
                                newestVersionURL: data.URL,
                                newestVersionMessage: data.UpgradeContent
                            })
                        }
                    }
                } else if (data._backcode == '400') {
                    this.setState({ indicatorAnimating: false })
                    this.refs.toast.show('接口调用异常-获取失败', 800)
                }
            })
            .catch(error => {
                this.setState({ indicatorAnimating: false })
                console.log('errer:', error)
            })
    }

    //退出登陆
    alertSureDown = () => {
        console.log('点击了alert确定按钮')
        NavigationUtil.goPage({ 'logout': '1' }, 'Auth')
        publicDao.IS_LOGOUT = "1"
    }

    //物理按键禁用
    onBackPress() {
        NavigationUtil.goBack(this.props.navigation);
        return true;
    }

    componentDidMount() {

        this.backPress.componentDidMount();
    }

    componentWillUnmount() {
        this.backPress.componentWillUnmount();
        this.timer && clearTimeout(this.timer);
    }

    // 返回首页
    leftButtonClick() {
        NavigationUtil.goBack(this.props.navigation)
    }



    render() {
        let modalBackgroundStyle = {
            backgroundColor: 'rgba(0, 0, 0, 0.7)',
            width: screenWidth,
            height: screenHeight,
            justifyContent: 'center',
            alignItems: 'center',
        };
        let navigationBar =
            <NavigationBar
                title={'个人信息'}
                style={{ backgroundColor: GlobalStyles.nav_bar_color }}
                leftButton={ViewUtil.getLeftBackButton(() => this.leftButtonClick())}
            />;
        return (
            <SafeAreaViewPlus topColor={GlobalStyles.nav_bar_color}>
                {navigationBar}
                <Modal
                    animationType="none"
                    transparent={true}
                    visible={this.state.showModal}
                //visible={false}
                >
                    <View style={modalBackgroundStyle}>
                        {!this.state.showVersionMessageModle ?
                            <View style={styles.alertView}>
                                <View style={styles.alertNameView}>
                                    <Text style={styles.duiChangName}>修箱APP</Text>
                                    <Text style={styles.duiChangName}>{this.state.newestVersion}</Text>
                                </View>
                                <ScrollView style={styles.newVersionMessage}>
                                    <Text style={{ fontSize: 17, marginBottom: 10, }}>升级说明:</Text>
                                    <View style={styles.newVersionMessage2}>
                                        <Text style={{ lineHeight: 25 }}>{this.state.newestVersionMessage}</Text>
                                    </View>
                                </ScrollView>
                                <TouchableOpacity
                                    style={styles.upDataBotton}
                                    onPress={() => {this._version();}}
                                >
                                    <Text style={{ color: '#fff', fontSize: 23, fontWeight: 'bold' }}>升级</Text>
                                </TouchableOpacity>
                                <Toast ref="toasts"
                                    position='center'
                                />
                            </View> :
                            <View style={styles.alertView}>
                                <View style={styles.alertNameView}>
                                    <Text style={styles.duiChangName}>修箱APP</Text>
                                    <Text style={styles.duiChangName}>{this.state.newestVersion}</Text>
                                </View>
                                <ScrollView style={styles.newVersionMessage}>
                                    <Text style={{ fontSize: 17, marginBottom: 10, }}>当前版本信息:</Text>
                                    <View style={styles.newVersionMessage2}>
                                        <Text style={{ lineHeight: 25 }}>{this.state.newestVersionMessage}</Text>
                                    </View>
                                </ScrollView>
                                <TouchableOpacity
                                    style={styles.upDataBotton}
                                    onPress={() => {this.setState({
                                        showModal: false
                                    })}}
                                >
                                    <Text style={{ color: '#fff', fontSize: 23, fontWeight: 'bold' }}>关闭</Text>
                                </TouchableOpacity>
                            </View>

                        }

                    </View>
                </Modal>
                <View style={styles.home}>
                    <View style={{ alignItems: 'center' }}>
                        <Image style={{ width: 70, height: 70, marginBottom: 20 }} source={require('../../resource/ico.png')} />
                        <Text style={{ fontSize: 22 }}>中谷堆场</Text>
                    </View>
                    <View style={{
                        width: '100%',
                        justifyContent: 'space-around'
                    }}>
                        <View style={[styles.info, { borderTopColor: '#eee', borderTopWidth: 1 }]}>
                            <Text style={{ fontSize: 16 }}>堆场名称:  {publicDao.CRUUENT_DUINAME}</Text>
                        </View>
                        <View style={styles.info}>
                            <Text style={{ fontSize: 16 }}>用户名称:  {publicDao.CURRENT_USER}</Text>
                        </View>
                        <TouchableOpacity
                            style={styles.info}
                            onPress={() => {
                                this.getVersionMessage('showmessage')
                            }}
                        >
                            <Text style={{ fontSize: 16 }}>软件版本:  {this.state.Appversion}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.info, { marginBottom: 30 }]}
                            onPress={() => {
                                this.getVersionMessage('updata')
                            }}
                        >
                            <Text style={{ fontSize: 16 }}>检查更新</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        onPress={() => {
                            this.refs.AlertView.showAlert();
                        }}
                        style={styles.button}
                    >
                        <Text style={{ color: '#fff', fontSize: 20 }}>退出登陆</Text>
                    </TouchableOpacity>
                </View>
                <AlertView
                    ref="AlertView"
                    TitleText="确定退出登录?"
                    CancelText='取消'
                    OkText='确定'
                    BottomRightFontColor='#BA1B25'
                    alertSureDown={this.alertSureDown}
                    alertCancelDown={this.alertCancelDown}
                />
                {this.state.indicatorAnimating && (
                    <ActivityIndicator
                        style={[styles.indicatorStyle, { position: 'absolute', top: GlobalStyles.screenHeight / 2 - 10, left: GlobalStyles.screenWidth / 2 - 10 }]}
                        size={GlobalStyles.isIos ? 'large' : 40}
                        color='gray'
                    />
                )}
                <Toast ref="toast"
                    position='center'
                />
            </SafeAreaViewPlus>

        )
    }
}

const styles = StyleSheet.create({
    home: {
        flex: 1,
        backgroundColor: 'rgba(255,249,249,1)',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingHorizontal: 15,
        paddingVertical: 20
    },
    navigationBar: {
        height: GlobalStyles.is_iphoneX ? 44 : 64,
        backgroundColor: 'rgba(186,28,38,1)',
        flexDirection: 'row',
        alignItems: 'center',
        paddingRight: 20,
    },
    button: {
        height: 45,
        backgroundColor: GlobalStyles.nav_bar_color,
        width: 230,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    info: {
        borderBottomColor: '#eee',
        borderBottomWidth: 1,
        width: '100%',
        height: 55,
        justifyContent: 'center',
        alignItems: 'center'
    },
    alertView: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: '44%',
        width: '80%',
        overflow: 'hidden'
    },
    alertNameView: {
        flexDirection: 'row',
        height: 50,
        borderBottomColor: '#888',
        borderBottomWidth: 0.5,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    duiChangName: {
        color: GlobalStyles.nav_bar_color,
        fontSize: 18,
        textAlign: 'center',
        marginRight: 10,
    },
    newVersionMessage: {
        flex: 1,
        paddingHorizontal: 10,
        flexDirection: 'column',
        marginTop: 10
    },
    newVersionMessage2: {
        flex: 1,
        marginLeft: 30
    },
    upDataBotton: {
        backgroundColor: GlobalStyles.nav_bar_color,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
})