import React, { Component } from 'react';

import App from '../App';
import AppNav from '../js/Navigator/AppNavigator'

export default function setup() {
  class Root extends Component {
    render() {
      return (
        <AppNav />
      );
    }
  }

  return Root;
}