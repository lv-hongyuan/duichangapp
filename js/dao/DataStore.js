import AsyncStorage from '@react-native-community/async-storage';
const DUINAME_KEY = 'duiNameKey'
const MAINSCREEN_KEY = 'mainScreenKey'
const USERINFO_KEY = 'userInfoKey'

export default class DataStore {


    saveDuiNameArr(duiNameArr, currentDuiName) {
        this.clearDuiNameArr((data) => {
            if (data == '1') {
                var duiInfo = {}
                duiInfo.duiArr = duiNameArr
                duiInfo.currentDuiName = currentDuiName

                AsyncStorage.setItem(DUINAME_KEY, JSON.stringify(duiInfo), (error) => {
                    if (error) {
                        console.log('保存堆名信息失败')
                    } else {
                        console.log('保存堆名信息成功')
                    }
                })
            }
        })



    }
    getDuiNameArr(callback) {
        AsyncStorage.getItem(DUINAME_KEY, (error, result) => {
            if (error) {
                console.log('获取堆名信息失败')
            } else {
                console.log('堆名信息:', result)
                if (result === null) {
                    callback('{}')
                } else {
                    callback(result)
                }
            }
        })
    }
    clearDuiNameArr(callback) {
        AsyncStorage.removeItem(DUINAME_KEY, (error) => {
            if (error) {
                console.log('删除堆名信息失败:', error)
                callback('0')
            } else {
                console.log('删除堆名信息成功')
                callback('1')
            }
        })
    }

    //保存用户名密码和token
    saveUser(duiName, userName, passWord, token) {

        var user = {}
        user.userName = userName
        user.passWord = passWord
        user.token = token
        console.log(user)
        AsyncStorage.setItem(duiName, JSON.stringify(user), (error) => {
            if (error) {
                console.log('用户保存失败')
            } else {
                console.log('用户保存成功')
            }
        })

    }
    //获取用户名密码
    getUsers(duiName, callback) {
        AsyncStorage.getItem(duiName, (error, result) => {
            if (error) {
                console.log('获取数据失败')
            } else {
                console.log('用户信息:', result)
                if (result === null) {
                    callback('{}')
                } else {
                    callback(result)
                }
            }
        })
    }

    //清除用户名密码
    clearUser(duiName, callback) {
        AsyncStorage.removeItem(duiName, (error) => {
            if (error) {
                console.log('删除失败:', error)
                callback('0')
            } else {
                console.log('删除成功')
                callback('1')
            }
        })
    }

    //保存主页screenKey
    saveMainScreenKey(mainKey) {
        AsyncStorage.setItem(MAINSCREEN_KEY, mainKey, (error) => {
            if (error) {
                console.log('保存主页ScreenKey失败')
            } else {
                console.log('保存登录页ScreenKey成功')
            }
        })
    }
    //获取主页screenKey
    getMainScreenKey(callback) {
        AsyncStorage.getItem(MAINSCREEN_KEY, (error, result) => {
            if (error) {
                console.log('获取主页ScreenKey成功')
            } else {
                console.log('主页ScreenKey:', result)
                if (result === null) {
                    callback('')
                } else {
                    callback(result)
                }
            }
        })
    }

    //保存当前登陆用户的信息
    saveCurrentUserInfo(info) {
        var user = {}
        user.userName = info.userName
        user.duiName = info.duiName
        user.duiCode = info.duiCode
        user.token = info.token
        user.RoleID = info.RoleID
        user.Privileges = info.Privileges
        console.log(user)
        AsyncStorage.setItem(USERINFO_KEY, JSON.stringify(info), (error) => {
            if (error) {
                console.log('保存用户信息失败')
            } else {
                console.log('保存用户信息成功')
            }
        })
    }

    //获取当前登陆用户的信息
    getCurrentUserInfo(callback){
        AsyncStorage.getItem(USERINFO_KEY, (error, result) => {
            if (error) {
                console.log('获取用户信息失败')
            } else {
                console.log('当前用户信息:', result)
                if (result === null) {
                    callback('{}')
                } else {
                    callback(result)
                }
            }
        })
    }

    //清除当前用户信息
    clearCurrentUserInfo(callback) {
        AsyncStorage.removeItem(USERINFO_KEY, (error) => {
            if (error) {
                console.log('删除失败:', error)
                callback('0')
            } else {
                console.log('删除成功')
                callback('1')
            }
        })
    }

}