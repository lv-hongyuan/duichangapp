import FieldEvaluateSummeryDetail from "../View/fieldBoxEvaluate/fieldEvaluateSummeryDetail";

/**
 * 全局变量
 */

export default {
     CRUUENT_DUINAME: "",
     CURRENT_DUICODE: "",
     CURRENT_USER: "",
     CURRENT_TOKEN: "",
     CURRENT_PRIVILEGES: [],
     CURRENT_ROLEID:[],
     IS_LOGOUT: "0",
     BoxStateView_NavigationKey:'',
     EvaluateSummeryDetail_NavigationKey:'',
     FieldEvaluateSummeryDetail_NavigationKey:'',
     FieldBoxStateView_NavigationKey:''
}