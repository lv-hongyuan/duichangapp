/**
 * 网络请求
 */
import xml2js from 'react-native-xml2js/lib/parser';
import RNFetchBlob from 'rn-fetch-blob'
import RNFS from 'react-native-fs';
import { Platform, CameraRoll } from 'react-native';



export const header = {
    content: {
        Accept: 'application/Json',
        'Content-Type': 'application/json',
    },
};

var BASE_URL = '172.16.1.94:8088';    //测试服务器 
// var BASE_URL = '117.14.63.6:8088';    //正式服务器
// var BASE_URL = '192.168.10.95:8088';      //孙尖的服务器
var BASE_DOMAIN = 'http://' + BASE_URL + '/api/'
const dui_url = 'http://' + BASE_URL + '/api/GetYardMessage'
const login_url = 'http://' + BASE_URL + '/api/LoginCheck'

export default class NetworkDao {

    fetDuiName() {
        return new Promise((resolve, reject) => {
            fetch(dui_url, {
                method: 'POST',
                headers: {
                    Accept: 'json,application/xhtml+xml,text/html,application/xml',
                    'Content-Type': 'application/json',
                },
            }).then((response) => {
                if (response.ok) {
                    response.text().then(text => {
                        //数据解析
                        console.log('数据:', text)
                        let jsonData = JSON.parse(text)
                        console.log('转义后数据:', jsonData)
                        // let useData = jsonData.d
                        // console.log('可用数据:', useData)
                        resolve(jsonData)
                        // xml2js.parseString(text, (error, result) => {
                        //     console.log('error:', error)
                        //     console.log('result:', result.string._)
                        //     if (error) {
                        //         reject(error)
                        //     } else {
                        //         resolve(result.string._)
                        //     }
                        // })
                    })
                }
            }).catch((error) => {
                reject(error);
            })
        })
    }

    fetchPostNet(path, param) {
        let net_url = BASE_DOMAIN + path
        console.log('net_url is :', net_url)
        return new Promise((resolve, reject) => {
            fetch(net_url, {
                method: 'POST',
                headers: {
                    Accept: 'json,application/xhtml+xml,text/html,application/xml',
                    'Content-Type': 'application/json',
                },
                body: param
            }).then((response) => {
                if (response.ok) {
                    response.text().then(text => {
                        //数据解析
                        // console.log('数据:', text)
                        let jsonData = JSON.parse(text)
                        console.log('可用数据:', jsonData)

                        resolve(jsonData)

                    })
                }
            }).catch((error) => {
                console.log('error:', error)
                reject(error);
            })
        })
    }

    fetchLoginNet(param) {
        return new Promise((resolve, reject) => {
            fetch(login_url, {
                method: 'POST',
                headers: {
                    Accept: 'json,application/xhtml+xml,text/html,application/xml',
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                    'Content-Type': 'application/json',
                },
                body: param
            }).then((response) => {
                if (response.ok) {
                    response.text().then(text => {
                        //数据解析
                        console.log(text)
                        // let replaceData = text.split('\\').join('')
                        // console.log('去掉斜杠:',replaceData)
                        // let replaceData1 = text.replace(/\"{/,"{")
                        // let replaceData2 = replaceData1.replace(/}\"/,"}")
                        // console.log('正常的json:',replaceData2)
                        let jsonData = JSON.parse(text)
                        console.log('转义后数据:', jsonData)
                        // let useData = JSON.parse(jsonData.d)
                        // let useData = jsonData.d
                        // console.log('可用数据:', useData)

                        resolve(jsonData)
                    })
                }
            }).catch((error) => {
                reject(error);
            })
        })
    }

    uploadImgArr(path, body) {
        let net_url = BASE_DOMAIN + path
        return new Promise((resolve, reject) => {
            RNFetchBlob.fetch('POST', net_url, {
                // 'Content-Type': 'multipart/form-data',
            }, body)
                // .then(response => response.json())
                .then(response => {
                    console.log('上传结果:', response)
                    resolve(response)
                })
                .catch(error => {
                    console.log('上传失败:', error)
                    reject(error);
                })
        })
    }

    uploadImgArr2(path, data) {
        let net_url = BASE_DOMAIN + path
        const config = {
            method: 'POST',
            headers: {
                // 'Content-Type': 'multipart/form-data',
            },
            body: data
        }
        return new Promise((resolve, reject) => {
            fetch(net_url, config)
                .then(response => {
                    console.log('图片上传结果:', response)
                    if (response.ok) {
                        response.text().then(text => {
                            let jsonData = JSON.parse(text)
                            console.log('转义后数据:', jsonData)
                            resolve(jsonData)
                        })
                    }
                })
                .catch(error => {
                    console.log('图片上传失败:', error)
                })
        })
    }

    deleteLocalImage(imgUrl) {
        return RNFS.unlink(imgUrl)
            .then(() => {
                console.log('图片删除成功');
            })
            .catch((err) => {
                console.log('图片删除失败:', err);
            });
    }
    deleteLocalImage2(imgUrl) {
        return RNFS.unlink(imgUrl)
            .then(() => {
                console.log('图片删除成功');
            })
            .catch((err) => {
                console.log('图片删除失败:', err);
            });
    }
     downloadImage(imgUrl) {
        return new Promise((resolve, reject) => {
            let dirs = RNFS.ExternalDirectoryPath;
            const downloadDest = `${dirs}/${((Math.random() * 10000000) | 0)}.jpg`;
            const formUrl = imgUrl;
            RNFetchBlob.fs.writeFile(downloadDest, formUrl, 'base64')
                .then(() => {
                    if (downloadDest.indexOf('file://') != -1) {
                        resolve(downloadDest);
                    } else {
                        let localUri = 'file://' + downloadDest
                        resolve(localUri)
                    }
                });
        })
    }
}