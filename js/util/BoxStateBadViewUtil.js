/**
 * 残损估价的cell
 */
import React from 'react';
import { TouchableOpacity, StyleSheet, View, Text, TextInput } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import GlobalStyles from '../common/GlobalStyles'

export default class BoxStateBadViewUtil {
    /**
     * 有下拉选项的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} chooseText 
     * @param {*} cellHeight 
     */
    static renderChooseTableViewCell(callBack, isStar, title, chooseText, cellHeight) {
        let titleMarginLeft = isStar ? 3 : 8;
        let l = chooseText.length;  //文字长度
        let fontS = l < 15 ? 16 : 13;   //文字大小
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: cellHeight, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}
            onPress={callBack}
        >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <Text style={{ color: '#666666', fontSize: fontS, lineHeight: cellHeight, width: GlobalStyles.screenWidth - 150 }}>{chooseText}</Text>
            <Ionicons
                name={'ios-arrow-down'}
                size={22}
                style={{
                    position: 'absolute',
                    right: 15,
                    alignSelf: 'center',
                    color: '#666666',
                }} />
        </TouchableOpacity>
    }

    /**
     * 有textInput的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} placeHolder 
     * @param {*} cellHeight 
     * @param {*} defaultText
     * @param {*}Capitalize 是否大写
     * @param {*}maxLength 最多字符字数
     * @param {*}endCallBack 输入完成的回调
     */
    static renderInputTextCell(callBack, isStar, title, placeHolder, cellHeight, inputType, defaultText, capitalize, maxLength, endCallBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: cellHeight, flexDirection: 'row', backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}>
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <TextInput
                style={{ color: '#666666', paddingTop: 0, paddingBottom: 0, height: cellHeight, width: GlobalStyles.screenWidth - 120 }}
                placeholder={placeHolder}
                defaultValue={defaultText}
                fontSize={16}
                clearButtonMode='always'
                keyboardType={inputType}
                autoCapitalize={capitalize}
                maxLength={maxLength}
                onChangeText={(text) => {
                    callBack(text)
                }}
                onEndEditing={(event) => {
                    endCallBack(event)
                }}
            />
        </View>
    }

    /**
     * 
     * @param {*} textCallBack  textInput回调
     * @param {*} moreCallBack  下拉三角的回调
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} placeHolder 
     * @param {*} cellHeight 
     * @param {*} inputType 
     * @param {*} defultText    给textinput赋值
     */
    static renderInputChooseCell(textCallBack, moreCallBack, isStar, title, placeHolder, cellHeight, inputType, defultText,txtCallBack) {
        let titleMarginLeft = isStar ? 3 : 9;
        return <View style={{ height: cellHeight, flexDirection: 'row', backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}>
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: 90, lineHeight: cellHeight }}>{title}</Text>
            <TextInput
                style={{ color: '#666666', paddingTop: 0, paddingBottom: 0, height: cellHeight, width: GlobalStyles.screenWidth - 150 }}
                placeholder={placeHolder}
                defaultValue={defultText}
                fontSize={16}
                clearButtonMode='never'
                keyboardType={inputType}
                onChangeText={(text) => {
                    textCallBack(text)
                }}
                onEndEditing={(txt) => {
                   txtCallBack(txt)
                }}
            />
            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', right: 8, top: 10, width: 25, height: 25 }}
                onPress={moreCallBack}>
                <Ionicons
                    name={'ios-arrow-down'}
                    size={22}
                    style={{
                        color: '#666666',
                    }} />
            </TouchableOpacity>
        </View>
    }

    /**
     * 只有文字的cell
     * @param {*} callBack 
     * @param {*} isStar 
     * @param {*} title 
     * @param {*} cellHeight 
     */
    static renderTextCell(isStar, title, chooseText, cellHeight, titleWidth) {
        let titleMarginLeft = isStar ? 3 : 8;
        return <TouchableOpacity
            style={{ flexDirection: 'row', height: cellHeight, backgroundColor: 'white', paddingLeft: 15, paddingRight: 15 }}
        >
            {isStar ? <Text style={{ color: '#BC1920', fontSize: 17, textAlign: 'center', lineHeight: cellHeight }}>*</Text> : null}
            <Text style={{ color: 'black', marginLeft: titleMarginLeft, fontSize: 17, width: titleWidth, lineHeight: cellHeight }}>{title}</Text>
            <Text style={{ color: '#666666', fontSize: 16, lineHeight: cellHeight }}>{chooseText}</Text>
        </TouchableOpacity>
    }
}
