/**
 * 数据处理
 */
import secret from '../data/secret.json'
import RSAKey from 'react-native-rsa'
import { Buffer } from 'buffer'

export default class ViewUtil {
    //RSA加密
    static encryption(str) {
        let modulusKey1 = secret.modulus;                       //获取modulus 
        let modulusKey2 = new Buffer(modulusKey1, 'base64');    //创建buffer
        let modulusKey3 = modulusKey2.toString('hex');          //转16进制
        let modulusKey4 = secret.exponent                       //获取AQAB,转16进制就是10001
        let ConsultPublicKey = { n: modulusKey3, e: '10001' };  //生成公钥
        var rsa = new RSAKey();
        rsa.setPublicString(JSON.stringify(ConsultPublicKey));
        var sec_str = rsa.encrypt(str);
        let secBuffer = new Buffer(sec_str, 'hex');
        let secBuffer2 = secBuffer.toString('base64');
        let secBuffer3 = secBuffer2.split('+').join('%2B')  //+用%2B替换,这一步不在加密范围内

        return secBuffer3
    }

    //获取当前时间戳
    static getCurrentTimestamp() {
        let timestamp = parseInt(new Date().getTime() / 1000);
        return timestamp
    }

    //获取当前日期 YYYY-MM-DD
    static getCurrentDate() {
        let date = new Date();
        let seperator1 = "-";
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        let currentdate = year + seperator1 + month + seperator1 + strDate;
        return currentdate;
    }

    //获取当前时间 yyyy-MM-dd hh:mm:ss
    static getCurrentTime() {
        let nowDate = new Date();
        let year = nowDate.getFullYear();
        let month = nowDate.getMonth() + 1 < 10 ? "0" + (nowDate.getMonth() + 1)
            : nowDate.getMonth() + 1;
        let day = nowDate.getDate() < 10 ? "0" + nowDate.getDate() : nowDate
            .getDate();
        let hour = nowDate.getHours();
        let minutes = nowDate.getMinutes();
        let seconds = nowDate.getSeconds();
        let dateStr = year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + seconds;

        return dateStr

    }
}