import { createStackNavigator, createSwitchNavigator, createAppContainer } from "react-navigation";
import MainView from '../View/MainView'
import HomeView from '../View/home'
import LoginViewController from '../View/LoginViewController'
import BoxEvaluateView from '../View/boxEvaluate/BoxEvaluateView'
import BoxStateView from '../View/boxEvaluate/BoxStateView'
import BoxStateBadView from '../View/boxEvaluate/BoxStateBadView'
import DepositRegisteView from '../View/depositRegiste/DepositRegisteView'
import RegisteFormView from '../View/depositRegiste/RegisteFormView'
import EvaluateSummery from '../View/boxEvaluate/EvaluateSummery'
import App from '../../App'
import BoxStateBadDetailView from '../View/boxEvaluate/BoxStateBadDetailView'
import EvaluateSummeryDetail from '../View/boxEvaluate/EvaluateSummeryDetail'
import PlaneGetView from '../View/planeGet/PlaneGetView'
import RepairDetailView from '../View/planeGet/RepairDetailView'
import RepairDoneView from '../View/repairDone/RepairDoneView'
import RepairInfoSummery from '../View/repairDone/RepairInfoSummery'
import RepairInfoMaintain from '../View/repairDone/RepairInfoMaintain'
import UserInfo from '../View/UserInfo'
import FieldBoxEvaluateView from '../View/fieldBoxEvaluate/fieldBoxEvaluateView'
import BoxChangeView from '../View/fieldBoxEvaluate/BoxChangeView'
import FieldBoxStateView from '../View/fieldBoxEvaluate/fieldBoxStateView'
import FieldEvaluateSummery from '../View/fieldBoxEvaluate/fieldEvaluateSummery'
import FieldBoxStateDetailView from '../View/fieldBoxEvaluate/fieldBoxStateDetailView'
import FieldEvaluateSummeryDetail from '../View/fieldBoxEvaluate/fieldEvaluateSummeryDetail'
import HadEvaluateListView from '../View/hadEvaluateList/hadEvaluateView'

const InitNavigator = createStackNavigator({
    LoginViewController: {
        screen: LoginViewController,
        navigationOptions: {
            header: null,
        }
    }
});

const MainNavigator = createStackNavigator({
    MainView: {
        screen: MainView,
        navigationOptions: {
            header: null,
        }
    },
});

//登陆成功后,下次打开APP直接进入mainview
const AppStack = createStackNavigator({
    HomeView:{
        screen:HomeView,
        navigationOptions:{
            header:null
        }
    },
    MainView: {
        screen: MainView,
        navigationOptions: {
            header: null,
        }
    },
    UserInfo: {
        screen:UserInfo,
        navigationOptions:{
            header:null,
        }
    },
    BoxEvaluateView: {
        screen: BoxEvaluateView,
        navigationOptions: {
            header: null,
        }
    },
    BoxStateView: {
        screen: BoxStateView,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    BoxStateBadView: {
        screen: BoxStateBadView,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    DepositRegisteView: {
        screen: DepositRegisteView,
        navigationOptions: {
            header: null
        }
    },
    RegisteFormView: {
        screen: RegisteFormView,
        navigationOptions: {
            header: null,
        }
    },
    EvaluateSummery: {
        screen: EvaluateSummery,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    BoxStateBadDetailView:{
        screen:BoxStateBadDetailView,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    EvaluateSummeryDetail:{
        screen:EvaluateSummeryDetail,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    PlaneGetView:{
        screen:PlaneGetView,
        navigationOptions:{
            header:null,
        }
    },
    RepairDetailView:{
        screen:RepairDetailView,
        navigationOptions:{
            header:null,
            gesturesEnabled:false
        }
    },
    RepairDoneView:{
        screen:RepairDoneView,
        navigationOptions:{
            header:null,
        }
    },
    RepairInfoSummery:{
        screen:RepairInfoSummery,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    RepairInfoMaintain:{
        screen:RepairInfoMaintain,
        navigationOptions:{
            header:null,
            gesturesEnabled: false
        }
    },
    FieldBoxEvaluateView:{
        screen:FieldBoxEvaluateView,
        navigationOptions:{
            header:null,
        }
    },
    BoxChangeView:{
        screen:BoxChangeView,
        navigationOptions:{
            header:null,
        }
    },
    FieldBoxStateView:{
        screen:FieldBoxStateView,
        navigationOptions:{
            header:null,
            gesturesEnabled:false
        }
    },
    FieldEvaluateSummery:{
        screen:FieldEvaluateSummery,
        navigationOptions:{
            header:null,
            gesturesEnabled:false
        }
    },
    FieldEvaluateSummeryDetail:{
        screen:FieldEvaluateSummeryDetail,
        navigationOptions:{
            header:null,
            gesturesEnabled:false
        }
    },
    FieldBoxStateDetailView:{
        screen:FieldBoxStateDetailView,
        navigationOptions:{
            header:null,
            gesturesEnabled:false
        }
    },
    HadEvaluateListView:{
        screen:HadEvaluateListView,
        navigationOptions:{
            header:null
        }
    }
}, {
        defaultNavigationOptions: {
            header: null,
        }
    });


const AuthStack = createStackNavigator({
    LoginViewController: {
        screen: LoginViewController,
        navigationOptions: {
            header: null,
        }
    }
});

const AuthLoading = createStackNavigator({
    App: {
        screen: App,
        navigationOptions: {
            header: null,
        }
    }
});


export default createAppContainer(createSwitchNavigator({
    AuthLoading: AuthLoading,
    Main: AppStack,
    Auth: AuthStack,
}, {
        defaultNavigationOptions: {
            initialRouteName:AuthLoading
        }
    }));
