/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View ,TextInput} from 'react-native';
import DataStore from './js/dao/DataStore'
import publicDao from './js/dao/publicDao'
import SplashScreen from 'react-native-splash-screen' 
let ds = new DataStore();
//禁用字体大小跟随系统
Text.defaultProps={...(Text.defaultProps||{}),allowFontScaling:false};
TextInput.defaultProps={...(TextInput.defaultProps||{}),allowFontScaling:false};
export default class App extends Component {

  async getUserInfo() {
    const { navigate } = this.props.navigation
    await ds.getCurrentUserInfo(data => {
      var jsonData = JSON.parse(data)
      publicDao.CRUUENT_DUINAME = jsonData.duiName
      publicDao.CURRENT_DUICODE = jsonData.duiCode
      publicDao.CURRENT_USER = jsonData.userName
      publicDao.CURRENT_TOKEN = jsonData.token
      publicDao.CURRENT_PRIVILEGES = jsonData.Privileges
      setTimeout(() => {
        SplashScreen.hide()
      },1000); 
      if (jsonData.userName && jsonData.userName.length > 1) {
        navigate('Main')
      } else {
        navigate('Auth')
      }
    })
  } 
  componentDidMount() {
    this.getUserInfo()
  }
  render() {
    return (
      <View style={styles.container}>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
